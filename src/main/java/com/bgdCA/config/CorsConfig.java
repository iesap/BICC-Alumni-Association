package com.bgdCA.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;


/**
 * @ClassName WebMvcConfig
 * @Description TODO
 * @Author AlvinZheng
 * @Date 2020/7/14 17:26
 * @Version 1.0
 **/
@Configuration
public class CorsConfig {

    @Bean
    public FilterRegistrationBean corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin("http://localhost:8080");
        config.addAllowedOrigin("http://127.0.0.1:5500");
        config.addAllowedOrigin("http://127.0.0.1:8081");
        //sms调试
        config.addAllowedOrigin("http://223337742.free.idcfengye.com:8080");
        config.addAllowedOrigin("http://localhost:63343");
        //sms调试 end
        config.addAllowedOrigin("http://111.205.28.204:8081");
        config.addAllowedOrigin("http://111.205.28.204:8080");
        config.addAllowedOrigin("http://49.234.76.225:8080");

        config.addAllowedOrigin("null");
        config.addAllowedHeader("X-PINGOTHER");
        config.addAllowedHeader("Content-Type");
        config.addAllowedMethod("POST");
        config.addAllowedMethod("GET");
        config.setAllowCredentials(true);
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
        bean.setOrder(0);
        return bean;
    }

}