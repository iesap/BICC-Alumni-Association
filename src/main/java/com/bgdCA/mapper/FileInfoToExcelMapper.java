package com.bgdCA.mapper;

import com.bgdCA.domain.FileInfoToExcel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author ShiJiaWei
 * @Date 2020/9/27 15:17
 * @Version 1.0
 */
@Mapper
public interface FileInfoToExcelMapper {
    List<FileInfoToExcel> fileInfoToExcel(FileInfoToExcel fileInfoToExcel);
}
