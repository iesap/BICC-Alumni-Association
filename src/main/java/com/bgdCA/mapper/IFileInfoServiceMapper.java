package com.bgdCA.mapper;

import com.bgdCA.domain.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface IFileInfoServiceMapper {
    List<FileInfo> getFileInfo();

    Integer addFileInfo(FileInfo fileInfo);

    Integer updataFileInfo(FileInfo fileInfo);

    List<FileInfo> getTargetFileInfo(int fileId);

    int batchDeleteFileInfo(List list);

    Integer deleteFileInfo(int fileId);

    Integer uploadExcel(List<FileInfoExcel> fileInfo);

    List<FileReback> getFileReback();

    /*档案数据分析*/
    List<Analysis> dataAnalysisContinent();

    List<Analysis> dataAnalysisWorkDuty();

    List<Analysis> dataAnalysisDepartment();

    List<Analysis> dataAnalysisStudyStart();

    List<Analysis> dataAnalysisStudyEnd();

    List<FileInfoForExcel> dataSearch(FileInfo fileInfo);

    List<FileInfo> searchForAll(String key);

    List<FileInfo> dataAnalysispersonalYear(FileInfo fileInfo);

    List<FileInfo> dataAnalysislastName(FileInfo fileInfo);

    List<FileInfo> dataAnalysisfirstName(FileInfo fileInfo);

    List<FileInfo> dataAnalysischinesename(FileInfo fileInfo);

    List<FileInfo> dataAnalysisidNumber(FileInfo fileInfo);

    List<FileInfo> dataAnalysiscontinent(FileInfo fileInfo);

    List<FileInfo> dataAnalysisnational(FileInfo fileInfo);

    List<FileInfo> dataAnalysisgender(FileInfo fileInfo);

    List<FileInfo> dataAnalysisbirthday(FileInfo fileInfo);

    List<FileInfo> dataAnalysisage(FileInfo fileInfo);

    List<FileInfo> dataAnalysisreligion(FileInfo fileInfo);

    List<FileInfo> dataAnalysiscompany(FileInfo fileInfo);

    List<FileInfo> dataAnalysischineseCompany(FileInfo fileInfo);

    List<FileInfo> dataAnalysislevel(FileInfo fileInfo);

    List<FileInfo> dataAnalysisworkDuty(FileInfo fileInfo);

    List<FileInfo> dataAnalysischineseWorkDuty(FileInfo fileInfo);

    List<FileInfo> dataAnalysistelephone(FileInfo fileInfo);

    List<FileInfo> dataAnalysisemail(FileInfo fileInfo);

    List<FileInfo> dataAnalysisparticipateProject(FileInfo fileInfo);

    List<FileInfo> dataAnalysisstudyStart(FileInfo fileInfo);

    List<FileInfo> dataAnalysisstudyEnd(FileInfo fileInfo);

    List<FileInfo> dataAnalysislanguage(FileInfo fileInfo);

    List<FileInfo> dataAnalysisdepartment(FileInfo fileInfo);

    List<FileInfo> dataAnalysismanager(FileInfo fileInfo);

    List<FileInfo> dataAnalysischineseLevel(FileInfo fileInfo);

    List<SearchTest> analysisplus(FileInfo fileInfo);

}

