package com.bgdCA.mapper;

import com.bgdCA.domain.Signature;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Signature)表数据库访问层
 *
 * @author sms
 * @since 2020-08-12 20:11:16
 */
public interface SignatureMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param signatureId 主键
     * @return 实例对象
     */
    List<Signature> querySignatureById(String userName);

    /**
     * 新增数据
     *
     * @param signature 实例对象
     * @return 影响行数
     */
    int insertSignature(Signature signature);


    /**
     * 通过主键删除数据
     *
     * @param signatureId 主键
     * @return 影响行数
     */
    int deleteSignatureById(Integer signatureId);

}