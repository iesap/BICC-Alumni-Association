package com.bgdCA.mapper;

import com.bgdCA.domain.Groups;
import org.apache.ibatis.annotations.Param;

/**
 * (Groups)表数据库访问层
 *
 * @author sms
 * @since 2020-08-12 18:09:06
 */
public interface GroupsMapper {

    /**
     * 新增数据
     *
     * @param groups 实例对象
     * @return 影响行数
     */
    int insertGroup(Groups groups);

    /**
     * 修改数据
     *
     * @param groupId      原分组ID
     * @param groupContent 分组名
     * @return 影响行数
     */
    int updateGroup(@Param("groupContent") String groupContent,
                    @Param("groupId") int groupId);

    /**
     * 通过主键删除数据
     *
     * @param groupId 主键
     * @return 影响行数
     */
    int deleteGroupById(Integer groupId);

}