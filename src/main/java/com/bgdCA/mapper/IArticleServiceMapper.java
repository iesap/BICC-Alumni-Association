package com.bgdCA.mapper;

import com.bgdCA.domain.Article;
import com.bgdCA.domain.ColumnSecond;
import com.bgdCA.domain.Order;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/3 22:06
 */
@Mapper
public interface IArticleServiceMapper {
    List<Article> findAll();

    Article selectArticle(int articleId);

    int batchDeleteArticle(List list);

    int deleteArticle(int articleId);

    List<Article> selectArticleByAuthor(int authorId);

    int updateArticle(Article article);

    int insertArticle(Article article);

    long selectArticleName();

    List<Article> selectArticleByColumnSecond(String columnSecond);

    Article selectColumnName(int articleId);

    List<Article> selectArticleByColumnId(int columnId);

    List<ColumnSecond> selectAllColumnSecond();

    List<Article> findAllByKey(String keyWord);

    List<Article> selectArticleByAuthorByKey(Article article);

    int selectAllOrder(int articleId);

    int updateArticleOrder(Order order);
}
