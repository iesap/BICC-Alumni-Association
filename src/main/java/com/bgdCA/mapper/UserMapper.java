package com.bgdCA.mapper;

import com.bgdCA.domain.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {
    List<User> findUser(String username);
}
