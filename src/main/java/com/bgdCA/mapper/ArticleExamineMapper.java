package com.bgdCA.mapper;

import com.bgdCA.domain.ArticleDomain;
import com.bgdCA.domain.UserLoginDomain;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ArticleExamineMapper {

    //查询文章详细信息
    List<ArticleDomain> findTarget(String key);

    //获取当前用户权限
    UserLoginDomain checkPower(int userId);

    //一级审核员查询所有当前项目组未审核通过文章
    List<ArticleDomain> findFirstNoPass(@Param("userId") int userId, @Param("key") String key);

    //二级审核员查询所有当前项目组未审核通过文章
    List<ArticleDomain> findSecondNoPass(@Param("userId") int userId, @Param("key") String key);

    //审核通过接口
    int firstPass(int articleId);

    int secondPass(int articleId);

    //审核不通过接口
    int firstNoPass(int articleId);

    int secondNoPass(int articleId);

    //检查文章当前通过状态
    List<ArticleDomain> checkAudit(int articleId);

    //批量审核通过
    int passAllFirstSelect(List list);

    int passAllSecondSelect(List list);

    //批量审核不通过
    int noPassAllFirstSelect(List list);

    int noPassAllSecondSelect(List list);
    //查询所有通过文章
    List<ArticleDomain> findAllPass(String columnSecondName);
}