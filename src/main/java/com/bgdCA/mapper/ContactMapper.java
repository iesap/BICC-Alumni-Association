package com.bgdCA.mapper;

import com.bgdCA.domain.Contact;
import com.bgdCA.domain.ContactAndGroup;
import com.bgdCA.domain.DutyGroup;
import com.bgdCA.domain.Groups;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ContactMapper {
    int deleteContactByKey(Integer contactId);

    int insertContact(Contact record);

    List<Groups> selectByPrimaryKey(String userName);

    int updateContactGroup(@Param("list") List list);

    int deleteGroup(int groupId);

    int updateGroupOfContact(@Param("list") List list, @Param("groupId") int groupId);

    List<Groups> selectEmptyGroup(@Param("userName") String userName);

    int updateMailAddressAndName(@Param("contactMailAddress") String contactMailAddress,
                                 @Param("contactContent") String contactContent,
                                 @Param("contactId") int contactId);

    List<DutyGroup> selectEmailGroupByDepartment();

    List<DutyGroup> selectEmailGroupByCountry();

}