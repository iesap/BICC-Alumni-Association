package com.bgdCA.mapper;

import com.bgdCA.domain.Program;
import com.bgdCA.domain.Team;

import java.util.List;

public interface ProgramMapper {
    int modifyProgram(Program program);

    int deleteProgram(int programId);
    List<Program> findAll();


}
