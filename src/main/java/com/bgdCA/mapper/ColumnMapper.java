package com.bgdCA.mapper;

import com.bgdCA.domain.*;

import java.util.List;

import com.bgdCA.domain.ColumnSecond;
import org.apache.ibatis.annotations.Mapper;
import com.bgdCA.domain.ColumnDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/8/17 18:54
 */
@Mapper
public interface ColumnMapper {
    List<ColumnDomain> selectAllColumnFirst();

    int addSubColumn(ColumnSecond column);

    int addMainColumn(Column column);

    int DeleteColumnFirst(int columnFirstId);

    int DeleteColumnSecond(int columnSecondId);

    List<ColumnDomain> checkNull(int columnFirstId);

    int updataColumnFirst(ColumnDomain columnDomain);

    int updataColumnSecond(ColumnDomain columnDomain);

    List<ColumnSecond> selectArticleByColumnFirst(ColumnSecond columnFirstId);

    List<ColumnDomain> selectAllColumnFirstAndColumnSecond();

    List<ColumnPlus> meaningless();

    List<Column> selectColumnFirstAllByColumnSecondId(int columnSecondId);

    int findMainId();

    int selectOrderByFID(int columnFirstId);

    int selectOrderBySID(int columnSecondId);

    int updateColumnFirstOrder(Order order);

    int updateColumnSecondOrder(Order order);

}
