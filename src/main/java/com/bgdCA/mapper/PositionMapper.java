package com.bgdCA.mapper;

import com.bgdCA.domain.Position;
import com.bgdCA.domain.UserLoginDomain;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/25 10:14
 */
public interface PositionMapper {
    List<Position> findAll();

    int deletePosition(int positionId);

    List<UserLoginDomain> checkNull(int positionId);

    List<Position> checkHigherNull(int positionId);

    Position selectPowerById(int positionId);

    int modifyPosition(Position position);

    int addPosition(Position position);

    List<Position> isError(int positionId);

    List<Position> selectPositionByTeam(@Param("teamId") String teamId);
}
