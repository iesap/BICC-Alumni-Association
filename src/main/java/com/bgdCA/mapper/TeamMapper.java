package com.bgdCA.mapper;

import com.bgdCA.domain.Team;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface TeamMapper {
    int deleteTeam(int teamid);

    int addTeam(Team team);

    Team selectTargetTeam(@Param("teamId") int teamId);
}
