package com.bgdCA.mapper;

import com.bgdCA.domain.SysLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ISysLogServiceMapper {
    int insert(SysLog entity);
}
