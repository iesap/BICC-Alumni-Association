package com.bgdCA.mapper;

import com.bgdCA.domain.StationLetter;

import java.util.List;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/29 09:02
 */
public interface StationLetterMapper {
    List<StationLetter> selectAllByUserId(int userId);

    int deleteStationLetterById(int id);

    int batchDeleteStationLetter(List list);

    int updateStationLetterToRead(int id);

    int insertMessage(StationLetter stationLetter);

    long selectStationLetterId();

    int insertUserMessage(StationLetter stationLetter);

    StationLetter selectStationLetterById(int id);

    List<StationLetter> selectAllOrderByTime(StationLetter stationLetter);
}
