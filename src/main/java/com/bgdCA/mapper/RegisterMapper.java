package com.bgdCA.mapper;

import com.bgdCA.domain.Account;
import com.bgdCA.domain.ResetUser;

import java.util.List;

public interface RegisterMapper {
    List<Account> findTarget(String username);

    int changePsd(ResetUser resetUser);

    int insertUser(Account account);
}
