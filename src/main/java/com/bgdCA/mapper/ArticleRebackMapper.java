package com.bgdCA.mapper;

import com.bgdCA.domain.ArticleReback;

import java.util.List;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/26 15:57
 */
public interface ArticleRebackMapper {
    List<ArticleReback> findAll();

    String selectName(int authorId);

    List<ArticleReback> findAllPro(String keyWord);
}
