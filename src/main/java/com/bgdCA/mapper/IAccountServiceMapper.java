package com.bgdCA.mapper;

import com.bgdCA.domain.Account;
import com.bgdCA.domain.ProfilePictureUrl;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Mapper
public interface IAccountServiceMapper {

    List<Account> getAllAccount();

    int addAccount(Account accountBean);

    int addAccount1(Account accountBean);

    int updataAccount(Account account);

    int updataAccount1(Account account);

    List<Account> getTargetAccount(String name);

    Integer getTargetAccountForId(String name);

    int deleteAccount(int id);

    int batchdeleteAccount(List list);

    int banAccount(int id);

    int startAccount(int id);

    String getPower(int id);

    int updateAccountPower(int id, int power);


//    int deletePortrait(int id);

    List<ProfilePictureUrl> getTargetPerson(String username);

    int uploadPortrait(@Param("userName") String userName,
                       @Param("userId") String userId);

    List<ProfilePictureUrl> getTargetPersonEmail(String mail);

    List<ProfilePictureUrl> getTargetPersonNickName(String name);


}