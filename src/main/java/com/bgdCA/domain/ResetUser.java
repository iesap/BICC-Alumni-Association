package com.bgdCA.domain;

public class ResetUser {
    private String username;
    private String captcha;
    private String newpsd;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }

    public String getNewpsd() {
        return newpsd;
    }

    public void setNewpsd(String newpsd) {
        this.newpsd = newpsd;
    }

    @Override
    public String toString() {
        return "ResetUser{" +
                "username='" + username + '\'' +
                ", captcha='" + captcha + '\'' +
                ", newpsd='" + newpsd + '\'' +
                '}';
    }
}
