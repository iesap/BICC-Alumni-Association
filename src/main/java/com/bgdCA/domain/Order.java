package com.bgdCA.domain;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "栏目顺序", description = "栏目顺序")
public class Order {
    private long articleId;
    private long articleId1;
    private long articleId2;
    private long columnSecondId;
    private long columnFirstId;
    private int order1;
    private long columnFirstId1;
    private long columnFirstId2;
    private long columnSecondId1;
    private long columnSecondId2;

}
