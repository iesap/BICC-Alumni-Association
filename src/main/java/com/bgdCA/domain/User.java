package com.bgdCA.domain;

/**
 * @ClassName User
 * @Description TODO
 * @Author AlvinZheng
 * @Date 2020/8/1 11:55
 * @Version 1.0
 **/

import org.springframework.boot.autoconfigure.domain.EntityScan;

/**
 * 用户登录相关实体
 */

public class User {

    private String username;
    private String password;
    private Long id;
    private String authorization;
    private int positionid;
    private int teamid;

    public User() {
    }

    public User(String username, String password, Long id, String authorization, int positionid) {
        this.username = username;
        this.password = password;
        this.id = id;
        this.authorization = authorization;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public int getPositionid() {
        return positionid;
    }

    public void setPositionid(int positionid) {
        this.positionid = positionid;
    }

    public int getTeamid() {
        return teamid;
    }

    public void setTeamid(int teamid) {
        this.teamid = teamid;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", id=" + id +
                ", authorization='" + authorization + '\'' +
                ", positionid=" + positionid +
                '}';
    }
}
