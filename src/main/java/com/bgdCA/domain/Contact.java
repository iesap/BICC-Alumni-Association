package com.bgdCA.domain;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * contact
 *
 * @author
 */
@ApiModel("联系人实体类")
@Data
public class Contact implements Serializable {
    /**
     * 联系人主键
     */
    @ApiModelProperty(value = "联系人主键", example = "9")
    private Integer contactId;

    /**
     * 联系人内容
     */
    @ApiModelProperty(value = "联系人内容", example = "新联系人")
    private String contactContent;

    /**
     * 用户名（外键关联）
     */
    @ApiModelProperty(value = "用户名（外键关联）", example = "system2")
    private String userName;

    /**
     * 联系人邮箱
     */
    @ApiModelProperty(value = "联系人邮箱", example = "8888@qq.com")
    private String contactMailAddress;

    /**
     * 分组id（外键关联）
     */
    @ApiModelProperty(value = "分组id（外键关联）", example = "1")
    private Integer groupId;

    /**
     * 分组名（联合查询）
     */
    @ApiModelProperty(value = "分组名（联合查询）", example = "分组1")
    private String groupContent;

    private static final long serialVersionUID = 1L;
}