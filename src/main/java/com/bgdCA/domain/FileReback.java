package com.bgdCA.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@ApiModel(value = "档案回溯", description = "回溯实体类")
@Data
public class FileReback {
    @ApiModelProperty(name = "回溯Id", value = "回溯Id", example = "6", dataType = "int")
    private int fileRebackId;

    @ApiModelProperty(name = "档案Id", value = "档案Id", example = "6", dataType = "int")
    private int fileId;

    @ApiModelProperty(name = "用户Id", value = "用户Id", example = "1000", dataType = "int")
    private int userId;

    @ApiModelProperty(name = "用户年份", example = "2020", dataType = "int")
    private int personalYear;

    @ApiModelProperty(name = "姓氏", example = "赵", dataType = "String")
    private String lastName;

    @ApiModelProperty(name = "名称", example = "一", dataType = "String")
    private String firstName;

    @ApiModelProperty(name = "中文名", example = "张三", dataType = "String")
    private String chineseName;

    @ApiModelProperty(name = "证件号", example = "11022419880XXXX", dataType = "String")
    private String idNumber;

    @ApiModelProperty(name = "洲际", example = "亚洲", dataType = "String")
    private String continent;

    @ApiModelProperty(name = "国籍", example = "中国", dataType = "String")
    private String national;

    @ApiModelProperty(name = "性别", example = "1", dataType = "String")
    private String gender;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(name = "生日", example = "2017-04-05", dataType = "String")
    private Date birthday;

    @ApiModelProperty(name = "年龄", example = "3", dataType = "int")
    private int entrancAge;

    @ApiModelProperty(name = "宗教", example = "基督教", dataType = "String")
    private String religion;

    @ApiModelProperty(name = "公司", example = "能力有限公司", dataType = "String")
    private String company;

    @ApiModelProperty(name = "公司(中)", example = "能力有限公司", dataType = "String")
    private String chineseCompany;

    @ApiModelProperty(name = "职位", example = "员工", dataType = "String")
    private String workDuty;

    @ApiModelProperty(name = "职位（中）", example = "员工", dataType = "String")
    private String chineseWorkDuty;

    @ApiModelProperty(name = "用户等级", example = "1", dataType = "String")
    private String level;

    @ApiModelProperty(name = "电话", example = "1808073717", dataType = "String")
    private String telephone;

    @ApiModelProperty(name = "邮箱", example = "1@163.com", dataType = "String")
    private String email;

    @ApiModelProperty(name = "参与项目", example = "民营企业", dataType = "String")
    private String participateProject;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(name = "学习开始时间", example = "2013-06-08", dataType = "String")
    private Date studyStart;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(name = "学习结束时间", example = "2017-05-06", dataType = "String")
    private Date studyEnd;

    @ApiModelProperty(name = "语言", example = "中文", dataType = "String")
    private String language;

    @ApiModelProperty(name = "部门", example = "部门", dataType = "String")
    private String department;

    @ApiModelProperty(name = "班主任", example = "段老师", dataType = "String")
    private String manager;

    @ApiModelProperty(name = "汉语水平", example = "中", dataType = "String")
    private String chineseLevel;

    @ApiModelProperty(name = "判断类型", example = "0：增加，1：修改，2：删除", dataType = "String")
    private String type;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(name = "修改时间", example = "2017-05-06", dataType = "String")
    private java.sql.Timestamp changeTime;
}
