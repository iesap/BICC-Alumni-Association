package com.bgdCA.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@ApiModel(value = "档案对象", description = "档案实体类")
@Data
public class Analysis {

    @ApiModelProperty(name = "大洲", example = "亚洲", dataType = "String")
    private String continent;

    @ApiModelProperty(name = "职位", example = "员工", dataType = "String")
    private String workDuty;
    //多少人人数

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(name = "学习开始时间", example = "2013-06-08", dataType = "String")
    private Date studyStart;
    //时间数

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(name = "学习结束时间", example = "2017-05-06", dataType = "String")
    private Date studyEnd;
    //数量

    @ApiModelProperty(name = "部门", example = "部门", dataType = "String")
    private String department;
    //数量
    @ApiModelProperty(name = "计数", example = "计数", dataType = "int")
    private int counts;
}

