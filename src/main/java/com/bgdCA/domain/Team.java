package com.bgdCA.domain;

import lombok.Data;

@Data
public class Team {
    private int teamid;
    private String teamname;
    private String teamleader;
    private int parentteamid;
    private String teamdesc;
    private String teamEmail;
    private String teamEmailName;
    private String teamEmailKey;
    private String teamEmailHost;
    private String teamEmailPort;

}
