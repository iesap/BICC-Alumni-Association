package com.bgdCA.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * (Groups)实体类
 *
 * @author sms
 * @since 2020-08-12 18:09:05
 */
@Data
@ApiModel("分组实体类")
public class Groups implements Serializable {
    private static final long serialVersionUID = -42598879759782148L;
    /**
     * 分组主键
     */
    @ApiModelProperty(value = "分组主键", example = "3")
    private Integer groupId;
    /**
     * 分组名
     */
    @ApiModelProperty(value = "分组名", example = "分组2")
    private String groupContent;
    /**
     * 用户名（外键关联）
     */
    @ApiModelProperty(value = "用户名（外键关联）", example = "system2")
    private String userName;

    /**
     * 分组信息
     */
    @ApiModelProperty(value = "分组id（外键关联）", example = "1")
    private List<ContactAndGroup> contactMsg;

}