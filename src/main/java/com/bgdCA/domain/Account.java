package com.bgdCA.domain;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(value = "账户对象", description = "账户实体类")
public class Account {
    private int loginid;
    private String username;
    private String password;
    private int positionid;
    private Date lastlogintime;
    private int actingpower;
    private int accountstatus;
    private String name;
    private String phone;
    private String mail;
    private String mailauthentication;
    private int teamid;

    public int getLoginid() {
        return loginid;
    }

    public void setLoginid(int loginid) {
        this.loginid = loginid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getPositionid() {
        return positionid;
    }

    public void setPositionid(int positionid) {
        this.positionid = positionid;
    }

    public Date getLastlogintime() {
        return lastlogintime;
    }

    public void setLastlogintime(Date lastlogintime) {
        this.lastlogintime = lastlogintime;
    }

    public int getActingpower() {
        return actingpower;
    }

    public void setActingpower(int actingpower) {
        this.actingpower = actingpower;
    }

    public int getAccountstatus() {
        return accountstatus;
    }

    public void setAccountstatus(int accountstatus) {
        this.accountstatus = accountstatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMailauthentication() {
        return mailauthentication;
    }

    public void setMailauthentication(String mailauthentication) {
        this.mailauthentication = mailauthentication;
    }

    @Override
    public String toString() {
        return "Account{" +
                "loginid=" + loginid +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", positionid=" + positionid +
                ", lastlogintime=" + lastlogintime +
                ", actingpower=" + actingpower +
                ", accountstatus=" + accountstatus +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", mail='" + mail + '\'' +
                ", mailauthentication='" + mailauthentication + '\'' +
                '}';
    }
}