package com.bgdCA.domain;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class FileInfoForExcel {
    @ExcelProperty(value = "用户Id", index = 0)
    private int userId;

    @ExcelProperty(value = "用户年份", index = 1)
    private int personalYear;

    @ExcelProperty(value = "姓氏", index = 2)
    private String lastName;

    @ExcelProperty(value = "名称", index = 3)
    private String firstName;

    @ExcelProperty(value = "洲际", index = 4)
    private String continent;

    @ExcelProperty(value = "国籍", index = 5)
    private String national;

    @ExcelProperty(value = "性别", index = 6)
    private String gender;

    @DateTimeFormat("yyyy-MM-dd")
    @ExcelProperty(value = "生日", index = 7)
    private String birthday;

    @ExcelProperty(value = "宗教", index = 8)
    private String religion;

    @ExcelProperty(value = "用户等级", index = 9)
    private String level;

    @ExcelProperty(value = "电话", index = 10)
    private String telephone;

    @ExcelProperty(value = "邮箱", index = 11)
    private String email;

    @ExcelProperty(value = "参与项目", index = 12)
    private String participateProject;

    @DateTimeFormat("yyyy-MM-dd")
    @ExcelProperty(value = "学习开始时间", index = 13)
    private String studyStart;

    @DateTimeFormat("yyyy-MM-dd")
    @ExcelProperty(value = "学习结束时间", index = 14)
    private String studyEnd;

    @ExcelProperty(value = "语言", index = 15)
    private String language;

    @ExcelProperty(value = "部门", index = 16)
    private String department;

    @ExcelProperty(value = "班主任", index = 17)
    private String manager;

    @ExcelProperty(value = "护照号", index = 18)
    private String idNumber;

    @ExcelProperty(value = "入学时年龄", index = 19)
    private String entranceAge;

    @ExcelProperty(value = "工作职务（中文）", index = 20)
    private String chineseWorkDuty;

    @ExcelProperty(value = "工作职务（英文）", index = 21)
    private String workDuty;

    @ExcelProperty(value = "工作单位（中文）", index = 22)
    private String chineseCompany;

    @ExcelProperty(value = "工作单位（英文）", index = 23)
    private String company;

    @ExcelProperty(value = "汉语水平", index = 24)
    private String chineseLevel;

    @ExcelProperty(value = "中文名",index = 25)
    private String chineseName;

    @ExcelProperty(value = "备注",index = 26)
    private String remarks;
}
