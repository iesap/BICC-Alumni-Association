package com.bgdCA.domain;

import lombok.Data;

/**
 * @author Sun Mingshan
 * @Description TODO
 * @date 2020/8/26 22:10
 */
@Data
public class Email {
    private String email;
    private String lastName;
    private String firstName;
}
