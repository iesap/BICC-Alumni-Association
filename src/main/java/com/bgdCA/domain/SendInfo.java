package com.bgdCA.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @ClassName SendInfo
 * @Description TODO
 * @Author AlvinZheng
 * @Date 2020/7/27 23:01
 * @Version 1.0
 **/
@Data
@ApiModel("站外信api")
public class SendInfo {
    @ApiModelProperty(name = "邮件接收地址", value = "邮件接收地址", example = "686878@qq.com", required = true)
    private String[] to;
    @ApiModelProperty(name = "标题", value = "标题", example = "这是一封复杂的邮件", required = true)
    private String subject;
    @ApiModelProperty(name = "内容", value = "内容", example = "<h3>hello world!这是一封html邮件！</h3>", required = true)
    private String content;
    @ApiModelProperty(name = "附件", value = "附件", example = "附件上传", required = false)
    private MultipartFile[] attachment;
    private boolean html = true;
    @ApiModelProperty(name = "附件开关", value = "附件开关", example = "false", required = false)
    private boolean useAttachment = false;
    @ApiModelProperty(name = "收件人名字（问候）", value = "收件人名字（问候）", example = "false", required = false)
    private String[] toName;

    @ApiModelProperty(name = "问候开关", value = "问候开关", example = "false", required = false)
    private boolean useGreeting = true;
    /*自定义发件人*/
    @ApiModelProperty(name = "发件人邮箱名", value = "发件人邮箱名", example = "false", required = false)
    private String fromAddress = "223337742@qq.com";
    @ApiModelProperty(name = "发件人邮箱协议（问候）", value = "发件人邮箱协议", example = "false", required = false)
    private String host;
    @ApiModelProperty(name = "发件人密码（问候）", value = "发件人密码", example = "false", required = false)
    private String password;

    public SendInfo() {
    }

    public SendInfo(String[] to, String subject, String content, MultipartFile[] attachment, boolean html, boolean useAttachment,boolean useGreeting) {
        this.to = to;
        this.subject = subject;
        this.content = content;
        this.attachment = attachment;
        this.html = html;
        this.useAttachment = useAttachment;
        this.useGreeting = useGreeting;
    }

}
