package com.bgdCA.domain;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author Sun Mingshan
 * @Description 联系人分组（一对多）封装信息
 * @date 2020/8/15 16:42
 */
@Data
public class ContactAndGroup {
    /**
     * 联系人主键
     */
    @ApiModelProperty(value = "联系人主键", example = "9")
    private Integer contactId;

    /**
     * 联系人内容
     */
    @ApiModelProperty(value = "联系人内容", example = "新联系人")
    private String contactContent;

    /**
     * 联系人邮箱
     */
    @ApiModelProperty(value = "联系人邮箱", example = "8888@qq.com")
    private String contactMailAddress;

}
