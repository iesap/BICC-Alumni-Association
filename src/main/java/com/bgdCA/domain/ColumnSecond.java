package com.bgdCA.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;


@Data
@ApiModel(value = "目录对象", description = "目录实体类")
public class ColumnSecond {

    @ApiModelProperty(name = "二级目录ID", value = "二级目录ID", example = "1", dataType = "long")
    private long columnSecondId;

    @ApiModelProperty(name = "一级栏目ID", value = "一级栏目ID", example = "1", dataType = "long")
    private long columnFirstId;

    @ApiModelProperty(name = "二级栏目", value = "二级栏目", example = "国外名著", dataType = "String")
    private String columnSecondName;

    private List<Article> articleList;

    private int order1;
}
