package com.bgdCA.domain;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(value = "文章对象", description = "文章实体类")
public class ArticleDomain {

    //文章ID
    private int articleId;
    //作者ID
    private int authorId;
    //文章标题
    private String title;
    //创建时间
    private Date createTime;
    //最后修改时间
    private Date lastChangeTime;
    //权限
    private String audit;
    //作者名称
    private String name;
    //栏目一
    private String columnFirstName;
    //栏目二
    private String columnSecondName;

    private int order1;
}
