package com.bgdCA.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("一级栏目")
public class Column {

    @ApiModelProperty(name = "一级栏目名", value = "一级栏目名", example = "相信科学", dataType = "String")
    private String columnFirstName;

    private long columnFirstId;

    private List<ColumnSecond> columnSecondList;
}

