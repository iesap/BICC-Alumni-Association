package com.bgdCA.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "职位对象", description = "职位实体类")
public class Position {

    @ApiModelProperty(name = "职位Id", value = "职位Id", example = "4", dataType = "long")
    private long positionId;

    @ApiModelProperty(name = "职位名称", value = "职位名称", example = "总经理", dataType = "String")
    private String positionName;

    @ApiModelProperty(name = "权限", value = "权限", example = "00,01,02,03,04,05,06,07", dataType = "String")
    private String power;

    @ApiModelProperty(name = "上级职位id", value = "上级职位Id", dataType = "long")
    private long higherPositionId;

    @ApiModelProperty(name = "上级名称", value = "上级名称", example = "总经理", dataType = "String")
    private String higherPositionName;

    @ApiModelProperty(name = "部门ID", value = "部门ID", dataType = "long")
    private long teamId;


}
