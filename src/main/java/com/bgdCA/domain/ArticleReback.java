package com.bgdCA.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.sql.Timestamp;

@Data
@ApiModel(value = "文章历史记录表对象", description = "文章历史记录表实体类")
public class ArticleReback {

    private long articleRebackId;
    private long authorId;
    private String title;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private java.sql.Timestamp articleCreateTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private java.sql.Timestamp lastChangeTime;
    private String audit;
    private String columnFirst;
    private String columnSecond;
    private String name;
    private String type;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private java.sql.Timestamp changeTime;

}
