package com.bgdCA.domain;

/**
 * @ClassName AuthConstants
 * @Description TODO
 * @Author AlvinZheng
 * @Date 2020/8/3 0:53
 * @Version 1.0
 **/
public class AuthConstants {


    //用户管理权限
    public static final String Account = "account";

    //档案管理，数据分析权限
    public static final String FileInfo = "fileinfo";

    //职位（角色）管理权限
    public static final String Permissions = "permissons";

    //文章管理权限
    public static final String Article = "6";

    //消息管理权限-站外信
    public static final String MailOutside = "outmail";

    //消息管理权限-站内信
    public static final String MailInside = "8";

    //项目组管理权限
    public static final String Team = "9";

    //二级审核（第一次审核）
    public static final String ExamineFirst = "704,705";

    //一级审核（第二次审核）
    public static final String ExamineSecond = "703,705";


}
