package com.bgdCA.domain;

import java.util.Date;

public class UserLoginDomain {
    private int login_id;
    private String user_name;
    private String password;
    private String power;
    private Date last_login_time;
    private int acting_power;
    private String account_status;


    @Override
    public String toString() {
        return "UserDomain{" +
                "login_id=" + login_id +
                ", user_name='" + user_name + '\'' +
                ", password='" + password + '\'' +
                ", power='" + power + '\'' +
                ", last_login_time=" + last_login_time +
                ", acting_power=" + acting_power +
                ", account_status='" + account_status + '\'' +
                '}';
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public int getLogin_id() {
        return login_id;
    }

    public void setLogin_id(int login_id) {
        this.login_id = login_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getLast_login_time() {
        return last_login_time;
    }

    public void setLast_login_time(Date last_login_time) {
        this.last_login_time = last_login_time;
    }

    public int getActing_power() {
        return acting_power;
    }

    public void setActing_power(int acting_power) {
        this.acting_power = acting_power;
    }

    public String getAccount_status() {
        return account_status;
    }

    public void setAccount_status(String account_status) {
        this.account_status = account_status;
    }
}
