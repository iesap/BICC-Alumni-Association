package com.bgdCA.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/3 21:51
 */
@Data
@ApiModel(value = "文章对象", description = "文章实体类")
public class Article {
    @ApiModelProperty(name = "文章Id", value = "文章Id", example = "1", dataType = "long")
    private long articleId;

    @ApiModelProperty(name = "作者Id", value = "作者Id", example = "1", dataType = "long")
    private long authorId;

    @ApiModelProperty(name = "标题", value = "标题", example = "钢铁是怎样炼成的", dataType = "String")
    private String title;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(name = "创建时间", value = "创建时间", example = "2019-01-01 23:59:00", dataType = "String")
    private Date createTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ApiModelProperty(name = "更新时间", value = "更新时间", example = "2019-01-01 23:59:00", dataType = "String")
    private Date lastChangeTime;

    @ApiModelProperty(name = "审核状态", value = "审核状态", example = "0", dataType = "String")
    private String audit;

    @ApiModelProperty(name = "用户Id", value = "用户Id", example = "1", dataType = "long")
    private long userId;

    @ApiModelProperty(name = "姓名", value = "姓名", example = "张三", dataType = "String")
    private String name;


    @ApiModelProperty(name = "文章代码", value = "文章代码", example = "这是一段html代码", dataType = "String")
    private String code;

    private long columnId;

    private long columnSecondId;
    @ApiModelProperty(name = "二级栏目", value = "二级栏目", example = "国外名著", dataType = "String")
    private String columnSecondName;
    private long columnFirstId;

    @ApiModelProperty(name = "一级栏目", value = "一级栏目", example = "人文类", dataType = "String")
    private String columnFirstName;

    private int order1;

    String keyWord;
}
