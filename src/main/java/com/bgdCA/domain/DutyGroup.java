package com.bgdCA.domain;

import lombok.Data;

import java.util.List;

/**
 * @author Sun Mingshan
 * @Description 站外信联系人根据档案分组
 * @date 2020/8/26 22:08
 */
@Data
public class DutyGroup {
    private String department;
    private String national;
    private List<Email> emails;
}
