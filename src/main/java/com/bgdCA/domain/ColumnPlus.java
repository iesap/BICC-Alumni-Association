package com.bgdCA.domain;

import lombok.Data;

import java.util.List;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/8/21 14:47
 */
@Data
public class ColumnPlus {
    private long columnFirstId;
    private String columnFirstName;
    private int order1;
    private List<ColumnSecond> columnSecondList;
}
