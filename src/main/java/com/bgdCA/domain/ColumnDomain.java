package com.bgdCA.domain;

import lombok.Data;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/8/17 14:03
 */
@Data
public class ColumnDomain {
    private long columnSecondId;
    private String columnSecondName;
    private long columnFirstId;
    private String columnFirstName;
    private int order1;
    private int order2;
}
