package com.bgdCA.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/29 09:04
 */
@Data
@ApiModel(value = "站内信对象", description = "站内信实体类")
public class StationLetter {
    @ApiModelProperty(name = "站内信与作者对应Id", value = "站内信与作者对应Id", example = "1", dataType = "long")
    private long id;
    @ApiModelProperty(name = "站内信Id", value = "站内信Id", example = "1", dataType = "long")
    private long messageId;
    @ApiModelProperty(name = "收件人Id", value = "收件人Id", example = "1", dataType = "long")
    private long userId;
    @ApiModelProperty(name = "是否已读", value = "已读否", example = "1", dataType = "String")
    private String type;
    @ApiModelProperty(name = "发件人id", value = "发件人id", example = "1", dataType = "long")
    private long authorId;
    @ApiModelProperty(name = "标题", value = "标题", example = "这是一个标题", dataType = "String")
    private String title;
    @ApiModelProperty(name = "内容", value = "内容", example = "这是一段话", dataType = "String")
    private String content;
    @ApiModelProperty(name = "创建时间", value = "创建时间", example = "2019-01-01 23:59:00", dataType = "String")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private java.sql.Timestamp createTime;
    private String name;
    private String username;
    private int m;
    private int n;
}
