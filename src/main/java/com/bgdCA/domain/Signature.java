package com.bgdCA.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * (Signature)实体类
 *
 * @author sms
 * @since 2020-08-12 20:11:12
 */
@Data
@ApiModel("签名实体类")
public class Signature implements Serializable {
    private static final long serialVersionUID = 570607668139812689L;
    /**
     * 签名主键
     */
    @ApiModelProperty(value = "签名主键", example = "3")
    private Integer signatureId;
    /**
     * 签名内容
     */
    @ApiModelProperty(value = "签名内容", example = "新签名")
    private String signatureContent;
    /**
     * 用户名（外键关联）
     */
    @ApiModelProperty(value = "用户名（外键关联）", example = "system2")
    private String userName;

    @ApiModelProperty(value = "签名标题", example = "新签名标题")
    private String signatureTitle;
}