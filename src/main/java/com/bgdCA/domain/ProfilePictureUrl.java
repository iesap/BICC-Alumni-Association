package com.bgdCA.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value = "头像类", description = "头像实体类")
@Data
public class ProfilePictureUrl {

    @ApiModelProperty(name = "用户Id", value = "用户Id", example = "1", dataType = "int")
    private int userId;

    @ApiModelProperty(name = "用户头像", example = "url", dataType = "String")
    private String profilePictureUrl;

    @ApiModelProperty(name = "用户名", example = "url", dataType = "String")
    private String username;

    public int getUserid() {
        return userId;
    }
}
