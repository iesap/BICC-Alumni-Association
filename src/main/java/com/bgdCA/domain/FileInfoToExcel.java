package com.bgdCA.domain;

/**
 * @Author ShiJiaWei
 * @Date 2020/9/27 15:16
 * @Version 1.0
 */
import lombok.Data;

@Data
public class FileInfoToExcel {

    private long fileId;
    private long userId;
    private long personalYear;
    private String lastName;
    private String firstName;
    private String chineseName;
    private String idNumber;
    private String continent;
    private String national;
    private String gender;
    private java.sql.Date birthday;
    private long entranceAge;
    private String religion;
    private String company;
    private String chineseCompany;
    private String workDuty;
    private String chineseWorkDuty;
    private String level;
    private String telephone;
    private String email;
    private String participateProject;
    private java.sql.Date studyStart;
    private java.sql.Date studyEnd;
    private String language;
    private String department;
    private String manager;
    private String chineseLevel;
    private String remarks;

}
