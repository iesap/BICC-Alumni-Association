package com.bgdCA.response;

/**
 * @Author: 98050
 * @Time: 2018-11-24 21:37
 * @Feature: 返回状态码
 */
public class CodeMsg {

    private int code;
    private String msg;


    /**
     * 通用的错误码
     */
    public static CodeMsg SUCCESS = new CodeMsg(1, "success");

    /**
     * 登录模块 01
     */
    public static CodeMsg LOGIN_USER_NOT_FOUND = new CodeMsg(201001, "user not found");
    public static CodeMsg PASSWORD_NOT_RIGHT = new CodeMsg(201002, "password not right");
    public static CodeMsg GENERATE_TOKEN_FAILURE = new CodeMsg(201003, "generate token failure");
    public static CodeMsg GENERATE_TOKEN_IS_EMPTY = new CodeMsg(201004, "generate token is empty");
    public static CodeMsg GENERATE_COOKIES_FAILURE = new CodeMsg(201005, "generate cookies failure");
    /**
     * 注册模块 02
     */
    public static CodeMsg REGISTER_USERNAME_ALREADY_USE = new CodeMsg(202001, "username already use");

    /**
     * 重置密码模块 03
     */
    public static CodeMsg RESET_USER_NOT_FOUND = new CodeMsg(203001, "user not found");
    public static CodeMsg RESET_MAIL_NOT_RIGHT = new CodeMsg(203002, "mail not right");
    public static CodeMsg RESET_CAPTCHA_NOT_RIGHT = new CodeMsg(203003, "captcha not right");


    /**
     * 账号管理模块 04
     */
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULETHE_SERVERFAILED_TO_UPLOAD_VIDEO = new CodeMsg(104001, "Account management module（The server）：Failed to upload video");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULETHE_SERVERFAILED_TO_FREEZE_USER = new CodeMsg(104002, "Account management module（The server）：Failed to freeze user");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULETHE_SERVERMASS_DELETION_OF_USERS_FAILED = new CodeMsg(104003, "Account management module（The server）：Mass deletion of users failed");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULETHE_SERVERFAILED_TO_MODIFY_USER_PERMISSIONS = new CodeMsg(104004, "Account management module（The server）：Failed to modify user permissions");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULETHE_SERVERFAILED_TO_DELETE_A_SINGLE_USER = new CodeMsg(104005, "Account management module（The server）：Failed to delete a single user");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_USER = new CodeMsg(104006, "Account management module（The server）：Failed to query user");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_A_SINGLE_USER = new CodeMsg(104007, "Account management module（The server）：Failed to query a single user");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULETHE_SERVERFAILED_TO_OBTAIN_USER_RIGHTS = new CodeMsg(104008, "Account management module（The server）：Failed to obtain user rights");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULETHE_SERVERFAILED_TO_MODIFY_USER_PERMISSIONS1 = new CodeMsg(104009, "Account management module（The server）：Failed to modify user permissions");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULETHE_SERVERFAILED_TO_ENABLE_USER = new CodeMsg(104010, "Account management module（The server）：Failed to enable user");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_TO_UPLOAD_VIDEO = new CodeMsg(204011, "Account management module（program）：Failed to upload video");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_TO_FREEZE_USER = new CodeMsg(204012, "Account management module（program）：Failed to freeze user");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULEPROGRAMMASS_DELETION_OF_USERS_FAILED = new CodeMsg(204013, "Account management module（program）：Mass deletion of users failed");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_TO_MODIFY_USER_PERMISSIONS = new CodeMsg(204014, "Account management module（program）：Failed to modify user permissions");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_TO_DELETE_A_SINGLE_USER = new CodeMsg(204015, "Account management module（program）：Failed to delete a single user");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_TO_QUERY_USER = new CodeMsg(204016, "Account management module（program）：Failed to query user");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_TO_QUERY_A_SINGLE_USER = new CodeMsg(204017, "Account management module（program）：Failed to query a single user");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_TO_OBTAIN_USER_RIGHTS = new CodeMsg(204018, "Account management module（program）：Failed to obtain user rights");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_TO_MODIFY_USER_PERMISSIONS1 = new CodeMsg(204019, "Account management module（program）：Failed to modify user permissions");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_TO_ENABLE_USER = new CodeMsg(204020, "Account management module（program）：Failed to enable user");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_USER_NOT_FOUND = new CodeMsg(204021, "Account management module（program）：User not found");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_USERNAME_ALREADY_USE = new CodeMsg(204022, "Account management module（program）：Username already use");
    public static CodeMsg ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_PROFILEPICTUREURL_USE = new CodeMsg(204023, "Account management module（program）：Avatar change failed");


    /**
     * 文章审核模块 05
     */
    public static CodeMsg ARTICLE_REVIEW_MODULETHE_SERVERFAILED_TO_GET_ARTICLE_INFORMATION = new CodeMsg(105001, "Article review module（The server）：Failed to get article information");
    public static CodeMsg ARTICLE_REVIEW_MODULETHE_SERVERFAILED_TO_QUERY_UN_APPROVED_ARTICLES = new CodeMsg(105002, "Article review module（The server）：Failed to query UN approved articles");
    public static CodeMsg ARTICLE_REVIEW_MODULETHE_SERVERARTICLE_FAILURE = new CodeMsg(105003, "Article review module（The server）：Article failure");
    public static CodeMsg ARTICLE_REVIEW_MODULETHE_SERVERFAILED_TO_PASS_THE_ARTICLE_BATCH = new CodeMsg(105004, "Article review module（The server）：Failed to pass the article batch");
    public static CodeMsg ARTICLE_REVIEW_MODULETHE_SERVERARTICLE_PASS_FAILURE = new CodeMsg(105005, "Article review module（The server）：Article pass failure");
    public static CodeMsg ARTICLE_REVIEW_MODULETHE_SERVERARTICLE_BATCH_PASS_FAILED = new CodeMsg(105006, "Article review module（The server）：Article batch pass failed");
    public static CodeMsg ARTICLE_REVIEW_MODULEPROGRAMFAILED_TO_GET_ARTICLE_INFORMATION = new CodeMsg(205007, "Article review module（program）：Failed to get article information");
    public static CodeMsg ARTICLE_REVIEW_MODULEPROGRAMFAILED_TO_QUERY_UN_APPROVED_ARTICLES = new CodeMsg(205008, "Article review module（program）：Failed to query UN approved articles");
    public static CodeMsg ARTICLE_REVIEW_MODULEPROGRAMARTICLE_FAILURE = new CodeMsg(205009, "Article review module（program）：Article failure");
    public static CodeMsg ARTICLE_REVIEW_MODULEPROGRAMFAILED_TO_PASS_THE_ARTICLE_BATCH = new CodeMsg(205010, "Article review module（program）：Failed to pass the article batch");
    public static CodeMsg ARTICLE_REVIEW_MODULEPROGRAMARTICLE_PASS_FAILURE = new CodeMsg(205011, "Article review module（program）：Article pass failure");
    public static CodeMsg ARTICLE_REVIEW_MODULEPROGRAMARTICLE_BATCH_PASS_FAILED = new CodeMsg(205012, "Article review module（program）：Article batch pass failed");


    /**
     * 站内信模块 06
     */
    public static CodeMsg STATION_INFORMATION_MODULETHE_SERVERFAILED_TO_DELETE_MESSAGES_IN_BATCH = new CodeMsg(106001, "Station information module（The server）：Failed to delete messages in batch");
    public static CodeMsg STATION_INFORMATION_MODULETHE_SERVERFAILED_TO_DELETE_A_SINGLE_STATION_MESSAGE = new CodeMsg(106002, "Station information module（The server）：Failed to delete a single station message");
    public static CodeMsg STATION_INFORMATION_MODULETHE_SERVERFAILED_TO_QUERY_ALL_STATION_MESSAGES = new CodeMsg(106003, "Station information module（The server）：Failed to query all station messages");
    public static CodeMsg STATION_INFORMATION_MODULETHE_SERVERSENDING_STATION_MESSAGE_FAILED = new CodeMsg(106004, "Station information module（The server）：Sending station message failed");
    public static CodeMsg STATION_INFORMATION_MODULETHE_SERVERMARK_AS_READ_FAILED = new CodeMsg(106005, "Station information module（The server）：Mark as read failed");
    public static CodeMsg STATION_INFORMATION_MODULEPROGRAMFAILED_TO_DELETE_MESSAGES_IN_BATCH = new CodeMsg(206006, "Station information module（program）：Failed to delete messages in batch");
    public static CodeMsg STATION_INFORMATION_MODULEPROGRAMFAILED_TO_DELETE_A_SINGLE_STATION_MESSAGE = new CodeMsg(206007, "Station information module（program）：Failed to delete a single station message");
    public static CodeMsg STATION_INFORMATION_MODULEPROGRAMFAILED_TO_QUERY_ALL_STATION_MESSAGES = new CodeMsg(206008, "Station information module（program）：Failed to query all station messages");
    public static CodeMsg STATION_INFORMATION_MODULEPROGRAMSENDING_STATION_MESSAGE_FAILED = new CodeMsg(206009, "Station information module（program）：Sending station message failed");
    public static CodeMsg STATION_INFORMATION_MODULEPROGRAMMARK_AS_READ_FAILED = new CodeMsg(206010, "Station information module（program）：Mark as read failed");
    public static CodeMsg FailToSelectStationLetterById = new CodeMsg(206011, "FailToSelectStationLetterById");
    public static CodeMsg FailToSelectAllOrderByTime = new CodeMsg(206012, "FailToSelectAllOrderByTime");

    /**
     * 视频上传模块 07
     */
    public static CodeMsg VIDEO_UPLOAD_MODULETHE_SERVERREQUEST_TO_UPLOAD_VIDEO_FAILED = new CodeMsg(107001, "Video upload module（The server）：Request to upload video failed");
    public static CodeMsg VIDEO_UPLOAD_MODULEPROGRAMREQUEST_TO_UPLOAD_VIDEO_FAILED = new CodeMsg(207002, "Video upload module（program）：Request to upload video failed");

    /**
     * 文章管理模块 08
     */
    public static CodeMsg ARTICLE_MANAGEMENT_MODULETHE_SERVERMASS_DELETION_FAILED = new CodeMsg(108001, "Article management module（The server）：Mass deletion failed");
    public static CodeMsg ARTICLE_MANAGEMENT_MODULETHE_SERVERSINGLE_DELETION_FAILED = new CodeMsg(108002, "Article management module（The server）：Single deletion failed");
    public static CodeMsg ARTICLE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_ALL_ARTICLES = new CodeMsg(108003, "Article management module（The server）：Failed to query all articles");
    public static CodeMsg ARTICLE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_ADD_ARTICLE = new CodeMsg(108004, "Article management module（The server）：Failed to add article");
    public static CodeMsg ARTICLE_MANAGEMENT_MODULETHE_SERVERID_QUERY_FAILED = new CodeMsg(108005, "Article management module（The server）：ID query failed");
    public static CodeMsg ARTICLE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_THE_ARTICLE_UNDER_THE_SECONDARY_COLUMN = new CodeMsg(108006, "Article management module（The server）：Failed to query the article under the secondary column");
    public static CodeMsg ARTICLE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_UPDATE_ARTICLE = new CodeMsg(108007, "Article management module（The server）：Failed to update article");
    public static CodeMsg ARTICLE_MANAGEMENT_MODULEPROGRAMMASS_DELETION_FAILED = new CodeMsg(208008, "Article management module（program）：Mass deletion failed");
    public static CodeMsg ARTICLE_MANAGEMENT_MODULEPROGRAMSINGLE_DELETION_FAILED = new CodeMsg(208009, "Article management module（program）：Single deletion failed");
    public static CodeMsg ARTICLE_MANAGEMENT_MODULEPROGRAMFAILED_TO_QUERY_ALL_ARTICLES = new CodeMsg(208010, "Article management module（program）：Failed to query all articles");
    public static CodeMsg ARTICLE_MANAGEMENT_MODULEPROGRAMFAILED_TO_ADD_ARTICLE = new CodeMsg(208011, "Article management module（program）：Failed to add article");
    public static CodeMsg ARTICLE_MANAGEMENT_MODULEPROGRAMFAILED_TO_ADD_ARTICLE_TYPE = new CodeMsg(208016, "Article management module（program）：Failed to add articleType");
    public static CodeMsg ARTICLE_MANAGEMENT_MODULEPROGRAMID_QUERY_FAILED = new CodeMsg(208012, "Article management module（program）：ID query failed");
    public static CodeMsg ARTICLE_MANAGEMENT_MODULEPROGRAMFAILED_TO_QUERY_THE_ARTICLE_UNDER_THE_FIRST_COLUMN = new CodeMsg(214016, "Article management module（program）：Failed to query the article under the first column");
    public static CodeMsg ARTICLE_MANAGEMENT_MODULEPROGRAMFAILED_TO_QUERY_THE_ARTICLE_UNDER_THE_SECONDARY_COLUMN = new CodeMsg(208013, "Article management module（program）：Failed to query the article under the secondary column");
    public static CodeMsg ARTICLE_MANAGEMENT_MODULEPROGRAMFAILED_TO_UPDATE_ARTICLE = new CodeMsg(208014, "Article management module（program）：Failed to update article");
    public static CodeMsg FailSelectArticleByAuthor = new CodeMsg(208015, "FailSelectArticleByAuthor");
    public static CodeMsg FailSelectArticleByColumnId = new CodeMsg(208016, "Failed to find article through secondary column");

    /**
     * 权限管理模块 09
     */
    public static CodeMsg AUTHORITY_MANAGEMENT_MODULETHE_SERVERFAILED_TO_ADD = new CodeMsg(109001, "Authority management module（The server）：Failed to add");
    public static CodeMsg AUTHORITY_MANAGEMENT_MODULETHE_SERVERSINGLE_DELETION_FAILED = new CodeMsg(109002, "Authority management module（The server）：Single deletion failed");
    public static CodeMsg AUTHORITY_MANAGEMENT_MODULETHE_SERVERQUERY_ALL_FAILED = new CodeMsg(109003, "Authority management module（The server）：Query all failed:Someone in this postion");
    public static CodeMsg AUTHORITY_MANAGEMENT_MODULETHE_SERVERFAILED_TO_MODIFY_PERMISSIONS = new CodeMsg(109004, "Authority management module（The server）：Failed to modify permissions");
    public static CodeMsg AUTHORITY_MANAGEMENT_MODULETHE_SERVERID_LOOKUP_PERMISSION_FAILED = new CodeMsg(109005, "Authority management module（The server）：ID lookup permission failed");
    public static CodeMsg AUTHORITY_MANAGEMENT_MODULETHE_SERVERFAILED_TO_UPDATE_ARTICLE = new CodeMsg(109006, "Authority management module（The server）：Failed to update article");
    public static CodeMsg AUTHORITY_MANAGEMENT_MODULETHE_SERVERSINGLE_DELETION_FAILED1 = new CodeMsg(109007, "Authority management module（The server）：Single deletion failed: Existing lower postion");
    public static CodeMsg AUTHORITY_MANAGEMENT_MODULEPROGRAMFAILED_TO_ADD = new CodeMsg(209007, "Authority management module（program）：Failed to add");
    public static CodeMsg AUTHORITY_MANAGEMENT_MODULEPROGRAMSINGLE_DELETION_FAILED = new CodeMsg(209008, "Authority management module（program）：Single deletion failed");
    public static CodeMsg AUTHORITY_MANAGEMENT_MODULEPROGRAMQUERY_ALL_FAILED = new CodeMsg(209009, "Authority management module（program）：Query all failed");
    public static CodeMsg AUTHORITY_MANAGEMENT_MODULEPROGRAMFAILED_TO_MODIFY_PERMISSIONS = new CodeMsg(209010, "Authority management module（program）：Failed to modify permissions");
    public static CodeMsg AUTHORITY_MANAGEMENT_MODULEPROGRAMID_LOOKUP_PERMISSION_FAILED = new CodeMsg(209011, "Authority management module（program）：ID lookup permission failed");
    public static CodeMsg AUTHORITY_MANAGEMENT_MODULEPROGRAMFAILED_TO_UPDATE_ARTICLE = new CodeMsg(209012, "Authority management module（program）：Failed to update article");


    /**
     * 文章历史记录模块 10
     */
    public static CodeMsg ARTICLE_HISTORY_MODULETHE_SERVERARTICLE_HISTORY_QUERY_FAILED = new CodeMsg(110001, "Article history module（The server）：Article history query failed");
    public static CodeMsg ARTICLE_HISTORY_MODULEPROGRAMARTICLE_HISTORY_QUERY_FAILED = new CodeMsg(210002, "Article history module（program）：Article history query failed");

    /**
     * 档案管理模块 11
     */
    public static CodeMsg FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_UPLOAD_FILE = new CodeMsg(111001, "File management module（The server）：Failed to upload file");
    public static CodeMsg FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_ADD_FILE = new CodeMsg(111002, "File management module（The server）：Failed to add file");
    public static CodeMsg FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_DELETE_FILES_IN_BATCH = new CodeMsg(111003, "File management module（The server）：Failed to delete files in batch");
    public static CodeMsg FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_DELETE_FILE_INDIVIDUALLY = new CodeMsg(111004, "File management module（The server）：Failed to delete file individually");
    public static CodeMsg FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_FILE = new CodeMsg(111005, "File management module（The server）：Failed to query file");
    public static CodeMsg FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE = new CodeMsg(111006, "File management module（The server）：Failed to query single file");
    public static CodeMsg FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_UPDATE_FILE = new CodeMsg(111007, "File management module（The server）：Failed to update file");
    public static CodeMsg FILE_MANAGEMENT_MODULEPROGRAMFAILED_TO_UPLOAD_FILE = new CodeMsg(211008, "File management module（program）：Failed to upload file");
    public static CodeMsg FILE_MANAGEMENT_MODULEPROGRAMFAILED_TO_ADD_FILE = new CodeMsg(211009, "File management module（program）：Failed to add file");
    public static CodeMsg FILE_MANAGEMENT_MODULEPROGRAMFAILED_TO_DELETE_FILES_IN_BATCH = new CodeMsg(211010, "File management module（program）：Failed to delete files in batch");
    public static CodeMsg FILE_MANAGEMENT_MODULEPROGRAMFAILED_TO_DELETE_FILE_INDIVIDUALLY = new CodeMsg(211011, "File management module（program）：Failed to delete file individually");
    public static CodeMsg FILE_MANAGEMENT_MODULEPROGRAMFAILED_TO_QUERY_FILE = new CodeMsg(211012, "File management module（program）：Failed to query file");
    public static CodeMsg FILE_MANAGEMENT_MODULEPROGRAMFAILED_TO_QUERY_SINGLE_FILE = new CodeMsg(211013, "File management module（program）：Failed to query single file");
    public static CodeMsg FILE_MANAGEMENT_MODULEPROGRAMFAILED_TO_UPDATE_FILE = new CodeMsg(211014, "File management module（program）：Failed to update file");

    /**
     * 视频管理模块 13
     */
    public static CodeMsg VIDEO_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_VIDEO = new CodeMsg(113001, "Video management module（The server）：Failed to query video");
    public static CodeMsg VIDEO_MANAGEMENT_MODULETHE_SERVERFAILED_TO_UPLOAD_VIDEO = new CodeMsg(113002, "Video management module（The server）：Failed to upload video");
    public static CodeMsg VIDEO_MANAGEMENT_MODULEPROGRAMFAILED_TO_UPDATE_FILE = new CodeMsg(213003, "Video management module（program）：Failed to update file");
    public static CodeMsg VIDEO_MANAGEMENT_MODULEPROGRAMFAILED_TO_QUERY_VIDEO = new CodeMsg(213004, "Video management module（program）：Failed to query video");

    /**
     * 站外信管理模块 12
     */
    public static CodeMsg E_MAIL_SENDING_FAILED_SERVER = new CodeMsg(123001, "off-site message module（The server）：e-mail sending failed");
    public static CodeMsg FAILED_TO_DELETE_CONTACT_SERVER = new CodeMsg(123002, "off-site message module（The server）：Failed to delete contact");
    public static CodeMsg NEW_CONTACT_FAILED_SERVER = new CodeMsg(123003, "off-site message module（The server）：New contact failed");
    public static CodeMsg FAILED_TO_DISPLAY_CONTACT_AND_GROUP_INFORMATION_SERVER = new CodeMsg(123004, "off-site message module（The server）：Failed to display contact and group information");
    public static CodeMsg FAILED_TO_DELETE_GROUPS_WITH_CONTACTS_SERVER = new CodeMsg(123005, "off-site message module（The server）：Failed to delete groups with contacts");
    public static CodeMsg FAILED_TO_CHANGE_THE_CONTACT_GROUP_SERVER = new CodeMsg(123006, "off-site message module（The server）：Failed to change the contact group");
    public static CodeMsg FAILED_TO_DELETE_SIGNATURE_SERVER = new CodeMsg(123007, "off-site message module（The server）：Failed to delete signature");
    public static CodeMsg NEW_SIGNATURE_FAILED_SERVER = new CodeMsg(123008, "off-site message module（The server）：New signature failed");
    public static CodeMsg QUERY_SIGNATURE_FAILED_SERVER = new CodeMsg(123009, "off-site message module（The server）：Query signature failed");
    public static CodeMsg DELETE_GROUP_FAILED_SERVER = new CodeMsg(123010, "off-site message module（The server）：Delete group failed");
    public static CodeMsg NEW_GROUPING_FAILED_SERVER = new CodeMsg(123011, "off-site message module（The server）：New grouping failed");
    public static CodeMsg FAILED_TO_RENAME_THE_GROUP_SERVER = new CodeMsg(123012, "off-site message module（The server）：Failed to rename the group");
    public static CodeMsg FAILED_TO_SELECT_CONTACT_BY_DUTY_SERVER = new CodeMsg(123013, "off-site message module（The server）：failed to select contact by duty");

    public static CodeMsg E_MAIL_SENDING_FAILED = new CodeMsg(212013, "off-site message module（program）：e-mail sending failed");
    public static CodeMsg FAILED_TO_DELETE_CONTACT = new CodeMsg(212014, "off-site message module（program）：Failed to delete contact");
    public static CodeMsg NEW_CONTACT_FAILED = new CodeMsg(212015, "off-site message module（program）：New contact failed");
    public static CodeMsg FAILED_TO_DISPLAY_CONTACT_AND_GROUP_INFORMATION = new CodeMsg(212016, "off-site message module（program）：Failed to display contact and group information");
    public static CodeMsg FAILED_TO_DELETE_GROUPS_WITH_CONTACTS = new CodeMsg(212017, "off-site message module（program）：Failed to delete groups with contacts");
    public static CodeMsg FAILED_TO_CHANGE_THE_CONTACT_GROUP = new CodeMsg(212018, "off-site message module（program）：Failed to change the contact group");
    public static CodeMsg FAILED_TO_DELETE_SIGNATURE = new CodeMsg(212019, "off-site message module（program）：Failed to delete signature");
    public static CodeMsg NEW_SIGNATURE_FAILED = new CodeMsg(212020, "off-site message module（program）：New signature failed");
    public static CodeMsg QUERY_SIGNATURE_FAILED = new CodeMsg(212021, "off-site message module（program）：Query signature failed");
    public static CodeMsg DELETE_GROUP_FAILED = new CodeMsg(212022, "off-site message module（program）：Delete group failed");
    public static CodeMsg NEW_GROUPING_FAILED = new CodeMsg(212023, "off-site message module（program）：New grouping failed");
    public static CodeMsg FAILED_TO_RENAME_THE_GROUP = new CodeMsg(212024, "off-site message module（program）：Failed to rename the group");
    public static CodeMsg FAILED_TO_SELECT_CONTACT_BY_DUTY = new CodeMsg(123013, "off-site message module（program）：failed to select contact by duty");
    /**
     * 栏目模块 14
     */
    public static CodeMsg FailedToQueryAllMainColumnNames = new CodeMsg(114009, "FailedToQueryAllMainColumnNames");
    public static CodeMsg DELETE_FIRST_COLUMN_FAILED = new CodeMsg(114003, "DELETE_FIRST_COLUMN_FAILED");
    public static CodeMsg DELETE_SECOND_COLUMN_FAILED = new CodeMsg(114004, "DELETE_SECOND_COLUMN_FAILED");


    public static CodeMsg COLUMN_MANAGEMENT_MODULETHE_SERVERFAILED_TO_ADD_MAINCOLUMN = new CodeMsg(114001, "column module（The server）：Failed to add main column");
    public static CodeMsg COLUMN_MANAGEMENT_MODULETHE_SERVERFAILED_TO_ADD_SUBCOLUMN = new CodeMsg(114002, "column message module（The server）：Failed to add sub column");
    public static CodeMsg FailedToQueryAllMainColumnsAndSubColumns = new CodeMsg(214019, "FailedToQueryAllMainColumnsAndSubColumns");

    public static CodeMsg FailedToQueryAllArticlesUnderTheSecondaryColumn = new CodeMsg(214020, "Failed to query all articles under the secondary column");
    public static CodeMsg FailedSelectColumnFirstAllByColumnSecondId = new CodeMsg(214021, "FailedSelectColumnFirstAllByColumnSecondId");

    public static CodeMsg FailedUpdateColumnFirstOrder = new CodeMsg(214022, "FailedUpdateColumnFirstOrder");

    /**
     * 项目组模块 15
     * */
    public static CodeMsg FailedToModuleProjectTeam = new CodeMsg(11501,"FailedToModuleProjectTeam");
    public static CodeMsg FailedToDeleteProjectTeam = new CodeMsg(11502,"FailedToDeleteProjectTeam");
    public static CodeMsg FailedToFoundProjectTeam = new CodeMsg(11503,"FailedToFoundProjectTeam");

    private CodeMsg() {

    }

    private CodeMsg(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public CodeMsg fillArgs(Object... args) {
        int code = this.code;
        String message = String.format(this.msg, args);
        return new CodeMsg(code, message);
    }

    @Override
    public String toString() {
        return "CodeMsg [code=" + code + ", msg=" + msg + "]";
    }
}
