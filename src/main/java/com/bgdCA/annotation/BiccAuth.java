package com.bgdCA.annotation;

import java.lang.annotation.*;

/**
 * @ClassName AuthAnnotation
 * @Description TODO
 * @Author AlvinZheng
 * @Date 2020/8/2 15:05
 * @Version 1.0
 **/
@Target({ElementType.ANNOTATION_TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface BiccAuth {
    String[] auth();
}
