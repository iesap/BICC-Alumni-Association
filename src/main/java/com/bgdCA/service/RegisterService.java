package com.bgdCA.service;

import com.bgdCA.domain.Account;


public interface RegisterService {


    Account findUser(String username);

    int insertUser(Account account);
}
