package com.bgdCA.service.impl;

import com.bgdCA.domain.*;
import com.bgdCA.mapper.ColumnMapper;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.domain.ColumnDomain;
import com.bgdCA.mapper.ColumnMapper;
import com.bgdCA.service.ColumnService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/8/17 18:58
 */
@Slf4j
@Service
public class ColumnServiceImpl implements ColumnService {
    @Autowired
    ColumnMapper columnMapper;

    @Override
    public Result selectAllColumnFirst() {
        log.debug("Service：Query all main column names");
        List<ColumnDomain> columnDomainList = columnMapper.selectAllColumnFirst();

        //改变顺序
        for(int i =0 ; i<columnDomainList.size()-1 ; i++) {
            for(int j=0 ; j<columnDomainList.size()-1-i ; j++) {
                if(columnDomainList.get(j).getOrder1()>columnDomainList.get(j+1).getOrder1()) {
                    ColumnDomain temp = new ColumnDomain();
                    temp=columnDomainList.get(j);
                    columnDomainList.set(j,columnDomainList.get(j+1));
                    columnDomainList.set(j+1,temp);
                }
            }
        }

        if (columnDomainList.isEmpty()) {
            return Result.error(CodeMsg.FailedToQueryAllMainColumnNames);
        }
        return Result.success(columnDomainList);
    }

    @Override
    public Result addSubColumn(ColumnSecond column) {
        log.debug("Service：Add article,articleType:" + column.toString());

        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        System.out.println(column.toString());

        try {
            int result = columnMapper.addSubColumn(column);
            if (result == 0) {
                return Result.error(CodeMsg.COLUMN_MANAGEMENT_MODULETHE_SERVERFAILED_TO_ADD_SUBCOLUMN);
            }
        } catch (Exception e) {
            return Result.error(CodeMsg.COLUMN_MANAGEMENT_MODULETHE_SERVERFAILED_TO_ADD_SUBCOLUMN);
        }
        return Result.success(null);
    }

    @Override
    public Result addMainColumn(Column column) {
        log.debug("Service：add to main column");
        log.debug("column: " + column.toString());
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        System.out.println(column.toString());
        Column columnResult=new Column();
        try {
            int result = columnMapper.addMainColumn(column);
            if (result == 0) {
                return Result.error(CodeMsg.COLUMN_MANAGEMENT_MODULETHE_SERVERFAILED_TO_ADD_SUBCOLUMN);
            }
            String name=column.getColumnFirstName();
            log.debug("呵呵"+name);
            int result1 =  columnMapper.findMainId();
            columnResult.setColumnFirstId(result1);
        } catch (Exception e) {
            return Result.error(CodeMsg.COLUMN_MANAGEMENT_MODULETHE_SERVERFAILED_TO_ADD_SUBCOLUMN);
        }
        return Result.success(columnResult);
    }

    @Override
    public int DeleteColumnFirst(int columnFirstId) {
        List<ColumnDomain> result = columnMapper.checkNull(columnFirstId);
        if (result.isEmpty()) {
            return columnMapper.DeleteColumnFirst(columnFirstId);
        }
        return 0;
    }

    @Override
    public int DeleteColumnSecond(int columnSecondId) {
        return columnMapper.DeleteColumnSecond(columnSecondId);
    }

    @Override
    public Result saveColumnFirst(ColumnDomain columnDomain){

        columnMapper.updataColumnFirst(columnDomain);

        return Result.success(null);

    }

    @Override
    public Result saveColumnSecond(ColumnDomain columnDomain){

        columnMapper.updataColumnSecond(columnDomain);

        return Result.success(null);

    }
    @Override
    public Result selectArticleByColumnFirst(ColumnSecond columnFirstId) {
        log.debug("Service：Query all second-level columns under the first-level column:"+columnFirstId);
        List<ColumnSecond> ColumnList = columnMapper.selectArticleByColumnFirst(columnFirstId);
        if(ColumnList.isEmpty()){
            return Result.error(CodeMsg.ARTICLE_MANAGEMENT_MODULEPROGRAMFAILED_TO_QUERY_THE_ARTICLE_UNDER_THE_FIRST_COLUMN);
        }
        return Result.success(ColumnList);
    }

    @Override
    public Result selectAllColumnFirstAndColumnSecond(int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize); //启动分页
        log.debug("Service：Query all main columns and sub columns");
        List<ColumnDomain> columnDomainList = columnMapper.selectAllColumnFirstAndColumnSecond();

        //改变顺序
        for(int i =0 ; i<columnDomainList.size()-1 ; i++) {
            for(int j=0 ; j<columnDomainList.size()-1-i ; j++) {
                if(columnDomainList.get(j).getOrder2()>columnDomainList.get(j+1).getOrder2()){
                    ColumnDomain temp = new ColumnDomain();
                    temp=columnDomainList.get(j);
                    columnDomainList.set(j,columnDomainList.get(j+1));
                    columnDomainList.set(j+1,temp);
                }
                if(columnDomainList.get(j).getOrder2()<=columnDomainList.get(j+1).getOrder2()){
                    if(columnDomainList.get(j).getOrder1()>columnDomainList.get(j+1).getOrder1()) {
                        ColumnDomain temp = new ColumnDomain();
                        temp=columnDomainList.get(j);
                        columnDomainList.set(j,columnDomainList.get(j+1));
                        columnDomainList.set(j+1,temp);
                    }
                }
            }
        }

        PageInfo pageInfo = new PageInfo(columnDomainList);  //把dao层放进构造方法形参里，这个对象就有了分页的所有信息
        List<FileInfo> list = pageInfo.getList(); //获取分页对象中的数据列表，主要用于if判断
        if (list.isEmpty()) {
            return Result.error(CodeMsg.FailedToQueryAllMainColumnsAndSubColumns);
        }

        return Result.success(pageInfo);
    }

    @Override
    public Result selectAllColumnFirstAndColumnSecondPlus(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum,pageSize); //启动分页
        log.debug("Service：Query all main columns and sub columns");
        List<ColumnDomain> columnDomainList = columnMapper.selectAllColumnFirstAndColumnSecond();


        List<ColumnPlus> columnPlusList= columnMapper.meaningless();
        columnPlusList.clear();
        for (ColumnDomain a:columnDomainList
             ) {
            ColumnPlus columnPlus = new ColumnPlus();
            columnPlus.setColumnFirstId(a.getColumnFirstId());
            columnPlus.setColumnFirstName(a.getColumnFirstName());
            columnPlus.setOrder1(a.getOrder1());
            ColumnSecond columnSecond=new ColumnSecond();
            columnSecond.setColumnFirstId(a.getColumnFirstId());
            columnSecond.setOrder1(a.getOrder2());
            columnPlus.setColumnSecondList(columnMapper.selectArticleByColumnFirst(columnSecond));
            columnPlusList.add(columnPlus);
        }
        HashSet<ColumnPlus> set = new HashSet<ColumnPlus>(columnPlusList.size());
        List<ColumnPlus> result = new ArrayList<ColumnPlus>(columnPlusList.size());
        for (ColumnPlus str : columnPlusList) {
            if (set.add(str)) {
                result.add(str);
            }
        }
        columnPlusList.clear();
        columnPlusList.addAll(result);

        //改变顺序
        for(int i =0 ; i<columnPlusList.size()-1 ; i++) {
            for(int j=0 ; j<columnPlusList.size()-1-i ; j++) {
                if(columnPlusList.get(j).getOrder1()>columnPlusList.get(j+1).getOrder1()){
                    ColumnPlus temp = new ColumnPlus();
                    temp=columnPlusList.get(j);
                    columnPlusList.set(j,columnPlusList.get(j+1));
                    columnPlusList.set(j+1,temp);
                }
            }
        }
        for (ColumnPlus a:columnPlusList
             ) {
            for(int i =0 ; i<a.getColumnSecondList().size()-1 ; i++) {
                for(int j=0 ; j<a.getColumnSecondList().size()-1-i ; j++) {
                    if(a.getColumnSecondList().get(j).getOrder1()>a.getColumnSecondList().get(j+1).getOrder1()){
                        ColumnSecond temp = new ColumnSecond();
                        temp=a.getColumnSecondList().get(j);
                        a.getColumnSecondList().set(j,a.getColumnSecondList().get(j+1));
                        a.getColumnSecondList().set(j+1,temp);
                    }
                }
            }
        }

        PageInfo pageInfo = new PageInfo(columnPlusList);  //把dao层放进构造方法形参里，这个对象就有了分页的所有信息
        List<FileInfo> list = pageInfo.getList(); //获取分页对象中的数据列表，主要用于if判断
        if (list.isEmpty()) {
            return Result.error(CodeMsg.FailedToQueryAllMainColumnsAndSubColumns);
        }


        return Result.success(pageInfo);
    }

    @Override
    public Result selectAllColumnFirstAndColumnSecondPro() {
        log.debug("Service：Query all main columns and sub columns");
        List<ColumnDomain> columnDomainList = columnMapper.selectAllColumnFirstAndColumnSecond();
        if (columnDomainList.isEmpty()) {
            return Result.error(CodeMsg.FailedToQueryAllMainColumnsAndSubColumns);
        }

        //改变顺序
        for(int i =0 ; i<columnDomainList.size()-1 ; i++) {
            for(int j=0 ; j<columnDomainList.size()-1-i ; j++) {
                if(columnDomainList.get(j).getOrder2()>columnDomainList.get(j+1).getOrder2()){
                    ColumnDomain temp = new ColumnDomain();
                    temp=columnDomainList.get(j);
                    columnDomainList.set(j,columnDomainList.get(j+1));
                    columnDomainList.set(j+1,temp);
                }
                if(columnDomainList.get(j).getOrder2()<=columnDomainList.get(j+1).getOrder2()){
                    if(columnDomainList.get(j).getOrder1()>columnDomainList.get(j+1).getOrder1()) {
                        ColumnDomain temp = new ColumnDomain();
                        temp=columnDomainList.get(j);
                        columnDomainList.set(j,columnDomainList.get(j+1));
                        columnDomainList.set(j+1,temp);
                    }
                }
            }
        }

        return Result.success(columnDomainList);
    }

    @Override
    public Result selectAllColumnFirstAndColumnSecondPlusPro() {
        log.debug("Service：Query all main columns and sub columns");
        List<ColumnDomain> columnDomainList = columnMapper.selectAllColumnFirstAndColumnSecond();


        List<ColumnPlus> columnPlusList= columnMapper.meaningless();
        columnPlusList.clear();
        for (ColumnDomain a:columnDomainList
        ) {
            ColumnPlus columnPlus = new ColumnPlus();
            columnPlus.setColumnFirstId(a.getColumnFirstId());
            columnPlus.setColumnFirstName(a.getColumnFirstName());
            columnPlus.setOrder1(a.getOrder1());
            ColumnSecond columnSecond=new ColumnSecond();
            columnSecond.setColumnFirstId(a.getColumnFirstId());
            columnSecond.setOrder1(a.getOrder2());
            columnPlus.setColumnSecondList(columnMapper.selectArticleByColumnFirst(columnSecond));
            columnPlusList.add(columnPlus);
        }
        HashSet<ColumnPlus> set = new HashSet<ColumnPlus>(columnPlusList.size());
        List<ColumnPlus> result = new ArrayList<ColumnPlus>(columnPlusList.size());
        for (ColumnPlus str : columnPlusList) {
            if (set.add(str)) {
                result.add(str);
            }
        }
        columnPlusList.clear();
        columnPlusList.addAll(result);

        //改变顺序
        for(int i =0 ; i<columnPlusList.size()-1 ; i++) {
            for(int j=0 ; j<columnPlusList.size()-1-i ; j++) {
                if(columnPlusList.get(j).getOrder1()>columnPlusList.get(j+1).getOrder1()){
                    ColumnPlus temp = new ColumnPlus();
                    temp=columnPlusList.get(j);
                    columnPlusList.set(j,columnPlusList.get(j+1));
                    columnPlusList.set(j+1,temp);
                }
            }
        }
        for (ColumnPlus a:columnPlusList
        ) {
            for(int i =0 ; i<a.getColumnSecondList().size()-1 ; i++) {
                for(int j=0 ; j<a.getColumnSecondList().size()-1-i ; j++) {
                    if(a.getColumnSecondList().get(j).getOrder1()>a.getColumnSecondList().get(j+1).getOrder1()){
                        ColumnSecond temp = new ColumnSecond();
                        temp=a.getColumnSecondList().get(j);
                        a.getColumnSecondList().set(j,a.getColumnSecondList().get(j+1));
                        a.getColumnSecondList().set(j+1,temp);
                    }
                }
            }
        }

        if (columnPlusList.isEmpty()) {
            return Result.error(CodeMsg.FailedToQueryAllMainColumnsAndSubColumns);
        }


        return Result.success(columnPlusList);
    }

    @Override
    public Result selectColumnFirstAllByColumnSecondId(int columnSecondId,int pageNum,int pageSize) {
        PageHelper.startPage(pageNum,pageSize); //启动分页
        log.debug("Service：Through the secondary column ID single check,columnSecondId"+columnSecondId);
        List<Column> columnList = columnMapper.selectColumnFirstAllByColumnSecondId(columnSecondId);
        for (Column a:columnList
             ) {
            ColumnSecond columnSecond = new ColumnSecond();
            columnSecond.setColumnFirstId(a.getColumnFirstId());
            a.setColumnSecondList(columnMapper.selectArticleByColumnFirst(columnSecond));
        }

        HashSet<Column> set = new HashSet<Column>(columnList.size());
        List<Column> result = new ArrayList<Column>(columnList.size());
        for (Column str : columnList) {
            if (set.add(str)) {
                result.add(str);
            }
        }
        columnList.clear();
        columnList.addAll(result);
        PageInfo pageInfo = new PageInfo(columnList);  //把dao层放进构造方法形参里，这个对象就有了分页的所有信息
        List<FileInfo> list = pageInfo.getList(); //获取分页对象中的数据列表，主要用于if判断
        if (list.isEmpty()) {
            return Result.error(CodeMsg.FailedSelectColumnFirstAllByColumnSecondId);
        }
        return Result.success(pageInfo);
    }

    @Override
    public Result updateColumnFirstOrder(Order order) {
        int columnFirstId1= (int) order.getColumnFirstId1();
        int columnFirstId2= (int) order.getColumnFirstId2();
        log.debug("Service：updateColumnFirstOrder,order"+order.toString());
        int order11=columnMapper.selectOrderByFID(columnFirstId1);
        log.debug("order11:"+order11);
        int order12=columnMapper.selectOrderByFID(columnFirstId2);
        log.debug("order12:"+order12);
        Order order1=new Order();
        order1.setOrder1(order11);
        order1.setColumnFirstId(columnFirstId2);
        Order order2=new Order();
        order2.setOrder1(order12);
        order2.setColumnFirstId(columnFirstId1);
        int update1=columnMapper.updateColumnFirstOrder(order1);
        int update2=columnMapper.updateColumnFirstOrder(order2);
        log.debug("update1:"+update1);
        log.debug("update2:"+update2);
        if (!((update1==1) && (update2==1))) {
            return Result.error(CodeMsg.FailedUpdateColumnFirstOrder);
        }
        return Result.success(null);
    }

    @Override
    public Result updateColumnSecondOrder(Order order) {
        int columnSecondId1= (int) order.getColumnSecondId1();
        int columnSecondId2= (int) order.getColumnSecondId2();
        log.debug("Service：updateColumnSecondOrder,order"+order.toString());
        int order11=columnMapper.selectOrderBySID(columnSecondId1);
        log.debug("order11:"+order11);
        int order12=columnMapper.selectOrderBySID(columnSecondId2);
        log.debug("order12:"+order12);
        Order order1=new Order();
        order1.setOrder1(order11);
        order1.setColumnSecondId(columnSecondId2);
        Order order2=new Order();
        order2.setOrder1(order12);
        order2.setColumnSecondId(columnSecondId1);
        int update1=columnMapper.updateColumnSecondOrder(order1);
        int update2=columnMapper.updateColumnSecondOrder(order2);
        log.debug("update1:"+update1);
        log.debug("update2:"+update2);
        if (!((update1==1) && (update2==1))) {
            return Result.error(null);
        }
        return Result.success(null);
    }
}