package com.bgdCA.service.impl;


import com.bgdCA.domain.Account;
import com.bgdCA.domain.ProfilePictureUrl;
import com.bgdCA.mapper.IAccountServiceMapper;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.AccountManageService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class AccountManageServiceImpl implements AccountManageService {

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private IAccountServiceMapper accountServiceMapper;


    //查询所有账户
    @Override
    public HashMap<String, Object> findAll(int pageNum, int pageSize) {
        //  log.debug("Service:");
        PageHelper.startPage(pageNum, pageSize);
        PageInfo pageInfo = new PageInfo(accountServiceMapper.getAllAccount());
        HashMap<String, Object> hashMap = new HashMap<String, Object>();


        if (pageInfo.getList().isEmpty()) {
            hashMap.put("msg", "0");
            hashMap.put("AccountInfo", pageInfo);
            return hashMap;
        }
        hashMap.put("msg", "1");
        hashMap.put("AccountInfo", pageInfo);
        return hashMap;
    }

    //查询单个账户
    @Override
    public HashMap<String, Object> findTarget(String name) {
        // log.debug("Service:"+name);
        HashMap<String, Object> hashMap = new HashMap<String, Object>();

        List<Account> list = accountServiceMapper.getTargetAccount(name);

        if (list.isEmpty()) {
            hashMap.put("msg", "0");
            hashMap.put("Accountlist", list);
            return hashMap;
        }
        hashMap.put("Accountlist", list);
        hashMap.put("msg", "1");

        return hashMap;
    }

    //添加账户信息
    @Override
    public HashMap<String, Object> addAccount(Account account) {
        log.debug("Service:" + account.toString());
        System.out.println(account.toString());
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        String name = account.getUsername();
        //  System.out.println(name);
        List<Account> list = accountServiceMapper.getTargetAccount(name);
        if (!list.isEmpty()) {

            hashMap.put("msg", "0");
            return hashMap;
        }

        if (redisTemplate.hasKey(name)) {
            hashMap.put("msg", "0");
            return hashMap;
        }
        accountServiceMapper.addAccount(account);
        accountServiceMapper.addAccount1(account);
        hashMap.put("msg", "1");

        return hashMap;


    }

    //修改账户信息
    @Override
    public HashMap<String, Object> updataAccount(Account account) {
        log.debug("Service:" + account.toString());

        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        String name = account.getUsername();
        Integer id = account.getLoginid();
        //System.out.println(name);
        //  System.out.println(id);
        List<Account> list = accountServiceMapper.getTargetAccount(name);

        //修改后用户名无重复
        if (list.isEmpty()) {
            if (!redisTemplate.hasKey(name)) {
                accountServiceMapper.updataAccount(account);
                accountServiceMapper.updataAccount1(account);
                hashMap.put("msg", "1");
                return hashMap;
            }
            hashMap.put("msg", "0");
            return hashMap;
        }

        //修改后用户名与之前一致
        Integer oldid = accountServiceMapper.getTargetAccountForId(name);
        //    System.out.println(oldid);
        if (oldid == id) {
            accountServiceMapper.updataAccount(account);
            accountServiceMapper.updataAccount1(account);
            hashMap.put("msg", "1");
            return hashMap;
        }

        hashMap.put("msg", "0");
        return hashMap;


    }

    //删除账户信息
    @Override
    public HashMap<String, Object> deleteAccount(int id) {
        log.debug("Service:" + id);

        HashMap<String, Object> hashMap = new HashMap<String, Object>();

        int isDelete = accountServiceMapper.deleteAccount(id);

        hashMap.put("msg", "1");
        return hashMap;
    }

    //批量删除账户信息
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackForClassName = "Exception")
    @Override
    public HashMap<String, Object> batchdeleteAccount(List list) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();

        try {
            int result = accountServiceMapper.batchdeleteAccount(list);
            if (result != list.size()) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                hashMap.put("msg", "0");
                return hashMap;
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            hashMap.put("msg", "0");
            return hashMap;
        }
        hashMap.put("msg", "1");

        return hashMap;
    }

    //停用账户
    @Override
    public HashMap<String, Object> banAccount(int id) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        accountServiceMapper.banAccount(id);
        hashMap.put("msg", "1");
        return hashMap;
    }

    //启用账户
    @Override
    public HashMap<String, Object> startAccount(int id) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        accountServiceMapper.startAccount(id);
        hashMap.put("msg", "1");
        return hashMap;
    }

    //修改职位权限
    @Override
    public HashMap<String, Object> updateAccountPower(int id, int power) {
        //  log.debug("Service:"+account.toString());

        HashMap<String, Object> hashMap = new HashMap<String, Object>();

        if (accountServiceMapper.getPower(id).isEmpty()) {

            hashMap.put("msg", "0");

        }

        accountServiceMapper.updateAccountPower(id, power);
        hashMap.put("msg", "1");
        return hashMap;

    }

    //查询职位信息
    @Override
    public HashMap<String, Object> getPower(int id) {

        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        if (accountServiceMapper.getPower(id).isEmpty()) {

            hashMap.put("msg", "0");

        }
        String id1 = accountServiceMapper.getPower(id);
        hashMap.put("msg", "1");
        hashMap.put("power", id1);
        return hashMap;
    }


    @Override
    public Result uploadPortrait(MultipartFile file, String userName, String userId) {
        if (file.isEmpty()) {
            return Result.error(CodeMsg.ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_PROFILEPICTUREURL_USE);
        }
        // 文件名
        String originalFileName = file.getOriginalFilename();
        String nameSuffix = originalFileName.substring(originalFileName.lastIndexOf("."), originalFileName.length());
        String fileName = userName + nameSuffix;
        // 上传后的路径
        String filePath = "C://portrait//";
        // 创建文件
        File dest = new File(filePath + fileName);
        // 文件夹是否为空
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdirs();
        }
        try {
            //将文件存到目标路径
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //文件路径
        String pathName = "C:/portrait/" + fileName;
        accountServiceMapper.uploadPortrait(pathName, userId);
        return Result.success(fileName);
    }

    //查询联系人
    @Override
    public HashMap<String, Object> findPerson(String username) {
        // log.debug("Service:"+name);
        HashMap<String, Object> hashMap = new HashMap<String, Object>();

        List<ProfilePictureUrl> list = accountServiceMapper.getTargetPerson(username);

        if (list.isEmpty()) {
            hashMap.put("msg", "0");
            hashMap.put("Accountlist", list);
            return hashMap;
        }
        hashMap.put("Accountlist", list);
        hashMap.put("msg", "1");

        return hashMap;
    }

    //查询联系人
    @Override
    public HashMap<String, Object> findPersonEmail(String mail) {
        // log.debug("Service:"+name);
        HashMap<String, Object> hashMap = new HashMap<String, Object>();

        List<ProfilePictureUrl> list = accountServiceMapper.getTargetPersonEmail(mail);

        if (list.isEmpty()) {
            hashMap.put("msg", "0");
            hashMap.put("Accountlist", list);
            return hashMap;
        }
        hashMap.put("Accountlist", list);
        hashMap.put("msg", "1");

        return hashMap;
    }

    //查询联系人
    @Override
    public HashMap<String, Object> findPersonNickName(String name) {
        // log.debug("Service:"+name);
        HashMap<String, Object> hashMap = new HashMap<String, Object>();

        List<ProfilePictureUrl> list = accountServiceMapper.getTargetPersonNickName(name);

        if (list.isEmpty()) {
            hashMap.put("msg", "0");
            hashMap.put("Accountlist", list);
            return hashMap;
        }
        hashMap.put("Accountlist", list);
        hashMap.put("msg", "1");

        return hashMap;
    }
}



