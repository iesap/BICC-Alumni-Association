package com.bgdCA.service.impl;

import com.bgdCA.domain.StationLetter;
import com.bgdCA.mapper.StationLetterMapper;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.StationLetterService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/29 09:27
 */
@Slf4j
@Service
public class StationLetterServiceImpl implements StationLetterService {
    @Autowired
    StationLetterMapper stationLetterMapper;

    @Override
    public Result selectAllByUserId(int userId, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize); //启动分页
        log.debug("Service：Query all the messages of the recipient");
        List<StationLetter> stationLetterList = stationLetterMapper.selectAllByUserId(userId);

        PageInfo pageInfo = new PageInfo(stationLetterList);  //把dao层放进构造方法形参里，这个对象就有了分页的所有信息
        List<StationLetter> list = pageInfo.getList(); //获取分页对象中的数据列表，主要用于if判断
        if (list.isEmpty()) {
            return Result.error(CodeMsg.STATION_INFORMATION_MODULEPROGRAMFAILED_TO_QUERY_ALL_STATION_MESSAGES);
        }
        return Result.success(pageInfo);
    }

    @Override
    public Result deleteStationLetterById(int id) {
        log.debug("Service：Single delete station message,id:" + id);

        int isDelete = stationLetterMapper.deleteStationLetterById(id);

        if (isDelete == 0) {
            return Result.error(CodeMsg.STATION_INFORMATION_MODULEPROGRAMFAILED_TO_DELETE_A_SINGLE_STATION_MESSAGE);
        }
        return Result.success(null);
    }

    @Override
    public Result selectAllByUserIdPlus(int userId) {
        log.debug("Service：Query all the messages of the recipient");
        List<StationLetter> stationLetterList = stationLetterMapper.selectAllByUserId(userId);
        if (stationLetterList.isEmpty()) {
            return Result.error(CodeMsg.STATION_INFORMATION_MODULEPROGRAMFAILED_TO_QUERY_ALL_STATION_MESSAGES);
        }
        return Result.success(stationLetterList);
    }

    @Override
    public Result batchDeleteStationLetter(List list) {
        log.debug("Service：Delete messages in batches,list:" + list.toString());
        int isDelete = stationLetterMapper.batchDeleteStationLetter(list);

        if (isDelete == 0) {
            return Result.error(CodeMsg.STATION_INFORMATION_MODULEPROGRAMFAILED_TO_DELETE_MESSAGES_IN_BATCH);
        }
        return Result.success(null);
    }

    @Override
    public Result updateStationLetterToRead(int id) {
        log.debug("Service：Mark the site beacon as read,id:" + id);
        int isUpdate = stationLetterMapper.updateStationLetterToRead(id);
        if (isUpdate == 0) {
            return Result.error(CodeMsg.STATION_INFORMATION_MODULEPROGRAMMARK_AS_READ_FAILED);
        }
        return Result.success(null);
    }

    @Override
    public Result sendMessage(StationLetter stationLetter) {
        log.debug("Service：send Message,stationLetter:" + stationLetter.toString());
        int isInsert1 = stationLetterMapper.insertMessage(stationLetter);
        long messageId = stationLetterMapper.selectStationLetterId();
        stationLetter.setMessageId(messageId);
        int isInsert2 = stationLetterMapper.insertUserMessage(stationLetter);
        if (isInsert1 == 0 || isInsert2 == 0) {
            return Result.error(CodeMsg.STATION_INFORMATION_MODULEPROGRAMSENDING_STATION_MESSAGE_FAILED);
        }
        return Result.success(null);
    }

    @Override
    public Result selectStationLetterById(int id) {
        log.debug("Service：Find content by ID");
        StationLetter stationLetter = stationLetterMapper.selectStationLetterById(id);
        if (stationLetter == null) {
            return Result.error(CodeMsg.FailToSelectStationLetterById);
        }
        return Result.success(stationLetter);
    }

    @Override
    public Result selectAllOrderByTime(StationLetter stationLetter) {
        log.debug("Service：Find the message in the station and generate the chat record：stationLetter" + stationLetter.toString());
        List<StationLetter> stationLetterList = stationLetterMapper.selectAllOrderByTime(stationLetter);
        if (stationLetterList.isEmpty()) {
            return Result.error(CodeMsg.FailToSelectAllOrderByTime);
        }
        return Result.success(stationLetterList);
    }
}
