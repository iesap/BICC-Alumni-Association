package com.bgdCA.service.impl;

import com.bgdCA.domain.Program;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.ProgramService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProgramServiceImpl implements ProgramService {
    @Autowired
    private com.bgdCA.mapper.ProgramMapper programMapper;

    @Override
    public Result modifyProgram(Program program) {
        int result = programMapper.modifyProgram(program);
        if (result == 0) {
            return Result.error(CodeMsg.FAILED_TO_CHANGE_THE_CONTACT_GROUP);
        }
        return Result.success(null);
    }

    @Override
    public Result deleteProgram(int programId) {
        try {
            int result = programMapper.deleteProgram(programId);
            if (result == 0) {
                Result.error(CodeMsg.FAILED_TO_CHANGE_THE_CONTACT_GROUP);
            }
        } catch (Exception e) {
            Result.error(CodeMsg.FAILED_TO_CHANGE_THE_CONTACT_GROUP);
        }
        return Result.success(null);
    }

    @Override
    public Result foundProgram() {
        List<Program> list=programMapper.findAll();
        if (list.isEmpty()) {
            return Result.error(CodeMsg.FailedToFoundProjectTeam);
        }
        return Result.success(list);
    }
}
