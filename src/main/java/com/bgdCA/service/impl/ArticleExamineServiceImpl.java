package com.bgdCA.service.impl;


import com.bgdCA.domain.Article;
import com.bgdCA.domain.ArticleDomain;
import com.bgdCA.domain.UserLoginDomain;
import com.bgdCA.mapper.ArticleExamineMapper;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.ArticleExamineService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Slf4j
@Service
public class ArticleExamineServiceImpl implements ArticleExamineService {

    @Autowired
    private ArticleExamineMapper articleExamineMapper;

    //查询文章详情
    @Override
    public Result findTarget(String key) {
        log.debug("Service: Examine module Select appoint article by artcileId： " + key);
        List<ArticleDomain> articleDomain = articleExamineMapper.findTarget(key);
        if (articleDomain.isEmpty()) {
            return Result.error(CodeMsg.ARTICLE_REVIEW_MODULEPROGRAMFAILED_TO_GET_ARTICLE_INFORMATION);
        }
        return Result.success(articleDomain);
    }

    @Override
    public Result findNoPass(int userId, String key, int pageNum, int pageSize) {
        log.debug("Service: Examine module Select all no pass artcile by userId: " + userId + "--key: " + key);
        UserLoginDomain userlist = articleExamineMapper.checkPower(userId);
        if (userlist == null) {
            return Result.error(CodeMsg.ARTICLE_REVIEW_MODULEPROGRAMFAILED_TO_QUERY_UN_APPROVED_ARTICLES);
        }
        System.out.println(userlist.getPower());
        if (userlist.getPower().indexOf("703") != -1) {
            log.debug("articleList:" + articleExamineMapper.findFirstNoPass(userId, key));
            PageHelper.startPage(pageNum, pageSize);
            List<ArticleDomain> articleList = articleExamineMapper.findFirstNoPass(userId, key);
            PageInfo pageInfo = new PageInfo(articleList);
            List<ArticleDomain> list = pageInfo.getList();
            if (list.isEmpty()) {
                return Result.error(CodeMsg.ARTICLE_REVIEW_MODULEPROGRAMFAILED_TO_QUERY_UN_APPROVED_ARTICLES);
            }
            return Result.success(pageInfo);
        } else if (userlist.getPower().indexOf("704") != -1) {
            log.debug("articleList:" + articleExamineMapper.findSecondNoPass(userId, key));
            PageHelper.startPage(pageNum, pageSize);
            PageInfo pageInfo = new PageInfo(articleExamineMapper.findSecondNoPass(userId, key));
            List<ArticleDomain> list = pageInfo.getList();
            if (list.isEmpty()) {
                return Result.error(CodeMsg.ARTICLE_REVIEW_MODULEPROGRAMFAILED_TO_QUERY_UN_APPROVED_ARTICLES);
            }
            return Result.success(pageInfo);
        }
        return Result.error(CodeMsg.ARTICLE_REVIEW_MODULEPROGRAMFAILED_TO_QUERY_UN_APPROVED_ARTICLES);
    }


    @Override
    public Result pass(int articleId) {
        log.debug("Service: Examine module Update article pass examine by artcileId: " + articleId);
        List<ArticleDomain> audit = articleExamineMapper.checkAudit(articleId);
        if (audit.isEmpty()) {
            return Result.error(CodeMsg.ARTICLE_REVIEW_MODULEPROGRAMARTICLE_PASS_FAILURE);
        }
        if (audit.get(0).getAudit().equals("1")) {
            log.debug("Update " + articleId + " audit to 2");
            return Result.success(articleExamineMapper.firstPass(articleId));
        } else if (audit.get(0).getAudit().equals("2")) {
            log.debug("Update " + articleId + " audit to 3");
            return Result.success(articleExamineMapper.secondPass(articleId));
        }
        return Result.error(CodeMsg.ARTICLE_REVIEW_MODULEPROGRAMARTICLE_PASS_FAILURE);
    }

    @Override
    public Result noPass(int articleId) {
        log.debug("Service: Examine module Update article no pass examine by artcileId: " + articleId);
        List<ArticleDomain> audit = articleExamineMapper.checkAudit(articleId);
        if (audit.isEmpty()) {
            return Result.error(CodeMsg.ARTICLE_REVIEW_MODULEPROGRAMARTICLE_FAILURE);
        }
        if (audit.get(0).getAudit().equals("1")) {
            log.debug("Update artcileId: " + articleId + " audit to 0");
            return Result.success(articleExamineMapper.firstNoPass(articleId));
        } else if (audit.get(0).getAudit().equals("2")) {
            log.debug("Update " + articleId + " audit to 1");
            return Result.success(articleExamineMapper.secondNoPass(articleId));
        }
        return Result.error(CodeMsg.ARTICLE_REVIEW_MODULEPROGRAMARTICLE_FAILURE);
    }

    @Override
    public Result passAllSelect(List list) {
        log.debug("Service: Examine module Update article no pass examine by artcileId: " + list);
        List<ArticleDomain> audit = articleExamineMapper.checkAudit((int) list.get(0));
        if (audit.isEmpty()) {
            return Result.error(CodeMsg.ARTICLE_REVIEW_MODULEPROGRAMARTICLE_BATCH_PASS_FAILED);
        }
        if (audit.get(0).getAudit().equals("1")) {
            log.debug("Update artcileId: " + list + " audit to 2");
            return Result.success(articleExamineMapper.passAllFirstSelect(list));
        } else if (audit.get(0).getAudit().equals("2")) {
            log.debug("Update artcileId: " + list + " audit to 3");
            return Result.success(articleExamineMapper.passAllSecondSelect(list));
        }
        return Result.error(CodeMsg.ARTICLE_REVIEW_MODULEPROGRAMARTICLE_BATCH_PASS_FAILED);
    }

    @Override
    public Result noPassAllSelect(List list) {
        log.debug("Service: Examine module Update article no pass examine by artcileId: " + list);
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        List<ArticleDomain> audit = articleExamineMapper.checkAudit((int) list.get(0));
        if (audit.isEmpty()) {
            return Result.error(CodeMsg.ARTICLE_REVIEW_MODULEPROGRAMFAILED_TO_PASS_THE_ARTICLE_BATCH);
        }
        if (audit.get(0).getAudit().equals("1")) {
            log.debug("Update artcileId: " + list + " audit to 0");
            return Result.success(articleExamineMapper.noPassAllFirstSelect(list));
        } else if (audit.get(0).getAudit().equals("2")) {
            log.debug("Update artcileId: " + list + " audit to 1");
            return Result.success(articleExamineMapper.noPassAllSecondSelect(list));
        }
        return Result.error(CodeMsg.ARTICLE_REVIEW_MODULEPROGRAMFAILED_TO_PASS_THE_ARTICLE_BATCH);
    }

    public Result findAllPass(String columnSecondName){
        List<ArticleDomain> articleDomains = articleExamineMapper.findAllPass(columnSecondName);
        if(articleDomains.isEmpty()){
            //暂定
            return Result.error(CodeMsg.ARTICLE_MANAGEMENT_MODULEPROGRAMFAILED_TO_QUERY_ALL_ARTICLES);
        }
        return Result.success(articleDomains);
    }

}
