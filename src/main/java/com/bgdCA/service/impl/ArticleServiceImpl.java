package com.bgdCA.service.impl;

import com.bgdCA.domain.Article;
import com.bgdCA.domain.ColumnSecond;
import com.bgdCA.domain.Order;
import com.bgdCA.mapper.IArticleServiceMapper;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.IArticleService;
import com.bgdCA.util.FileToString;
import com.bgdCA.util.StringToFile;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/3 22:06
 */
@Slf4j
@Service
public class ArticleServiceImpl implements IArticleService {
    @Autowired
    private IArticleServiceMapper iArticleServiceMapper;

    @Override
    public Result findAll(int pageNum, int pageSize) {
        log.debug("Service：The article module queries all article information");
        PageHelper.startPage(pageNum, pageSize); //启动分页
        List<Article> ArticleList = iArticleServiceMapper.findAll();
        for (Article a : ArticleList
        ) {
            Article b = iArticleServiceMapper.selectColumnName((int) a.getArticleId());
            a.setColumnSecondId(b.getColumnSecondId());
            a.setColumnSecondName(b.getColumnSecondName());
            a.setColumnFirstId(b.getColumnFirstId());
            a.setColumnFirstName(b.getColumnFirstName());
        }
        PageInfo pageInfo = new PageInfo(ArticleList);  //把dao层放进构造方法形参里，这个对象就有了分页的所有信息
        List<Article> list = pageInfo.getList(); //获取分页对象中的数据列表，主要用于if判断
        if (list.isEmpty()) {
            return Result.error(CodeMsg.ARTICLE_MANAGEMENT_MODULEPROGRAMFAILED_TO_QUERY_ALL_ARTICLES);
        }
        return Result.success(pageInfo);
    }

    @Override
    public Result findAllByKey(String keyWord, int pageNum, int pageSize) {
        log.debug("Service：The article module queries all article information");
        String indexKeyWord = "%" + keyWord + "%";
        PageHelper.startPage(pageNum, pageSize); //启动分页
        List<Article> ArticleList = iArticleServiceMapper.findAllByKey(indexKeyWord);
        for (Article a : ArticleList
        ) {
            Article b = iArticleServiceMapper.selectColumnName((int) a.getArticleId());
            a.setColumnSecondId(b.getColumnSecondId());
            a.setColumnSecondName(b.getColumnSecondName());
            a.setColumnFirstId(b.getColumnFirstId());
            a.setColumnFirstName(b.getColumnFirstName());
        }
        PageInfo pageInfo = new PageInfo(ArticleList);  //把dao层放进构造方法形参里，这个对象就有了分页的所有信息
        List<Article> list = pageInfo.getList(); //获取分页对象中的数据列表，主要用于if判断
        if (list.isEmpty()) {
            return Result.error(CodeMsg.ARTICLE_MANAGEMENT_MODULEPROGRAMFAILED_TO_QUERY_ALL_ARTICLES);
        }
        return Result.success(pageInfo);
    }

    @Override
    public Result findAllByKeyPro(String keyWord) {
        log.debug("Service：The article module queries all article information");
        String indexKeyWord = "%" + keyWord + "%";
        log.debug(indexKeyWord);
        List<Article> ArticleList = iArticleServiceMapper.findAllByKey(indexKeyWord);
        log.debug(ArticleList.toString());
        for (Article a : ArticleList
        ) {
            Article b = iArticleServiceMapper.selectColumnName((int) a.getArticleId());
            a.setColumnSecondId(b.getColumnSecondId());
            a.setColumnSecondName(b.getColumnSecondName());
            a.setColumnFirstId(b.getColumnFirstId());
            a.setColumnFirstName(b.getColumnFirstName());
        }
        if (ArticleList.isEmpty()) {
            return Result.error(CodeMsg.ARTICLE_MANAGEMENT_MODULEPROGRAMFAILED_TO_QUERY_ALL_ARTICLES);
        }
        return Result.success(ArticleList);
    }

    @Override
    public Result findAllPro() {
        log.debug("Service：The article module queries all article information");
        List<Article> ArticleList = iArticleServiceMapper.findAll();
        for (Article a : ArticleList
        ) {
            Article b = iArticleServiceMapper.selectColumnName((int) a.getArticleId());
            a.setColumnSecondId(b.getColumnSecondId());
            a.setColumnSecondName(b.getColumnSecondName());
            a.setColumnFirstId(b.getColumnFirstId());
            a.setColumnFirstName(b.getColumnFirstName());
        }
        if (ArticleList.isEmpty()) {
            return Result.error(CodeMsg.ARTICLE_MANAGEMENT_MODULEPROGRAMFAILED_TO_QUERY_ALL_ARTICLES);
        }
        return Result.success(ArticleList);
    }

    @Override
    public Result selectArticle(int articleId) throws IOException {
        log.debug("Service：Query articles by article id,articleId:" + articleId);
        String filePath = "C:\\bicc\\article\\" + articleId + ".txt";
        //String filePath="/Users/fushengyuanwuhui/Downloads/"+articleId+".txt";
        Article article = iArticleServiceMapper.selectArticle(articleId);
        article.setCode(FileToString.fileToString(filePath));
        if (article == null) {
            return Result.error(CodeMsg.ARTICLE_MANAGEMENT_MODULEPROGRAMID_QUERY_FAILED);
        }
        return Result.success(article);
    }

    @Override
    public Result batchDeleteArticle(List list) {
        log.debug("Service：Delete articles in bulk,list:" + list.toString());

        int isDelete = iArticleServiceMapper.batchDeleteArticle(list);
        if (isDelete == 0) {
            return Result.error(CodeMsg.ARTICLE_MANAGEMENT_MODULEPROGRAMMASS_DELETION_FAILED);
        }
        return Result.success(null);
    }

    @Override
    public Result deleteArticle(int articleId) {
        log.debug("Service：Delete articles individually,articleId:" + articleId);

        int isDelete = iArticleServiceMapper.deleteArticle(articleId);

        if (isDelete == 0) {
            return Result.error(CodeMsg.ARTICLE_MANAGEMENT_MODULEPROGRAMSINGLE_DELETION_FAILED);
        }
        return Result.success(null);
    }

    @Override
    public Result selectArticleByAuthor(int authorId, int pageNum, int pageSize) {
        log.debug("Service：Query all articles by the author,authorId:" + authorId);
        PageHelper.startPage(pageNum, pageSize); //启动分页
        List<Article> ArticleList = iArticleServiceMapper.selectArticleByAuthor(authorId);
        for (Article a : ArticleList
        ) {
            Article b = iArticleServiceMapper.selectColumnName((int) a.getArticleId());
            a.setColumnSecondId(b.getColumnSecondId());
            a.setColumnSecondName(b.getColumnSecondName());
            a.setColumnFirstId(b.getColumnFirstId());
            a.setColumnFirstName(b.getColumnFirstName());
        }

        PageInfo pageInfo = new PageInfo(ArticleList);  //把dao层放进构造方法形参里，这个对象就有了分页的所有信息
        List<Article> list = pageInfo.getList(); //获取分页对象中的数据列表，主要用于if判断
        if (list.isEmpty()) {
            return Result.error(CodeMsg.FailSelectArticleByAuthor);
        }
        return Result.success(pageInfo);
    }

    @Override
    public Result updateArticle(Article article) {
        log.debug("Service：Update article,article:" + article.toString());
        int isUpdate = iArticleServiceMapper.updateArticle(article);
        long fileName = article.getArticleId();
        String filePath = "C:\\bicc\\article\\" + fileName + ".txt";
        //String filePath="/Users/fushengyuanwuhui/Downloads/"+fileName+".txt";
        StringToFile.stringToFile(article.getCode(), filePath);
        if (isUpdate == 0) {
            return Result.error(CodeMsg.ARTICLE_MANAGEMENT_MODULEPROGRAMFAILED_TO_UPDATE_ARTICLE);
        }
        return Result.success(null);
    }

    //下面的需要修改
    @Override
    public Result insertArticle(Article article) {
        log.debug("Service：Add article,article:" + article.toString());
        int isInsert = iArticleServiceMapper.insertArticle(article);
        long fileName = iArticleServiceMapper.selectArticleName();
        String filePath = "C:\\bicc\\article\\" + fileName + ".txt";
        //String filePath="/Users/fushengyuanwuhui/Downloads/"+fileName+".txt";
        StringToFile.stringToFile(article.getCode(), filePath);
        if (isInsert == 0) {
            return Result.error(CodeMsg.ARTICLE_MANAGEMENT_MODULEPROGRAMFAILED_TO_ADD_ARTICLE);
        }
        return Result.success(null);
    }

    @Override
    public Result selectArticleByColumnSecond(String columnSecond) {
        log.debug("Service：Query all articles under the secondary column,columnSecond:" + columnSecond);
        List<Article> articleList = iArticleServiceMapper.selectArticleByColumnSecond(columnSecond);
        if (articleList.isEmpty()) {
            return Result.error(CodeMsg.ARTICLE_MANAGEMENT_MODULEPROGRAMFAILED_TO_QUERY_THE_ARTICLE_UNDER_THE_SECONDARY_COLUMN);
        }
        return Result.success(articleList);
    }

    @Override
    public Result selectArticleByColumnId(int columnId, int pageNum, int pageSize) {
        log.debug("Service：Query all articles by the author,columnId:" + columnId);
        PageHelper.startPage(pageNum, pageSize); //启动分页
        List<Article> ArticleList = iArticleServiceMapper.selectArticleByColumnId(columnId);
        for (Article a : ArticleList
        ) {
            Article b = iArticleServiceMapper.selectColumnName((int) a.getArticleId());
            a.setColumnSecondId(b.getColumnSecondId());
            a.setColumnSecondName(b.getColumnSecondName());
            a.setColumnFirstId(b.getColumnFirstId());
            a.setColumnFirstName(b.getColumnFirstName());
        }
        PageInfo pageInfo = new PageInfo(ArticleList);  //把dao层放进构造方法形参里，这个对象就有了分页的所有信息
        List<Article> list = pageInfo.getList(); //获取分页对象中的数据列表，主要用于if判断
        if (list.isEmpty()) {
            return Result.error(CodeMsg.FailSelectArticleByColumnId);
        }
        return Result.success(pageInfo);
    }

    @Override
    public Result selectArticleByColumnIdPro(int columnId) {
        log.debug("Service：Query all articles by the author,columnId:" + columnId);
        List<Article> ArticleList = iArticleServiceMapper.selectArticleByColumnId(columnId);
        for (Article a : ArticleList
        ) {
            Article b = iArticleServiceMapper.selectColumnName((int) a.getArticleId());
            a.setColumnSecondId(b.getColumnSecondId());
            a.setColumnSecondName(b.getColumnSecondName());
            a.setColumnFirstId(b.getColumnFirstId());
            a.setColumnFirstName(b.getColumnFirstName());
        }
        if (ArticleList.isEmpty()) {
            return Result.error(CodeMsg.FailSelectArticleByColumnId);
        }
        return Result.success(ArticleList);
    }

    @Override
    public Result selectAllColumnSecond(int pageNum, int pageSize) {
        log.debug("Service：Query all articles under the secondary column");
        PageHelper.startPage(pageNum, pageSize); //启动分页
        List<ColumnSecond> columnSecondList = iArticleServiceMapper.selectAllColumnSecond();
        for (ColumnSecond a : columnSecondList
        ) {
            a.setArticleList(iArticleServiceMapper.selectArticleByColumnId((int) a.getColumnSecondId()));
        }
        PageInfo pageInfo = new PageInfo(columnSecondList);  //把dao层放进构造方法形参里，这个对象就有了分页的所有信息
        List<Article> list = pageInfo.getList(); //获取分页对象中的数据列表，主要用于if判断
        if (list.isEmpty()) {
            return Result.error(CodeMsg.FailedToQueryAllArticlesUnderTheSecondaryColumn);
        }
        return Result.success(pageInfo);
    }

    @Override
    public Result selectArticleByAuthorByKey(Article article) {
        log.debug("Service：Query all articles by the author,authorId:" + article.getArticleId());
        String indexKeyWord = "%" + article.getKeyWord() + "%";
        article.setKeyWord(indexKeyWord);
        log.debug(article.toString());
        List<Article> ArticleList = iArticleServiceMapper.selectArticleByAuthorByKey(article);
        log.debug(ArticleList.toString());
        /*for (Article a:ArticleList
        ) {
            Article b=iArticleServiceMapper.selectColumnName((int) a.getArticleId());
            a.setColumnSecondId(b.getColumnSecondId());
            a.setColumnSecondName(b.getColumnSecondName());
            a.setColumnFirstId(b.getColumnFirstId());
            a.setColumnFirstName(b.getColumnFirstName());
        }*/
        if (ArticleList.isEmpty()) {
            return Result.error(CodeMsg.FailSelectArticleByAuthor);
        }
        return Result.success(ArticleList);
    }

    @Override
    public Result updateArticleOrder(Order order) {
        int articleId1= (int) order.getArticleId1();
        int articleId2= (int) order.getArticleId2();
        log.debug("Service：updateArticleOrder,order"+order.toString());
        int order11=iArticleServiceMapper.selectAllOrder(articleId1);
        log.debug("order11:"+order11);
        int order12=iArticleServiceMapper.selectAllOrder(articleId2);
        log.debug("order12:"+order12);
        Order order1=new Order();
        order1.setOrder1(order11);
        order1.setArticleId(articleId2);
        Order order2=new Order();
        order2.setOrder1(order12);
        order2.setArticleId(articleId1);
        int update1=iArticleServiceMapper.updateArticleOrder(order1);
        int update2=iArticleServiceMapper.updateArticleOrder(order2);
        log.debug("update1:"+update1);
        log.debug("update2:"+update2);
        if (!((update1==1) && (update2==1))) {
            return Result.error(CodeMsg.FailedUpdateColumnFirstOrder);
        }
        return Result.success(null);
    }


}
