package com.bgdCA.service.impl;


import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.bgdCA.domain.*;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.IFileInfoService;
import com.bgdCA.util.EasyExcelUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class FileInfoServiceImpl implements IFileInfoService {

    @Autowired
    private com.bgdCA.mapper.IFileInfoServiceMapper iFileInfoServiceMapper;

    @Autowired
    private com.bgdCA.mapper.FileInfoToExcelMapper fileInfoToExcelMapper;

    @Override
    public Result findAll(int pageNum, int pageSize) {
        log.debug("Service");
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        PageHelper.startPage(pageNum, pageSize);
        //分页对象
        PageInfo pageInfo = new PageInfo(iFileInfoServiceMapper.getFileInfo());
        System.out.println(pageInfo.toString());
        //用于判断是否为空值
        List<FileInfo> list = pageInfo.getList();

        if (list.isEmpty()) {
            return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_FILE);
        }
        return Result.success(pageInfo);
    }

    @Override
    public Result findTarget(int fileId) {
        log.debug("Service:" + fileId);

        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        List<FileInfo> list = iFileInfoServiceMapper.getTargetFileInfo(fileId);
        if (list.isEmpty()) {
            return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
        }
        return Result.success(list);
    }

    @Override
    public Result updataFileInfo(FileInfo fileInfo) {
        log.debug("Service:" + fileInfo.toString());

        System.out.println(fileInfo.toString());
        iFileInfoServiceMapper.updataFileInfo(fileInfo);
//        HashMap<String, Object> hashMap = new HashMap<String, Object>();
//        try {
//            int result =
//            if (result == 0) {
//                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_UPDATE_FILE);
//            }
//        } catch (Exception e) {
//            return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_UPDATE_FILE);
//        }
        return Result.success(null);
    }


    @Override
    public Result addFileInfo(FileInfo fileInfo) {
        log.debug("Service:" + fileInfo.toString());

        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        System.out.println(fileInfo.toString());
        iFileInfoServiceMapper.addFileInfo(fileInfo);
//        try {
//            int result =
//            if (result == 0) {
//                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_ADD_FILE);
//            }
//        } catch (Exception e) {
//            return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_ADD_FILE);
//        }
        return Result.success(null);
    }

    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED, rollbackForClassName = "Exception")
    @Override
    public Result batchDeleteFileInfo(List list) {
        log.debug("Service listSize:" + list.size());
        HashMap<String, Object> hashMap = new HashMap();

        try {
            int result = iFileInfoServiceMapper.batchDeleteFileInfo(list);
            if (result != list.size()) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_DELETE_FILES_IN_BATCH);
            }
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_DELETE_FILES_IN_BATCH);
        }
        return Result.success(null);
    }

    @Override
    public Result deleteFileInfo(int fileId) {
        log.debug("Service:" + fileId);
        try {
            int result = iFileInfoServiceMapper.deleteFileInfo(fileId);
            if (result == 0) {
                Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_DELETE_FILE_INDIVIDUALLY);
            }
        } catch (Exception e) {
            Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_DELETE_FILE_INDIVIDUALLY);
        }
        return Result.success(null);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = "Exception")
    @Override
    public Result uploadExcel(List<FileInfoExcel> fileInfo) {
        log.debug("Service listSize:" + fileInfo.size());
        int result = iFileInfoServiceMapper.uploadExcel(fileInfo);

        if (result != fileInfo.size() || fileInfo.size() == 0) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_UPLOAD_FILE);
        }
        return Result.success(null);
    }

    @Override
    public Result rebackAll(int pageNum, int pageSize) {
        log.debug("Service");
        PageHelper.startPage(pageNum, pageSize);
        PageInfo pageInfo = new PageInfo(iFileInfoServiceMapper.getFileReback());
        System.out.println(pageInfo.toString());
        List<FileInfo> list = pageInfo.getList();

        if (list.isEmpty()) {
            Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_UPLOAD_FILE);
        }
        return Result.success(pageInfo);
    }

    @Override
    public Result dataAnalysis() {
        log.debug("Service");
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        /*档案四组分析数据*/

        List<Analysis> ContinentAnalysis = iFileInfoServiceMapper.dataAnalysisContinent();
        List<Analysis> workDutyAnalysis = iFileInfoServiceMapper.dataAnalysisWorkDuty();
        List<Analysis> departmentAnalysis = iFileInfoServiceMapper.dataAnalysisDepartment();
        List<Analysis> studyStartAnalysis = iFileInfoServiceMapper.dataAnalysisStudyStart();
        List<Analysis> studyEndAnalysis = iFileInfoServiceMapper.dataAnalysisStudyEnd();
        if (ContinentAnalysis.isEmpty() || workDutyAnalysis.isEmpty() || departmentAnalysis.isEmpty()
                || studyStartAnalysis.isEmpty() || studyEndAnalysis.isEmpty()) {
            return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_FILE);
        }
        hashMap.put("ContinentAnalysis", ContinentAnalysis);
        hashMap.put("workDutyAnalysis", workDutyAnalysis);
        hashMap.put("departmentAnalysis", departmentAnalysis);
        hashMap.put("studyStartAnalysis", studyStartAnalysis);
        hashMap.put("studyEndAnalysis", studyEndAnalysis);
        return Result.success(hashMap);
    }

    @Override
    public Result datasearch(FileInfo fileInfo) {
        log.debug("Service:" + fileInfo);

        List<FileInfoForExcel> list = iFileInfoServiceMapper.dataSearch(fileInfo);
        if (list.isEmpty()) {
            return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
        }
        return Result.success(list);
    }

    @Override
    public Result datasearchPage(FileInfo fileInfo) {
        log.debug("Service:" + fileInfo);
        int pageNum = fileInfo.getPageNum();
        int pageSize = fileInfo.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        PageInfo pageInfo = new PageInfo(iFileInfoServiceMapper.dataSearch(fileInfo));
        List<FileInfo> list = pageInfo.getList();
        if (list.isEmpty()) {
            return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
        }
        return Result.success(pageInfo);
    }

    @Override
    public Result datasearchAll(String key) {
        log.debug("Service:" + key);

        List<FileInfo> list = iFileInfoServiceMapper.searchForAll(key);
        if (list.isEmpty()) {
            return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
        }
        return Result.success(list);
    }

    @Override
    public Result Analysisplus(FileInfo fileInfo) {

        List<SearchTest> list = iFileInfoServiceMapper.analysisplus(fileInfo);
        if (list.isEmpty()) {
            return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
        }
        return Result.success(list);

    }

    @Override
    public void AnalysisplusGetExcel(FileInfo fileInfo, HttpServletResponse response,String filename) throws Exception {
        List<FileInfoForExcel> list = iFileInfoServiceMapper.dataSearch(fileInfo);
        ExcelWriter writer = ExcelUtil.getWriter(true);
        writer.write(list);
        response.reset();
        response.setContentType("application/octet-stream; charset=utf-8");
        response.setHeader("Content-Disposition","attachment; filename="+ URLEncoder.encode(filename,"UTF-8"));
        writer.flush(response.getOutputStream());
        writer.close();
    }

    @Override
    public Result dataAnalysis2(FileInfo fileInfo) {
        log.debug("Service:" + fileInfo);

        if (fileInfo.getPersonalYear() != 0) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysispersonalYear(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getLastName() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysislastName(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }

        if (fileInfo.getFirstName() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysisfirstName(fileInfo);
           // System.out.println("aaaa");
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getChineseName() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysischinesename(fileInfo);
           // System.out.println("aaaa");
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getIdNumber() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysisidNumber(fileInfo);
            // System.out.println("aaaa");
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getContinent() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysiscontinent(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getNational() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysisnational(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getGender() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysisgender(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getBirthday() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysisbirthday(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getEntranceAge() != 0) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysisage(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getReligion() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysisreligion(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getChineseCompany() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysischineseCompany(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getCompany() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysiscompany(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getWorkDuty() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysisworkDuty(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getChineseWorkDuty() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysischineseWorkDuty(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getLevel() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysislevel(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getTelephone() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysistelephone(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getEmail() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysisemail(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getParticipateProject() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysisparticipateProject(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getStudyStart() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysisstudyStart(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getStudyEnd() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysisstudyEnd(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getLanguage() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysislanguage(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getDepartment() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysisdepartment(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getManager() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysismanager(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);
        }
        if (fileInfo.getChineseLevel() != null) {
            List<FileInfo> list = iFileInfoServiceMapper.dataAnalysischineseLevel(fileInfo);
            if (list.isEmpty()) {
                return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);
            }
            return Result.success(list);

        }
        return Result.error(CodeMsg.FILE_MANAGEMENT_MODULETHE_SERVERFAILED_TO_QUERY_SINGLE_FILE);





    }

    @Override
    public List<FileInfoToExcel> fileInfoToExcel(FileInfoToExcel fileInfoToExcel) {
        List<FileInfoToExcel> list=fileInfoToExcelMapper.fileInfoToExcel(fileInfoToExcel);
        return list;
    }


}
