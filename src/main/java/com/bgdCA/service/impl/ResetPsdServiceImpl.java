package com.bgdCA.service.impl;

import com.bgdCA.domain.Account;
import com.bgdCA.domain.ResetUser;
import com.bgdCA.mapper.RegisterMapper;
import com.bgdCA.service.ResetPsdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResetPsdServiceImpl implements ResetPsdService {

    @Autowired
    private RegisterMapper registerMapper;

    @Override
    public Account findUser(String username) {
        //暂时模拟数据库读取
        //根据用户名查找用户，返回Account对象
        List<Account> account = registerMapper.findTarget(username);
        if (account.isEmpty()) {
            return null;
        }
        return account.get(0);
    }

    @Override
    public int resetPsd(ResetUser resetUser) {
        //传入username，newPsd，修改用户表中数据
        return registerMapper.changePsd(resetUser);
    }
}
