package com.bgdCA.service.impl;

import com.bgdCA.mapper.SignatureMapper;
import com.bgdCA.domain.Signature;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.SignatureService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Signature)表服务实现类
 *
 * @author sms
 * @since 2020-08-12 20:11:21
 */
@Service("signatureService")
public class SignatureServiceImpl implements SignatureService {
    @Resource
    private SignatureMapper signatureMapper;

    @Override
    public Result querySignatureById(String userName) {
        List<Signature> list = signatureMapper.querySignatureById(userName);
        if (list.isEmpty()) {
            Result.error(CodeMsg.QUERY_SIGNATURE_FAILED);
        }
        return Result.success(list);
    }

    @Override
    public Result insertSignature(Signature signature) {
        int affectedRow = signatureMapper.insertSignature(signature);
        if (affectedRow == 0) {
            Result.error(CodeMsg.NEW_SIGNATURE_FAILED);
        }
        return Result.success(signature.getSignatureId());
    }

    @Override
    public Result deleteSignatureById(Integer signatureId) {
        int affectedRow = signatureMapper.deleteSignatureById(signatureId);
        if (affectedRow == 0) {
            Result.error(CodeMsg.FAILED_TO_DELETE_SIGNATURE);
        }
        return Result.success(null);
    }
}