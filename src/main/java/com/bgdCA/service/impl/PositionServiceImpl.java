package com.bgdCA.service.impl;

import com.bgdCA.domain.Position;
import com.bgdCA.domain.UserLoginDomain;
import com.bgdCA.mapper.PositionMapper;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.PositionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/25 10:20
 */
@Slf4j
@Service
public class PositionServiceImpl implements PositionService {
    @Autowired
    PositionMapper positionMapper;

    @Override
    public List<Position> findAll() {
        log.debug("Service：Permission module query all");
        List<Position> positionList = positionMapper.findAll();
        for (Position s : positionList
        ) {
            if ((int) s.getHigherPositionId() != 0) {
                Position position = new Position();
                position = positionMapper.selectPowerById((int) s.getHigherPositionId());
//                s.setHigherPositionName(position.getPositionName());
            }
        }
        return positionList;

    }

    @Override
    public Result deletePosition(int positionId) {
        log.debug("Service：Single delete permission module,positionId:" + positionId);
        System.out.println(positionId);
        List<UserLoginDomain> list = positionMapper.checkNull(positionId);
        List<Position> positionList = positionMapper.checkHigherNull(positionId);
        if (!list.isEmpty()) {
            System.out.println(positionId);

            return Result.error(CodeMsg.AUTHORITY_MANAGEMENT_MODULETHE_SERVERSINGLE_DELETION_FAILED);
        }

        if (!positionList.isEmpty()) {
            System.out.println(positionId);
            return Result.error(CodeMsg.AUTHORITY_MANAGEMENT_MODULETHE_SERVERSINGLE_DELETION_FAILED1);

        }
        System.out.println("sus="+positionId);
        int position = positionMapper.deletePosition(positionId);
        return Result.success(position);
    }

    @Override
    public Position selectPowerById(int positionId) {
        log.debug("Service：Find permissions by ID");
        Position position = positionMapper.selectPowerById(positionId);
        return position;

    }

    @Override
    public int addPosition(Position position) {
        log.debug("Service：Single add permission module,positionId:\"+positionId");
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        log.debug("position: " + position);
        int isDelete = positionMapper.addPosition(position);
        return isDelete;


    }


    @Override
    public int modifyPosition(Position position) {
        log.debug("Service：Single modify permission module,positionId:\"+positionId");
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        log.debug("position: " + position);
        int isDelete = positionMapper.modifyPosition(position);
        return isDelete;
    }

    @Override
    public Result selectPositionByTeam(String teamId) {
        log.debug("teamId:"+teamId);
        HashMap<String, Object> hashMap = new HashMap();
        List<Position> positionList = positionMapper.selectPositionByTeam(teamId);
        if (positionList.isEmpty()) {
            return Result.error(null);
        }
        return Result.success(positionList);
    }
}
