package com.bgdCA.service.impl;

import com.bgdCA.mapper.GroupsMapper;
import com.bgdCA.domain.Groups;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.GroupsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * (Groups)表服务实现类
 *
 * @author sms
 * @since 2020-08-12 18:09:07
 */
@Service("groupsService")
public class GroupsServiceImpl implements GroupsService {
    @Resource
    private GroupsMapper groupsMapper;

    /**
     * 新增数据
     *
     * @param groups 实例对象
     * @return 实例对象
     */
    @Override
    public Result insertGroup(Groups groups) {
        int affectedRow = groupsMapper.insertGroup(groups);
        if (affectedRow == 0) {
            Result.error(CodeMsg.NEW_GROUPING_FAILED);
        }
        return Result.success(groups.getGroupId());
    }

    /**
     * 修改数据
     *
     * @param groupId      分组ID
     * @param groupContent 分组名
     * @return 实例对象
     */
    @Override
    public Result updateGroup(String groupContent, int groupId) {
        int affectedRow = groupsMapper.updateGroup(groupContent, groupId);
        if (affectedRow == 0) {
            Result.error(CodeMsg.FAILED_TO_RENAME_THE_GROUP);
        }
        return Result.success(null);
    }

    /**
     * 通过主键删除数据
     *
     * @param groupId 主键
     * @return 是否成功
     */
    @Override
    public Result deleteGroupById(Integer groupId) {
        int affectedRow = groupsMapper.deleteGroupById(groupId);
        if (affectedRow == 0) {
            Result.error(CodeMsg.DELETE_GROUP_FAILED);
        }
        return Result.success(null);
    }
}