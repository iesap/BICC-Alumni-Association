package com.bgdCA.service.impl;

import com.bgdCA.domain.SysLog;
import com.bgdCA.mapper.ISysLogServiceMapper;
import com.bgdCA.service.ISysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Promise
 * @createTime 2018年12月18日 下午9:30:57
 * @description
 */
@Service("sysLogService")
public class SysLogServiceImpl implements ISysLogService {
    @Autowired
    private ISysLogServiceMapper sysLogMapper;

    @Override
    public int insertLog(SysLog entity) {
        //       // TODO Auto-generated method stub
//       return sysLogMapper.insert(entity);
        return 0;
    }
}