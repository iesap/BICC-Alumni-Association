package com.bgdCA.service.impl;

import com.bgdCA.domain.User;
import com.bgdCA.mapper.UserMapper;
import com.bgdCA.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName LoginServiceImpl
 * @Description TODO
 * @Author AlvinZheng
 * @Date 2020/8/1 11:58
 * @Version 1.0
 **/
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User findUser(String username) {
        List<User> account = userMapper.findUser(username);
        System.out.println(account.isEmpty());
        if (account.isEmpty()) {
            return null;
        }
        return account.get(0);
    }
}
