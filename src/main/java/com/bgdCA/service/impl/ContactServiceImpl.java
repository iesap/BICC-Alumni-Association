package com.bgdCA.service.impl;

import com.bgdCA.domain.ContactAndGroup;
import com.bgdCA.domain.DutyGroup;
import com.bgdCA.domain.Groups;
import com.bgdCA.mapper.ContactMapper;
import com.bgdCA.domain.Contact;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.ContactService;
import org.aspectj.apache.bcel.classfile.Code;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Contact)表服务实现类
 *
 * @author sms
 * @since 2020-08-12 19:19:26
 */
@Service("contactService")
public class ContactServiceImpl implements ContactService {
    @Resource
    private ContactMapper contactMapper;


    @Override
    public Result deleteContactByKey(Integer contactId) {
        int affectedRow = contactMapper.deleteContactByKey(contactId);
        if (affectedRow == 0) {
            Result.error(CodeMsg.FAILED_TO_DELETE_CONTACT);
        }
        return Result.success(null);
    }

    @Override
    public Result insertContact(Contact record) {
        int affectedRow = contactMapper.insertContact(record);
        if (affectedRow == 0) {
            Result.error(CodeMsg.NEW_CONTACT_FAILED);
        }
        return Result.success(record.getContactId());
    }

    @Override
    public Result selectByPrimaryKey(String userName) {
        List<Groups> list = contactMapper.selectByPrimaryKey(userName);
        System.out.println(list.toString());
        if (list.isEmpty()) {
            return Result.error(CodeMsg.FAILED_TO_DISPLAY_CONTACT_AND_GROUP_INFORMATION);
        }
        return Result.success(list);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = "Exception")
    @Override
    public Result updateContactGroup(List list, int groupId) {
        int affectedRowContact = contactMapper.updateContactGroup(list);
        if (affectedRowContact != list.size()) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.error(CodeMsg.FAILED_TO_CHANGE_THE_CONTACT_GROUP);
        }
        int affectedRowGroup = contactMapper.deleteGroup(groupId);
        if (affectedRowGroup != 1) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.error(CodeMsg.FAILED_TO_DELETE_GROUPS_WITH_CONTACTS);
        }
        return Result.success(null);
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackForClassName = "Exception")
    @Override
    public Result updateGroupOfContact(List list, int groupId) {
        int affectedRowGroup = contactMapper.updateGroupOfContact(list, groupId);
        if (list.size() != affectedRowGroup) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return Result.error(CodeMsg.FAILED_TO_CHANGE_THE_CONTACT_GROUP);
        }
        return Result.success(null);
    }

    @Override
    public Result selectEmptyGroup(String userName) {
        List<Groups> emptyGroup = contactMapper.selectEmptyGroup(userName);
        if (emptyGroup.isEmpty()) {
            Result.error(CodeMsg.FAILED_TO_DISPLAY_CONTACT_AND_GROUP_INFORMATION);
        }
        return Result.success(emptyGroup);
    }

    @Override
    public Result updateMailAddressAndName(String contactMailAddress, String contactContent, int contactId) {
        int affectedRow = contactMapper.updateMailAddressAndName(contactMailAddress, contactContent, contactId);
        if (affectedRow == 0) {
            return Result.error(CodeMsg.FAILED_TO_CHANGE_THE_CONTACT_GROUP);
        }
        return Result.success(null);
    }

    @Override
    public Result selectEmailGroupByDepartment() {
        List<DutyGroup> dutyGroups = contactMapper.selectEmailGroupByDepartment();
        if (dutyGroups.isEmpty()) {
            return Result.error(CodeMsg.FAILED_TO_SELECT_CONTACT_BY_DUTY);
        }
        return Result.success(dutyGroups);
    }

    @Override
    public Result selectEmailGroupByCountry() {
        List<DutyGroup> countryGroup = contactMapper.selectEmailGroupByCountry();
        if (countryGroup.isEmpty()) {
            return Result.error(CodeMsg.FAILED_TO_SELECT_CONTACT_BY_DUTY);
        }
        return Result.success(countryGroup);
    }
}