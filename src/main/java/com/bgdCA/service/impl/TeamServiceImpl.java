package com.bgdCA.service.impl;

import com.bgdCA.domain.Team;
import com.bgdCA.mapper.TeamMapper;
import com.bgdCA.response.Result;
import com.bgdCA.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamMapper teamMapper;

    @Override
    public Result addTeam(Team team) {


        int affectedRow = teamMapper.addTeam(team);
        if (affectedRow == 0){
            return Result.error(null);
        }
        return Result.success(team.getTeamid());
    }

    @Override
    public Result deleteTeam(int teamid) {


        int affectedRow = teamMapper.deleteTeam(teamid);
        if (affectedRow == 0){
            return Result.error(null);
        }
        return Result.success(null);
    }

    @Override
    public Result selectTargetTeam(int teamId) {
        Team team = teamMapper.selectTargetTeam(teamId);
        if (team == null){
            return Result.error(null);
        }
        return Result.success(team);
    }

}
