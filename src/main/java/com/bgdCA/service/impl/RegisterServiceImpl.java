package com.bgdCA.service.impl;


import com.bgdCA.domain.Account;
import com.bgdCA.mapper.RegisterMapper;
import com.bgdCA.service.RegisterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Slf4j
public class RegisterServiceImpl implements RegisterService {

    @Autowired
    private RegisterMapper registerMapper;

    @Override
    public Account findUser(String username) {
        //暂时模拟数据库读取
        //根据用户名查找用户，返回Account对象
//        String date = "2010-1-2";
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        Date newDate = sdf.parse(date);
        List<Account> account = registerMapper.findTarget(username);
        if (account.isEmpty()) {
            return null;
        }

        return account.get(0);
    }

    @Override
    public int insertUser(Account account) {
        System.out.println("serice: " + account.toString());
        return registerMapper.insertUser(account);
    }
}