package com.bgdCA.service.impl;

import com.bgdCA.domain.ArticleReback;
import com.bgdCA.mapper.ArticleRebackMapper;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.ArticleRebackService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/26 16:00
 */
@Slf4j
@Service
public class ArticleRebackServiceImpl implements ArticleRebackService {
    @Autowired
    ArticleRebackMapper artcileRebackMapper;

    @Override
    public Result findAll(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize); //启动分页
        log.debug("Service：Article table history table query all");
        List<ArticleReback> artcileRebackList = artcileRebackMapper.findAll();
        for (ArticleReback s : artcileRebackList
        ) {
            s.setName(artcileRebackMapper.selectName((int) s.getAuthorId()));
        }

        PageInfo pageInfo = new PageInfo(artcileRebackList);  //把dao层放进构造方法形参里，这个对象就有了分页的所有信息
        List<ArticleReback> list = pageInfo.getList(); //获取分页对象中的数据列表，主要用于if判断
        if (artcileRebackList.isEmpty()) {
            return Result.error(CodeMsg.ARTICLE_HISTORY_MODULEPROGRAMARTICLE_HISTORY_QUERY_FAILED);
        }
        return Result.success(pageInfo);
    }

    @Override
    public Result findAllPro(String keyWord) {
        log.debug("Service：Article table history table query all");
        String indexKeyWord = "%" + keyWord + "%";
        log.debug(indexKeyWord);
        List<ArticleReback> artcileRebackList = artcileRebackMapper.findAllPro(indexKeyWord);
        if (artcileRebackList.isEmpty()) {
            return Result.error(CodeMsg.ARTICLE_HISTORY_MODULEPROGRAMARTICLE_HISTORY_QUERY_FAILED);
        }
        return Result.success(artcileRebackList);
    }
}
