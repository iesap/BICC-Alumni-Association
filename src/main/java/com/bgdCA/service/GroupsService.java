package com.bgdCA.service;

import com.bgdCA.domain.Groups;
import com.bgdCA.response.Result;

/**
 * (Groups)表服务接口
 *
 * @author sms
 * @since 2020-08-12 18:09:06
 */
public interface GroupsService {


    /**
     * 新增数据
     *
     * @param groups 实例对象
     * @return 实例对象
     */
    Result insertGroup(Groups groups);

    /**
     * 重命名分组
     *
     * @param groupContent
     * @param groupId
     * @return
     */
    Result updateGroup(String groupContent, int groupId);

    /**
     * 通过主键删除数据
     *
     * @param groupId 主键
     * @return 是否成功
     */
    Result deleteGroupById(Integer groupId);

}