package com.bgdCA.service;

import com.bgdCA.domain.Signature;
import com.bgdCA.response.Result;

import java.util.List;

/**
 * (Signature)表服务接口
 *
 * @author sms
 * @since 2020-08-12 20:11:21
 */
public interface SignatureService {

    /**
     * 通过ID查询单条数据
     *
     * @param userName 主键
     * @return 实例对象
     */
    Result querySignatureById(String userName);


    /**
     * 新增数据
     *
     * @param signature 实例对象
     * @return 实例对象
     */
    Result insertSignature(Signature signature);

    /**
     * 通过主键删除数据
     *
     * @param signatureId 主键
     * @return 是否成功
     */
    Result deleteSignatureById(Integer signatureId);

}