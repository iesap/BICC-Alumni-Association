package com.bgdCA.service;

import com.bgdCA.domain.Article;
import com.bgdCA.domain.ColumnSecond;
import com.bgdCA.domain.Order;
import com.bgdCA.response.Result;

import java.io.IOException;
import java.util.List;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/3 22:05
 */
public interface IArticleService {
    //    public HashMap<String,Object> findAll(int pageNum, int pageSize);
    public Result findAll(int pageNum, int pageSize);

    public Result findAllByKey(String keyWord, int pageNum, int pageSize);

    public Result findAllByKeyPro(String keyWord);

    public Result findAllPro();

    public Result selectArticle(int articleId) throws IOException;

    public Result batchDeleteArticle(List list);

    public Result deleteArticle(int articleId);

    public Result selectArticleByAuthor(int authorId, int pageNum, int pageSize);

    public Result updateArticle(Article article);

    public Result insertArticle(Article article);

    public Result selectArticleByColumnSecond(String columnSecond);

    public Result selectArticleByColumnId(int columnId, int pageNum, int pageSize);

    public Result selectArticleByColumnIdPro(int columnId);

    public Result selectAllColumnSecond(int pageNum, int pageSize);

    public Result selectArticleByAuthorByKey(Article article);

    public Result updateArticleOrder(Order order);
}
