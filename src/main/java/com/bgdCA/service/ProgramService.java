package com.bgdCA.service;

import com.bgdCA.domain.Program;
import com.bgdCA.response.Result;

public interface ProgramService {
    Result modifyProgram(Program program);

    Result deleteProgram(int programId);
    Result foundProgram();
}
