package com.bgdCA.service;

import com.bgdCA.domain.StationLetter;
import com.bgdCA.response.Result;

import java.util.HashMap;
import java.util.List;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/29 09:26
 */
public interface StationLetterService {
    public Result selectAllByUserId(int userId, int pageNum, int pageSize);

    public Result deleteStationLetterById(int id);

    public Result selectAllByUserIdPlus(int userId);

    public Result batchDeleteStationLetter(List list);

    public Result updateStationLetterToRead(int id);

    public Result sendMessage(StationLetter stationLetter);

    public Result selectStationLetterById(int id);

    public Result selectAllOrderByTime(StationLetter stationLetter);
}
