package com.bgdCA.service;

import com.bgdCA.domain.User;

public interface LoginService {

    /**
     * 通过username找到对应用
     *
     * @param username 用户名
     * @return 用户存在返回封装后的User，不存在返回null
     */
    public User findUser(String username);
}
