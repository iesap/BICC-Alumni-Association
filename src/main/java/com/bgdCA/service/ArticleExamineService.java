package com.bgdCA.service;

import com.bgdCA.domain.ArticleDomain;
import com.bgdCA.response.Result;
import com.github.pagehelper.PageInfo;

import java.util.List;

public interface ArticleExamineService {
    //查找文章详细信息
    public Result findTarget(String key);

    //管理员当前部门未审核信息
    public Result findNoPass(int userId, String key, int pageNum, int pageSize);

    //审核通过
    public Result pass(int articleId);

    //审核不通过
    public Result noPass(int articleId);

    //批量审核通过
    public Result passAllSelect(List list);

    //批量审核不通过
    public Result noPassAllSelect(List list);

    public Result findAllPass(String columnSecondName);
}
