package com.bgdCA.service;

import com.bgdCA.domain.Account;
import com.bgdCA.domain.ProfilePictureUrl;
import com.bgdCA.response.Result;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.List;


public interface AccountManageService {
    //查询所有账户
    public HashMap<String, Object> findAll(int pageNum, int pageSize);

    //查询单个账户
    public HashMap<String, Object> findTarget(String name);

    //添加账户信息
    public HashMap<String, Object> addAccount(Account account);

    //修改账户信息
    public HashMap<String, Object> updataAccount(Account account);

    //删除账户信息
    public HashMap<String, Object> deleteAccount(int id);

    //停用账户
    public HashMap<String, Object> banAccount(int id);

    //启用账户
    public HashMap<String, Object> startAccount(int id);

    //批量删除账户信息
    public HashMap<String, Object> batchdeleteAccount(List list);

    //修改账户权限
    public HashMap<String, Object> updateAccountPower(int id, int power);

    //查询账户权限
    public HashMap<String, Object> getPower(int id);


    //上传头像
    Result uploadPortrait(MultipartFile file, String userName, String userId);

    //查询联系人
    public HashMap<String, Object> findPerson(String username);

    public HashMap<String, Object> findPersonEmail(String mail);

    public HashMap<String, Object> findPersonNickName(String name);


}


