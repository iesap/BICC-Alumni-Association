package com.bgdCA.service;

import com.bgdCA.domain.Column;
import com.bgdCA.domain.ColumnDomain;
import com.bgdCA.domain.ColumnSecond;
import com.bgdCA.domain.Order;
import com.bgdCA.response.Result;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/8/17 18:57
 */
public interface ColumnService {
    public Result selectAllColumnFirst();

    public Result addSubColumn(ColumnSecond column);

    public Result addMainColumn(Column column);

    int DeleteColumnFirst(int columnFirstId);

    int DeleteColumnSecond(int columnSecondId);

    //修改栏目
    public Result saveColumnFirst(ColumnDomain columnDomain);

    public Result saveColumnSecond(ColumnDomain columnDomain);

    //查找栏目
    public Result selectArticleByColumnFirst(ColumnSecond columnFirstId);

    public Result selectAllColumnFirstAndColumnSecond(int pageNum, int pageSize);

    public Result selectAllColumnFirstAndColumnSecondPlus(int pageNum, int pageSize);

    public Result selectAllColumnFirstAndColumnSecondPro();

    public Result selectAllColumnFirstAndColumnSecondPlusPro();


    public Result selectColumnFirstAllByColumnSecondId(int columnSecondId, int pageNum, int pageSize);


    public Result updateColumnFirstOrder(Order order);

    public Result updateColumnSecondOrder(Order order);
}
