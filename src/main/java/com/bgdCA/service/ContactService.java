package com.bgdCA.service;

import com.bgdCA.domain.Contact;
import com.bgdCA.domain.Contact;
import com.bgdCA.domain.DutyGroup;
import com.bgdCA.domain.Groups;
import com.bgdCA.response.Result;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Contact)表服务接口
 *
 * @author sms
 * @since 2020-08-12 19:19:26
 */
public interface ContactService {

    Result deleteContactByKey(Integer contactId);

    Result insertContact(Contact record);

    Result selectByPrimaryKey(String userName);

    Result updateContactGroup(List list, int groupId);

    Result updateGroupOfContact(List list, int groupId);

    Result selectEmptyGroup(String userName);

    Result updateMailAddressAndName(String contactMailAddress,
                                    String contactContent, int contactId);

    Result selectEmailGroupByDepartment();

    Result selectEmailGroupByCountry();
}