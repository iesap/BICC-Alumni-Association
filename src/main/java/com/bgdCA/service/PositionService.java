package com.bgdCA.service;

import com.bgdCA.domain.Position;
import com.bgdCA.response.Result;

import java.util.List;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/25 10:16
 */
public interface PositionService {
    List<Position> findAll();

    Result deletePosition(int positionId);

    Position selectPowerById(int positionId);

    int addPosition(Position position);

    int modifyPosition(Position position);

    Result selectPositionByTeam(String teamId);
}
