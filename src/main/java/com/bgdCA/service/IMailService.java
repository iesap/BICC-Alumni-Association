package com.bgdCA.service;


import com.bgdCA.domain.SendInfo;
import com.bgdCA.response.Result;

/**
 * @ClassName MailService
 * @Description TODO
 * @Author AlvinZheng
 * @Date 2020/7/27 10:41
 * @Version 1.0
 **/
public interface IMailService {

    public Result sendHtmlMail(SendInfo sendInfo);

    public void sendTemplateMail(String[] to, String subject, String activeHref);

    public void registerTemplateMail(String[] to, String subject, String activeHref);

    public void resetTemplateMail(String[] to, String subject, String captcha);


}
