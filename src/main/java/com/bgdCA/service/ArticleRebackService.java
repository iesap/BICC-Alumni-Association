package com.bgdCA.service;

import com.bgdCA.response.Result;

import java.util.HashMap;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/26 15:59
 */
public interface ArticleRebackService {
    public Result findAll(int pageNum, int pageSize);

    public Result findAllPro(String keyWord);
}
