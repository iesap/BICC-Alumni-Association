package com.bgdCA.service;

import com.bgdCA.domain.Account;
import com.bgdCA.domain.ResetUser;


public interface ResetPsdService {

    Account findUser(String username);

    int resetPsd(ResetUser resetUser);
}
