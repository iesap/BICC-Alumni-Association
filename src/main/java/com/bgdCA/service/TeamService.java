package com.bgdCA.service;


import com.bgdCA.domain.Team;
import com.bgdCA.response.Result;

public interface TeamService {

    Result addTeam(Team team);

    Result deleteTeam(int teamid);

    Result selectTargetTeam(int teamId);

}
