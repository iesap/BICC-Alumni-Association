package com.bgdCA.service;

import com.bgdCA.domain.FileInfo;
import com.bgdCA.domain.FileInfoExcel;
import com.bgdCA.domain.FileInfoToExcel;
import com.bgdCA.domain.Pages;
import com.bgdCA.response.Result;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface IFileInfoService {
    Result findAll(int pageNum, int pageSize);

    Result findTarget(int fileId);

    Result addFileInfo(FileInfo fileInfo);

    Result updataFileInfo(FileInfo fileInfo);

    Result batchDeleteFileInfo(List list);

    Result deleteFileInfo(int fileId);

    Result uploadExcel(List<FileInfoExcel> fileInfo);

    Result rebackAll(int pageNum, int pageSize);

    Result dataAnalysis();

    Result datasearch(FileInfo fileInfo);

    Result datasearchPage(FileInfo fileInfo);

    Result datasearchAll(String key);

    Result dataAnalysis2(FileInfo fileInfo);

    Result Analysisplus(FileInfo fileInfo);

    void AnalysisplusGetExcel(FileInfo fileInfo, HttpServletResponse response, String filename) throws Exception;

    List<FileInfoToExcel> fileInfoToExcel(FileInfoToExcel fileInfoToExcel);

}
