package com.bgdCA.auth.exception;

/**
 * @ClassName ExceptionEnum
 * @Description TODO
 * @Author AlvinZheng
 * @Date 2020/8/5 12:08
 * @Version 1.0
 **/
public enum BiccExceptionEnum implements IBiccExceptionEnum {

    USER_NOT_FOUND("201001", "user not found"),
    PASSWORD_NOT_RIGHT("201002", "password not right"),
    GENERATE_TOKEN_FAILURE("201003", "generate token failure"),
    GENERATE_TOKEN_IS_EMPTY("201004", "generate token is empty"),
    GENERATE_COOKIES_FAILURE("201005", "generate cookies failure"),
    COOKIES_NOT_FOUND("201006", "cookies not found");

    private String code;
    private String message;

    BiccExceptionEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return null;
    }

    @Override
    public String getMessage() {
        return null;
    }
}
