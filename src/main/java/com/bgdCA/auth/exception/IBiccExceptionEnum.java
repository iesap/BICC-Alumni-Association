package com.bgdCA.auth.exception;

public interface IBiccExceptionEnum {

    public String getCode();

    public String getMessage();


}
