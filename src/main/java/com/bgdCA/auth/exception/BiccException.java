package com.bgdCA.auth.exception;

/**
 * @ClassName BiccException
 * @Description TODO
 * @Author AlvinZheng
 * @Date 2020/8/5 12:14
 * @Version 1.0
 **/
public class BiccException extends RuntimeException {

    public BiccException(Object object) {
        super(object.toString());
    }


}
