package com.bgdCA.auth.config;

import com.bgdCA.auth.filter.HttpServletFilter;
import com.bgdCA.auth.interceptor.AuthInterceptor;
import com.bgdCA.auth.interceptor.CaptchaInterceptor;
import com.bgdCA.auth.interceptor.TokenInterceptor;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * @ClassName WebConfiger
 * @Description TODO
 * @Author AlvinZheng
 * @Date 2020/8/1 0:46
 * @Version 1.0
 **/
@Configuration
public class WebApplicationConfig implements WebMvcConfigurer {


    private Boolean token = true;
    private Boolean auth = true;
    private Boolean captcha = false;
    @Resource
    private HttpServletFilter httpServletFilter;

    @Bean
    public TokenInterceptor getTokenInterceptor() {
        return new TokenInterceptor();
    }

    @Bean
    public CaptchaInterceptor getCaptchaInterceptor() {
        return new CaptchaInterceptor();
    }

    @Bean
    public AuthInterceptor getAuthInterceptor() {
        return new AuthInterceptor();
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        if (token) {
            registry.addInterceptor(getTokenInterceptor())
                    .addPathPatterns("/**")
                    .excludePathPatterns("/login", "/captcha.jpg", "/register", "/start", "/reset", "/reset2")
                    .excludePathPatterns("/swagger-resources/**", "/webjars/**", "/v2/**", "/doc.html/**", "/error/**", "/stationLetter/**",
                            "/sendOutSideMail/**","/reception/**","/signature/**","/contact/**","/groups/**");
                 //   .excludePathPatterns("/column/**","/article/**","/login/**","/register/**","/stationLetter/**","/ac_manage/**");
        }
        if (auth) {
            registry.addInterceptor(getAuthInterceptor())
                    .addPathPatterns("/**")
                    .excludePathPatterns("/login", "/captcha.jpg", "/register", "/start", "/reset", "/reset2")
                    .excludePathPatterns("/swagger-resources/**", "/webjars/**", "/v2/**", "/doc.html/**", "/error/**", "/stationLetter/**",
                            "/sendOutSideMail/**","/reception/**","/signature/**","/contact/**","/groups/**");
              //      .excludePathPatterns("/column/**","/article/**","/login/**","/register/**","/stationLetter/**","/ac_manage/**");

        }
        if (captcha) {
            registry.addInterceptor(getCaptchaInterceptor())
                    .addPathPatterns("/login")
                    .excludePathPatterns("/swagger-resources/**", "/webjars/**", "/v2/**", "/doc.html/**", "/error/**", "/stationLetter/**","/reception/**");
               //    .excludePathPatterns("/column/**","/article/**","/login/**","/register/**","/stationLetter/**","/ac_manage/**");

        }

    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("doc.html")
                .addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Bean
    public FilterRegistrationBean registerFilter() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(httpServletFilter);
        registration.addUrlPatterns("/*");
        registration.setOrder(1);
        return registration;
    }
}
