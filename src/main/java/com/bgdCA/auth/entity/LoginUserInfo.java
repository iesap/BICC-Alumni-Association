package com.bgdCA.auth.entity;

/**
 * @ClassName LoginUserInfo
 * @Description TODO
 * @Author AlvinZheng
 * @Date 2020/8/6 14:32
 * @Version 1.0
 **/

public class LoginUserInfo {

    private String username;

    private String password;

    private String captcha;

    @Override
    public String toString() {
        return "LoginUserInfo{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", captcha='" + captcha + '\'' +
                '}';
    }

    public LoginUserInfo() {
    }

    public LoginUserInfo(String username, String password, String captcha) {
        this.username = username;
        this.password = password;
        this.captcha = captcha;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }
}
