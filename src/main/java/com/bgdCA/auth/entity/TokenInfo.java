package com.bgdCA.auth.entity;

/**
 * 用户信息
 */
public class TokenInfo {

    private Long id;

    private String username;

    private String authorization;

    @Override
    public String toString() {
        return "TokenInfo{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", authorization='" + authorization + '\'' +
                '}';
    }

    public TokenInfo() {
    }

    public TokenInfo(Long id, String username, String authorization) {
        this.id = id;
        this.username = username;
        this.authorization = authorization;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}