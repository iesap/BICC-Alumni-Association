package com.bgdCA.auth.entity;

/**
 * @ClassName AuthConstants
 * @Description TODO
 * @Author AlvinZheng
 * @Date 2020/8/3 0:53
 * @Version 1.0
 **/
public class AuthConstants {

    public static final String ADMIN = "admin";

    public static final String USER = "user";

}
