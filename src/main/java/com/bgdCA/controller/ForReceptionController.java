package com.bgdCA.controller;


import com.bgdCA.annotation.BiccAuth;
import com.bgdCA.domain.StationLetter;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.AccountManageService;
import com.bgdCA.service.ColumnService;
import com.bgdCA.service.IArticleService;
import com.bgdCA.service.StationLetterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@Slf4j
@Controller
@Api(value = "前台接口", tags = "前台用API")
@RequestMapping("/reception")
public class ForReceptionController {

    @Autowired
    private IArticleService iArticleService;

    @Autowired
    ColumnService columnService;

    @Autowired
    private AccountManageService accountService;

    @Autowired
    StationLetterService stationLetterService;

    @ApiOperation(value = "查询所有一级二级栏目（前台无分页）", notes = "查询所有一级二级栏目（前台无分页）")
    @RequestMapping(value = "/selectAllColumnFirstAndColumnSecondPlusPro", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public Result selectAllColumnFirstAndColumnSecondPlusPro() {
        log.debug("controller：Query all main columns and sub columns");
        return columnService.selectAllColumnFirstAndColumnSecondPlusPro();
    }

    @ApiOperation(value = "查询所有一级二级栏目（后台无分页）", notes = "查询所有一级二级栏目（后台无分页）")
    @RequestMapping(value = "/selectAllColumnFirstAndColumnSecondPro", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public Result selectAllColumnFirstAndColumnSecondPro() {
        log.debug("controller：Query all main columns and sub columns");
        return columnService.selectAllColumnFirstAndColumnSecondPro();
    }

    @ApiOperation(value = "通过二级栏目查找文章", notes = "通过二级栏目查找文章")
    @RequestMapping(value = "/article/selectArticleByColumnId", method = RequestMethod.POST)
    @ResponseBody
    public Result selectArticleByColumnId(@RequestParam int columnId,
                                          @ApiParam(value = "起始页(num>=0)", example = "1", required = true)
                                          @RequestParam(defaultValue = "1") int pageNum,
                                          @ApiParam(value = "查询数(默认10)", example = "10", required = false)
                                          @RequestParam(defaultValue = "10") int pageSize) {
        log.debug("controller：Query all articles by the author,columnId:" + columnId);
        return iArticleService.selectArticleByColumnId(columnId, pageNum, pageSize);
    }

    @ApiOperation(value = "通过文章id查询文章", notes = "通过文章id查询文章")
    @RequestMapping(value = "/article/selectArticle", method = RequestMethod.POST)
    @ResponseBody
    public Result selectArticle(@RequestBody int articleId
    ) throws IOException {
        log.debug("controller：Query articles by article id,articleId:" + articleId);
        return iArticleService.selectArticle(articleId);
    }

    @ApiOperation(value = "查询所有文章信息(无分页)", notes = "查询所有文章信息（无分页）")
    @RequestMapping(value = "/article/findAllPro", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public Result findAllPro() {
        log.debug("controller：The article module queries all article information");
        return iArticleService.findAllPro();
    }

    @ApiOperation(value = "查询联系人", notes = "查询联系人")
    @PostMapping("/findPersonByName")
    @ResponseBody
    public Result findTargetPersonName(@RequestBody String name) {
        log.debug("Controller:" + name);
        if (accountService.findPerson(name).get("msg") != "1") {

            return Result.error(CodeMsg.ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_USER_NOT_FOUND);
        }

        return Result.success(accountService.findPerson(name).get("Accountlist"));
    }

    @ApiOperation(value = "查询联系人", notes = "查询联系人")
    @PostMapping("/findPersonByEmail")
    @ResponseBody
    public Result findTargetPersonEmail(@RequestBody String mail) {
        log.debug("Controller:" + mail);
        if (accountService.findPersonEmail(mail).get("msg") != "1") {

            return Result.error(CodeMsg.ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_USER_NOT_FOUND);
        }

        return Result.success(accountService.findPersonEmail(mail).get("Accountlist"));
    }

    @ApiOperation(value = "查询联系人", notes = "查询联系人")
    @PostMapping("/findPersonByNickName")
    @ResponseBody
    public Result findTargetPersonNickName(@RequestBody String name) {
        log.debug("Controller:" + name);
        if (accountService.findPersonNickName(name).get("msg") != "1") {

            return Result.error(CodeMsg.ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_USER_NOT_FOUND);
        }

        return Result.success(accountService.findPersonNickName(name).get("Accountlist"));
    }

    @ApiOperation(value = "查询收件人所有站内信（无分页）", notes = "查询收件人所有站内信（无分页）")
    @ApiImplicitParam(name = "userId", value = "收件人ID", dataType = "int", paramType = "json", required = true)
    @RequestMapping(value = "/stationLetter/checkAllMessagesReceivedPlus", method = RequestMethod.POST)
    @ResponseBody
    public Result selectAllByUserIdPlus(@RequestBody int userId) {
        log.debug("controller：Query all the messages of the recipient,userId:" + userId);
        return stationLetterService.selectAllByUserIdPlus(userId);
    }

    @ApiOperation(value = "发送站内信", notes = "发送站内信")
    @RequestMapping(value = "/stationLetter/sendMessages", method = RequestMethod.POST)
    @ResponseBody
    public Result sendMessage(@RequestBody StationLetter stationLetter) {
        log.debug("controller：send Message,stationLetter:" + stationLetter.toString());
        return stationLetterService.sendMessage(stationLetter);
    }

    @ApiOperation(value = "查找站内信，生成聊天记录", notes = "查找站内信，生成聊天记录")
    @RequestMapping(value = "/stationLetter/selectAllOrderByTime", method = RequestMethod.POST)
    @ResponseBody
    public Result selectAllOrderByTime(@RequestBody StationLetter stationLetter) {
        log.debug("controller：Find the message in the station and generate the chat record：stationLetter" + stationLetter.toString());
        return stationLetterService.selectAllOrderByTime(stationLetter);
    }

    @ApiOperation(value = "查询收件人所有站内信", notes = "查询收件人所有站内信")
    @RequestMapping(value = "/stationLetter/checkAllMessagesReceived", method = RequestMethod.POST)
    @ResponseBody
    public Result selectAllByUserId(@RequestParam(defaultValue = "1") int pageNum,
                                    @RequestParam(defaultValue = "10") int pageSize,
                                    @RequestParam int userId) {
        log.debug("controller：Query all the messages of the recipient,userId:" + userId);
        return stationLetterService.selectAllByUserId(userId, pageNum, pageSize);
    }


}
