package com.bgdCA.controller;

import com.bgdCA.annotation.BiccAuth;
import com.bgdCA.domain.Program;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.ProgramService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@Controller
@Api(value = "项目组管理", tags = "项目组管理API")
public class ProgramController {
    @Autowired
    ProgramService programService;

    @ApiOperation(value = "修改项目组内容", notes = "修改项目组内容")
    @RequestMapping(value = "/program/modifyProgram", method = {RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = "9")
    public Result modifyProgram(@RequestBody Program program) {
        return programService.modifyProgram(program);
    }

    @ApiOperation(value = "删除项目组内容", notes = "删除项目组内容")
    @RequestMapping(value = "/program/deleteProgram", method = {RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = "9")
    public Result deleteProgram(int programId) {
        return programService.deleteProgram(programId);
    }

    @ApiOperation(value = "查找项目组内容", notes = "查找项目组内容")
    @RequestMapping(value = "/program/foundProgram",method = {RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = "9")
    public Result foundProgram(){
        return programService.foundProgram();
    }
}
