package com.bgdCA.controller;

import com.bgdCA.annotation.BiccAuth;
import com.bgdCA.domain.Contact;
import com.bgdCA.response.Result;
import com.bgdCA.service.ContactService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author Sun Mingshan
 * @Description 【站外信】联系人
 * @date 2020/8/12 20:07
 */
@Api(value = "联系人【站外信】", tags = "联系人【站外信】")
@Slf4j
@RestController
@RequestMapping("contact")
public class ContactController {
    @Autowired
    ContactService contactService;

    @ApiOperation("删除联系人")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "contactId", value = "联系人id", dataType = "int", paramType = "query", required = true, example = "3")
    })
    @PostMapping("deleteContactByKey")
    @BiccAuth(auth = "7")
    Result deleteContactByKey(@RequestParam Integer contactId) {
        log.debug("contactId:" + contactId);
        return contactService.deleteContactByKey(contactId);
    }

    @ApiOperation("新增联系人")
    @PostMapping("insertContact")
    @BiccAuth(auth = "7")
    Result insertContact(@RequestBody Contact record) {
        log.debug("record:" + record);
        return contactService.insertContact(record);
    }

    @ApiOperation("显示联系人/分组信息(根据用户)")
    @GetMapping("selectByPrimaryKey")
    @BiccAuth(auth = "7")
    Result selectByPrimaryKey(@RequestParam String userName) {
        log.debug("userName:" + userName);
        Result result = contactService.selectByPrimaryKey(userName);
        return result;
    }

    @ApiOperation("删除有联系人的分组(批量处理)")
    @PostMapping("updateContactGroup")
    @BiccAuth(auth = "7")
    Result updateContactGroup(@RequestBody Map map) {
        log.debug("map:" + map.toString());
        List<Integer> list = (List<Integer>) map.get("list");
        int groupId = (int) map.get("groupId");
        return contactService.updateContactGroup(list, groupId);
    }

    @ApiOperation("修改联系人所在分组(批量)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "list", value = "联系人id数组", dataType = "list", paramType = "query", required = true, example = "2,3,4"),
            @ApiImplicitParam(name = "groupId", value = "分组id", dataType = "int", paramType = "query", required = true, example = "2")
    })
    @PostMapping("updateGroupOfContact")
    @BiccAuth(auth = "7")
    Result updateGroupOfContact(@RequestParam List list, @RequestParam int groupId) {
        log.debug("list:" + list);
        log.debug("groupId:" + groupId);
        return contactService.updateGroupOfContact(list, groupId);
    }

    @ApiOperation("查询空分组")
    @GetMapping("selectEmptyGroup")
    @BiccAuth(auth = "7")
    Result selectEmptyGroup(@RequestParam("userName") String userName) {
        log.debug("userName:" + userName);
        return contactService.selectEmptyGroup(userName);
    }

    @ApiOperation("修改邮箱和姓名")
    @PostMapping(value = "updateMailAddressAndName")
    @BiccAuth(auth = "7")
    public Result updateMailAddressAndName(@RequestBody Map<String, String> map) {
        log.debug("map:" + map);
        String contactMailAddress = (String) map.get("contactMailAddress");
        String contactContent = (String) map.get("contactContent");
        int contactId = Integer.parseInt(map.get("contactId"));
        return contactService.updateMailAddressAndName(contactMailAddress, contactContent, contactId);
    }

    @ApiOperation("获取档案职位分组（根据邮箱）")
    @GetMapping(value = "selectEmailGroupByDepartment")
    @BiccAuth(auth = "7")
    public Result selectEmailGroupByDepartment() {
        return contactService.selectEmailGroupByDepartment();
    }

    @ApiOperation("获取档案国籍分组（根据邮箱）")
    @GetMapping(value = "selectEmailGroupByCountry")
    @BiccAuth(auth = "7")
    public Result selectEmailGroupByCountry() {
        return contactService.selectEmailGroupByCountry();
    }
}
