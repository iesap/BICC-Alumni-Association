package com.bgdCA.controller;

import com.bgdCA.annotation.BiccAuth;
import com.bgdCA.domain.AuthConstants;
import com.bgdCA.domain.SendInfo;
import com.bgdCA.response.Result;
import com.bgdCA.service.IFileInfoService;
import com.bgdCA.service.IMailService;
import com.bgdCA.service.TeamService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Sun Mingshan
 * @Description TODO
 * @date 2020/8/8 11:48
 */
@Api(value = "【站外信】", tags = "发送邮件【站外信】")
@Slf4j
@CrossOrigin
@RestController
public class OutsideMailController {
    @Autowired
    IMailService iMailService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private IFileInfoService dataService;

    @ApiOperation(value = "发送邮件", notes = "标题，内容，接收地址，【附件，附件布尔值】（可选）" +
            "\n{\n" +
            "     \"subject\":\"标题\",\n" +
            "    \"content\":\"内容\",\n" +
            "     \"to\":[\"1094079588@qq.com\",\"223337742@qq.com\"],\n" +
            "     \"useAttachment\":\"False\",\n" +
            "     \"toName\":[\"问候1\",\"问候2\"],\n" +
            "\n" +
            "    \"fromAddress\":\"1094079588@qq.com\",\n" +
            "    \"password\":\"zkvunnkagxtufedb\",\n" +
            "    \"host\":\"smtp.qq.com\"\n" +
            "}")
    @PostMapping("/sendOutSideMail")
    @BiccAuth(auth = {AuthConstants.MailOutside})
    public Result sendOutSideMail(SendInfo sendInfo) throws Exception {
        System.out.println(sendInfo.getTo()[0]);
        System.out.println(sendInfo.getContent());
        System.out.println(sendInfo.getSubject());
        System.out.println(sendInfo.getToName());
        System.out.println(sendInfo.isUseAttachment());


        return iMailService.sendHtmlMail(sendInfo);
    }

    @ApiOperation(value = "查找单一项目组(站外信页面)", notes = "查找单一项目组")
    @RequestMapping(value = "/sendOutSideMail/selectTargetTeam",method = {RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = {AuthConstants.MailOutside})
    public Result selectTargetTeam(@RequestParam("teamId") int teamId){
        return teamService.selectTargetTeam(teamId);
    }

    @ApiOperation(value = "档案全字段搜索(站外信页面)", notes = "档案全字段搜索")
    @PostMapping("/sendOutSideMail/searchAll")
    @BiccAuth(auth = {AuthConstants.MailOutside})
    public Result dataSearchAll(@RequestBody String key) {
        log.debug("Controller:+" + key);
        return dataService.datasearchAll(key);
    }

}
