package com.bgdCA.controller;

import com.bgdCA.annotation.BiccAuth;
import com.bgdCA.domain.*;
import com.bgdCA.response.Result;
import com.bgdCA.service.ColumnService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import com.bgdCA.response.CodeMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/8/17 19:05
 */
@Slf4j
@Controller
@RequestMapping("/column")
@Api(value = "栏目管理", tags = "栏目API")
public class ColumnController {
    @Autowired
    ColumnService columnService;

    @ApiOperation(value = "查询所有主栏目名称", notes = "查询所有主栏目名称")
    @RequestMapping(value = "/selectAllColumnFirst", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result findAll() {
        log.debug("controller：Query all main column names");
        return columnService.selectAllColumnFirst();
    }


    @ApiOperation(value = "新增子栏目", notes = "新增子栏目")
    @RequestMapping(value = "/addSubColumn", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result addSubColumn(@RequestBody ColumnSecond column) {
        log.debug("controller: Add articleType,articleType:" + column.toString());
        return columnService.addSubColumn(column);
    }

    @ApiOperation(value = "新增主栏目", notes = "新增主栏目")
    @RequestMapping(value = "/addMainColumn", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result addMainColumn(@RequestBody Column column) {
        log.debug("controller: Add articleType,articleType:" + column.toString());
        return columnService.addMainColumn(column);
    }

    @RequestMapping(value = "/deleteFirst", method = RequestMethod.POST)
    @ApiOperation(value = "删除一级栏目", notes = "删除一级栏目")
    @ResponseBody
    public Result DeleteColumnFirst(@RequestBody int columnFirstId) {
        int result = columnService.DeleteColumnFirst(columnFirstId);
        if (result == 0) {
            return Result.error(CodeMsg.DELETE_FIRST_COLUMN_FAILED);
        }
        return Result.success(null);
    }

    @RequestMapping(value = "/deleteSecond", method = RequestMethod.POST)
    @ApiOperation(value = "删除二级栏目", notes = "删除二级栏目")
    @ResponseBody
    public Result DeleteColumnSecond(@RequestBody int columnSecondId) {
        int result = columnService.DeleteColumnSecond(columnSecondId);
        if (result == 0) {
            return Result.error(CodeMsg.DELETE_SECOND_COLUMN_FAILED);
        }
        return Result.success(null);
    }

    @ApiOperation(value = "修改主栏目信息", notes = "修改主栏目信息")
    @RequestMapping(value = "/saveFirst", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result saveFirst(@RequestBody ColumnDomain columnDomain) {

        return columnService.saveColumnFirst(columnDomain);

    }

    @ApiOperation(value = "修改子栏目信息", notes = "修改子栏目信息")
    @RequestMapping(value = "/saveSecond", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result saveSecond(@RequestBody ColumnDomain columnDomain) {

        return columnService.saveColumnSecond(columnDomain);

    }

    @ApiOperation(value = "查询主栏目下的子栏目", notes = "查询主栏目下的子栏目")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "columnFirstId", value = "一级栏目Id", dataType = "long", paramType = "body", required = true, example = "1")
    })
    @RequestMapping(value = "/article/selectArticleByColumnFirst", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result selectArticleByColumnFirst(@RequestBody ColumnSecond columnSecond) {
        log.debug("controller: Query all second-level columns under the first-level column:" + columnSecond.getColumnFirstId());
        return columnService.selectArticleByColumnFirst(columnSecond);
    }


    @ApiOperation(value = "查询所有一级二级栏目（后台）", notes = "查询所有一级二级栏目（后台）")
    @RequestMapping(value = "/selectAllColumnFirstAndColumnSecond", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result selectAllColumnFirstAndColumnSecond(@RequestParam(defaultValue = "1") int pageNum,
                                                      @RequestParam(defaultValue = "10") int pageSize) {
        log.debug("controller：Query all main columns and sub columns");
        return columnService.selectAllColumnFirstAndColumnSecond(pageNum, pageSize);
    }

    @ApiOperation(value = "查询所有一级二级栏目（前台无分页）", notes = "查询所有一级二级栏目（前台无分页）")
    @RequestMapping(value = "/selectAllColumnFirstAndColumnSecondPlusPro", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public Result selectAllColumnFirstAndColumnSecondPlusPro() {
        log.debug("controller：Query all main columns and sub columns");
        return columnService.selectAllColumnFirstAndColumnSecondPlusPro();
    }

    @ApiOperation(value = "查询所有一级二级栏目（后台无分页）", notes = "查询所有一级二级栏目（后台无分页）")
    @RequestMapping(value = "/selectAllColumnFirstAndColumnSecondPro", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result selectAllColumnFirstAndColumnSecondPro() {
        log.debug("controller：Query all main columns and sub columns");
        return columnService.selectAllColumnFirstAndColumnSecondPro();
    }

    @ApiOperation(value = "查询所有一级二级栏目（前台）", notes = "查询所有一级二级栏目（前台）")
    @RequestMapping(value = "/selectAllColumnFirstAndColumnSecondPlus", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result selectAllColumnFirstAndColumnSecondPlus(@RequestParam(defaultValue = "1") int pageNum,
                                                          @RequestParam(defaultValue = "10") int pageSize) {
        log.debug("controller：Query all main columns and sub columns");
        return columnService.selectAllColumnFirstAndColumnSecondPlus(pageNum, pageSize);
    }

    @ApiOperation(value = "通过二级栏目id单查", notes = "通过二级栏目id单查")
    @RequestMapping(value = "/selectColumnFirstAllByColumnSecondId", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result selectColumnFirstAllByColumnSecondId(@RequestParam int columnSecondId,
                                                       @RequestParam(defaultValue = "1") int pageNum,
                                                       @RequestParam(defaultValue = "10") int pageSize
    ) {
        log.debug("controller：Through the secondary column ID single check,columnSecondId\"+columnSecondId");
        return columnService.selectColumnFirstAllByColumnSecondId(columnSecondId, pageNum, pageSize);
    }

    @ApiOperation(value = "修改主栏目顺序", notes = "修改主栏目顺序")
    @RequestMapping(value = "/updateColumnFirstOrder", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result updateColumnFirstOrder(@RequestBody Order order) {
        return columnService.updateColumnFirstOrder(order);
    }

    @ApiOperation(value = "修改子栏目顺序", notes = "修改子栏目顺序")
    @RequestMapping(value = "/updateColumnSecondOrder", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result updateColumnSecondOrder(@RequestBody Order order) {
        return columnService.updateColumnSecondOrder(order);
    }
}