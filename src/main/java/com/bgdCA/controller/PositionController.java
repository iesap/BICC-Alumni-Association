package com.bgdCA.controller;

import com.bgdCA.annotation.BiccAuth;
import com.bgdCA.domain.AuthConstants;
import com.bgdCA.domain.Position;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.PositionService;
import io.lettuce.core.ConnectionEvents;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/25 10:22
 */
@Slf4j
@Controller
@Api(value = "职位管理", tags = "职位管理API")
public class PositionController {
    @Autowired
    PositionService positionService;

    @ApiOperation(value = "查询所有职位信息", notes = "查询所有职位信息")
    @RequestMapping(value = "/position/findAll", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = {AuthConstants.Permissions})
    public Result findAll() {
        log.debug("controller：Permission module query all");
        if (positionService.findAll().isEmpty()) {
            return Result.error(CodeMsg.AUTHORITY_MANAGEMENT_MODULETHE_SERVERQUERY_ALL_FAILED);
        }
        return Result.success(positionService.findAll());

    }

    @ApiOperation(value = "单个删除职位", notes = "单个删除职位")
    @ApiImplicitParam(name = "positionId", value = "权限ID", dataType = "int", paramType = "json", required = true)
    @RequestMapping(value = "/position/deletePosition", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = {AuthConstants.Permissions})
    public Result deletePosition(@RequestBody int positionId) {
        log.debug("controller：Single delete permission module,positionId:" + positionId);

        return positionService.deletePosition(positionId);
    }


    @ApiOperation(value = "通过id找职位", notes = "通过id找职位")
    @ApiImplicitParam(name = "positionId", value = "权限ID", dataType = "int", paramType = "json", required = true)
    @RequestMapping(value = "/position/selectPowerById", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = {AuthConstants.Permissions})
    public Result selectPowerById(@RequestBody int positionId) {
        log.debug("controller：Find permissions by ID,positionId:" + positionId);
        Position position = positionService.selectPowerById(positionId);
        if (position == null) {
            return Result.error(CodeMsg.AUTHORITY_MANAGEMENT_MODULETHE_SERVERID_LOOKUP_PERMISSION_FAILED);
        }
        return Result.success(position);
    }

    @ApiOperation(value = "修改职位信息", notes = "修改职位信息")
    @RequestMapping(value = "/position/modify", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = {AuthConstants.Permissions})
    public Result modifyPosition(@RequestBody Position position) {
        log.debug("controller：Permission module query all");
        if (positionService.modifyPosition(position) == 0) {
            return Result.error(CodeMsg.AUTHORITY_MANAGEMENT_MODULETHE_SERVERFAILED_TO_UPDATE_ARTICLE);
        }
        return Result.success(null);
    }

    @ApiOperation(value = "新增职位信息", notes = "新增职位信息")
    @RequestMapping(value = "/position/add", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = {AuthConstants.Permissions})
    public Result addPosition(@RequestBody Position position) {
        log.debug("controller：Permission module query all");
        if (positionService.addPosition(position) == 0) {
            return Result.error(CodeMsg.AUTHORITY_MANAGEMENT_MODULETHE_SERVERFAILED_TO_ADD);
        }
        return Result.success(null);
    }

    @ApiOperation(value = "查询职位（根据项目组）", notes = "查询职位（根据项目组）")
    @RequestMapping(value = "/position/selectPositionByTeam", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = {AuthConstants.Permissions})
    public Result selectPositionByTeam(@RequestParam String teamId) {
        log.debug("controller：Permission module query all");
        return positionService.selectPositionByTeam(teamId);
    }
}
