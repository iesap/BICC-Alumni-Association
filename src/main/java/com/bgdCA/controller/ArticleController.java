package com.bgdCA.controller;

import com.bgdCA.annotation.BiccAuth;
import com.bgdCA.domain.Article;
import com.bgdCA.domain.ColumnSecond;
import com.bgdCA.domain.Order;
import com.bgdCA.response.Result;
import com.bgdCA.service.ArticleExamineService;
import com.bgdCA.service.IArticleService;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


import java.io.IOException;
import java.util.List;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/3 21:53
 */
@Slf4j
@Controller
@Api(value = "文章管理", tags = "文章API")
public class ArticleController {
    @Autowired
    private IArticleService iArticleService;

    @Autowired
    private ArticleExamineService auditService;
    /*@RequestMapping("/article/findAll")
    @ResponseBody
    public HashMap findAll(int pageNum,int pageSize){
        return iArticleService.findAll(pageNum,pageSize);
    }*/
    @ApiOperation(value = "查询所有文章信息", notes = "查询所有文章信息")
    @RequestMapping(value = "/article/findAll", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result findAll(@RequestParam(defaultValue = "1") int pageNum,
                          @RequestParam(defaultValue = "10") int pageSize) {
        log.debug("controller：The article module queries all article information");
        return iArticleService.findAll(pageNum, pageSize);
    }

    @ApiOperation(value = "查询所有文章信息(模糊查询)", notes = "查询所有文章信息(模糊查询)")
    @RequestMapping(value = "/article/findAllByKey", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result findAllByKey(@RequestParam String keyWord,
                               @RequestParam(defaultValue = "1") int pageNum,
                               @RequestParam(defaultValue = "10") int pageSize) {
        log.debug("controller：The article module queries all article information");
        return iArticleService.findAllByKey(keyWord, pageNum, pageSize);
    }

    @ApiOperation(value = "查询所有文章信息(模糊查询)（无分页）", notes = "查询所有文章信息(模糊查询)（无分页）")
    @RequestMapping(value = "/article/findAllByKeyPro", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result findAllByKeyPro(@RequestParam String keyWord) {
        log.debug("controller：The article module queries all article information");
        return iArticleService.findAllByKeyPro(keyWord);
    }

    @ApiOperation(value = "查询所有文章信息(无分页)", notes = "查询所有文章信息（无分页）")
    @RequestMapping(value = "/article/findAllPro", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result findAllPro() {
        log.debug("controller：The article module queries all article information");
        return iArticleService.findAllPro();
    }

    @ApiOperation(value = "通过文章id查询文章", notes = "通过文章id查询文章")
    @RequestMapping(value = "/article/selectArticle", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result selectArticle(@RequestBody int articleId
    ) throws IOException {
        log.debug("controller：Query articles by article id,articleId:" + articleId);
        return iArticleService.selectArticle(articleId);
    }

    @ApiOperation(value = "批量删除文章", notes = "批量删除文章")
    @RequestMapping(value = "/article/batchDeleteArticle", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result batchDeleteArticle(@RequestBody List<Integer> list) {
        log.debug("controller：Delete articles in bulk,list:" + list.toString());
        return iArticleService.batchDeleteArticle(list);
    }

    @ApiOperation(value = "单个删除文章", notes = "单个删除文章")
    @ApiImplicitParam(name = "articleId", value = "文章ID", dataType = "int", paramType = "json", required = true)
    @RequestMapping(value = "/article/deleteArticle", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result deleteArticle(@RequestBody int articleId) {
        log.debug("controller：Delete articles individually,articleId:" + articleId);
        return iArticleService.deleteArticle(articleId);
    }

    @ApiOperation(value = "查询作者的所有文章", notes = "查询作者的所有文章")
    @RequestMapping(value = "/article/selectArticleByAuthor", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result selectArticleByAuthor(@RequestParam int authorId,
                                        @ApiParam(value = "起始页(num>=0)", example = "1", required = true)
                                        @RequestParam(defaultValue = "1") int pageNum,
                                        @ApiParam(value = "查询数(默认10)", example = "10", required = false)
                                        @RequestParam(defaultValue = "10") int pageSize) {
        log.debug("controller：Query all articles by the author,authorId:" + authorId);
        return iArticleService.selectArticleByAuthor(authorId, pageNum, pageSize);
    }


    @ApiOperation(value = "查询作者的所有文章(模糊查询)", notes = "查询作者的所有文章(模糊查询)")
    @RequestMapping(value = "/article/selectArticleByAuthorByKey", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result selectArticleByAuthorByKey(@RequestBody Article article) {
        log.debug("controller：Query all articles by the author,authorId:" + article.getArticleId());
        return iArticleService.selectArticleByAuthorByKey(article);
    }

    @ApiOperation(value = "更新文章", notes = "更新文章")
    @RequestMapping(value = "/article/updateArticle", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result updateArticle(@RequestBody Article article) {
        log.debug("controller：Update article,article:" + article);
        return iArticleService.updateArticle(article);
    }

    @ApiOperation(value = "新增文章", notes = "新增文章")
    @RequestMapping(value = "/article/insertArticle", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result insertArticle(@RequestBody Article article) {
        log.debug("controller：Add article,article:" + article);
        return iArticleService.insertArticle(article);
    }

    @ApiOperation(value = "通过二级栏目查找文章", notes = "通过二级栏目查找文章")
    @RequestMapping(value = "/article/selectArticleByColumnId", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result selectArticleByColumnId(@RequestParam int columnId,
                                          @ApiParam(value = "起始页(num>=0)", example = "1", required = true)
                                          @RequestParam(defaultValue = "1") int pageNum,
                                          @ApiParam(value = "查询数(默认10)", example = "10", required = false)
                                          @RequestParam(defaultValue = "10") int pageSize) {
        log.debug("controller：Query all articles by the author,columnId:" + columnId);
        return iArticleService.selectArticleByColumnId(columnId, pageNum, pageSize);
    }

    @ApiOperation(value = "通过二级栏目查找文章(无分页)", notes = "通过二级栏目查找文章（无分页）")
    @RequestMapping(value = "/article/selectArticleByColumnIdPro", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result selectArticleByColumnIdPro(@RequestParam int columnId) {
        log.debug("controller：Query all articles by the author,columnId:" + columnId);
        return iArticleService.selectArticleByColumnIdPro(columnId);
    }

    /*@ApiOperation(value = "查询二级栏目下所有文章", notes = "查询二级栏目下所有文章")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "columnSecond", value = "二级栏目", dataType = "String", paramType = "body", required = true,example = "国外名著")
    })
    @RequestMapping(value = "/article/selectArticleByColumnSecond",method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "06")
    public Result selectArticleByColumnSecond(@RequestBody String columnSecond){
        log.debug("controller：Query all articles under the secondary column,columnSecond:"+columnSecond);
        return iArticleService.selectArticleByColumnSecond(columnSecond);
    }*/

    @ApiOperation(value = "查询二级栏目下的所有文章", notes = "查询二级栏目下的所有文章")
    @RequestMapping(value = "/article/selectAllColumnSecondArticle", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result selectAllColumnSecond(@RequestParam(defaultValue = "1") int pageNum,
                                        @RequestParam(defaultValue = "10") int pageSize) {
        log.debug("controller：query all articles under the secondary column");
        return iArticleService.selectAllColumnSecond(pageNum, pageSize);
    }


    @ApiOperation(value = "修改文章顺序", notes = "修改文章顺序")
    @RequestMapping(value = "/updateArticleOrder", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "06")
    public Result updateArticleOrder(@RequestBody Order order) {
        return iArticleService.updateArticleOrder(order);
    }

    @ApiOperation(value = "查询所有已通过文章",notes="查询所有已通过文章")
    @RequestMapping(value = "/findAllPass", method =RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "06")
    public Result findAllPass(@RequestBody String columnSecondName){
        log.debug("Controller: Examine module find all pass all audit artcile");
        return auditService.findAllPass(columnSecondName);
    }
}

