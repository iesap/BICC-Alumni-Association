package com.bgdCA.controller;

import com.bgdCA.annotation.BiccAuth;
import com.bgdCA.domain.StationLetter;
import com.bgdCA.response.Result;
import com.bgdCA.service.StationLetterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/29 09:30
 */
@Slf4j
@Controller
@Api(value = "站内信模块", tags = "站内信管理API")
public class StationLetterController {
    @Autowired
    StationLetterService stationLetterService;

    @ApiOperation(value = "查询收件人所有站内信", notes = "查询收件人所有站内信")
    @RequestMapping(value = "/stationLetter/checkAllMessagesReceived", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "8")
    public Result selectAllByUserId(@RequestParam(defaultValue = "1") int pageNum,
                                    @RequestParam(defaultValue = "10") int pageSize,
                                    @RequestParam int userId) {
        log.debug("controller：Query all the messages of the recipient,userId:" + userId);
        return stationLetterService.selectAllByUserId(userId, pageNum, pageSize);
    }

    @ApiOperation(value = "查询收件人所有站内信（无分页）", notes = "查询收件人所有站内信（无分页）")
    @ApiImplicitParam(name = "userId", value = "收件人ID", dataType = "int", paramType = "json", required = true)
    @RequestMapping(value = "/stationLetter/checkAllMessagesReceivedPlus", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "8")
    public Result selectAllByUserIdPlus(@RequestBody int userId) {
        log.debug("controller：Query all the messages of the recipient,userId:" + userId);
        return stationLetterService.selectAllByUserIdPlus(userId);
    }

    @ApiOperation(value = "删除单个站内信", notes = "删除单个站内信")
    @ApiImplicitParam(name = "id", value = "站内信ID", dataType = "int", paramType = "json", required = true)
    @RequestMapping(value = "/stationLetter/deleteOneStationLetter", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "8")
    public Result deleteStationLetterById(@RequestBody int id) {
        log.debug("controller：Single delete station message,id:" + id);
        return stationLetterService.deleteStationLetterById(id);
    }

    @ApiOperation(value = "批量删除站内信", notes = "批量删除站内信")
    @RequestMapping(value = "/stationLetter/deleteManyStationLetter", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "8")
    public Result batchDeleteStationLetter(@RequestBody List<Integer> list) {
        log.debug("controller：Delete messages in batches,list:" + list.toString());
        return stationLetterService.batchDeleteStationLetter(list);
    }

    @ApiOperation(value = "站内信标为已读", notes = "站内信标为已读")
    @ApiImplicitParam(name = "id", value = "站内信ID", dataType = "int", paramType = "json", required = true)
    @RequestMapping(value = "/stationLetter/markStationBeaconAsRead", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "8")
    public Result updateStationLetterToRead(@RequestBody int id) {
        log.debug("controller：Mark the site beacon as read,id:" + id);
        return stationLetterService.updateStationLetterToRead(id);
    }

    @ApiOperation(value = "发送站内信", notes = "发送站内信")
    @RequestMapping(value = "/stationLetter/sendMessages", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "8")
    public Result sendMessage(@RequestBody StationLetter stationLetter) {
        log.debug("controller：send Message,stationLetter:" + stationLetter.toString());
        return stationLetterService.sendMessage(stationLetter);
    }


    @ApiOperation(value = "根据id找内容", notes = "根据id找内容")
    @ApiImplicitParam(name = "id", value = "站内信ID", dataType = "int", paramType = "json", required = true)
    @RequestMapping(value = "/stationLetter/selectStationLetterById", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "8")
    public Result selectStationLetterById(@RequestBody int id) {
        log.debug("controller：Find content by ID,id:" + id);
        return stationLetterService.selectStationLetterById(id);
    }

    @ApiOperation(value = "查找站内信，生成聊天记录", notes = "查找站内信，生成聊天记录")
    @RequestMapping(value = "/stationLetter/selectAllOrderByTime", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = "8")
    public Result selectAllOrderByTime(@RequestBody StationLetter stationLetter) {
        log.debug("controller：Find the message in the station and generate the chat record：stationLetter" + stationLetter.toString());
        return stationLetterService.selectAllOrderByTime(stationLetter);
    }
}
