package com.bgdCA.controller;


import com.bgdCA.annotation.BiccAuth;
import com.bgdCA.domain.Account;
import com.bgdCA.domain.AuthConstants;
import com.bgdCA.domain.ProfilePictureUrl;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.AccountManageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.Multipart;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Api(value = "账号管理", tags = "账号API")
@Slf4j
@RequestMapping("/ac_manage")
@Controller
public class AccountManageController {

    @Autowired
    private AccountManageService accountService;

    @ApiOperation(value = "查询用户信息（支持分页）", notes = "查询用户信息（支持分页）")
    @GetMapping("/findAll")
    @ResponseBody
    @BiccAuth(auth = {AuthConstants.Account})
    public Result findAll(@ApiParam(value = "起始页(num>=0)", example = "1", required = true)
                          @RequestParam(defaultValue = "1") int pageNum,
                          @ApiParam(value = "查询数(默认10)", example = "10", required = false)
                          @RequestParam(defaultValue = "10") int pageSize) {
        //      log.debug("Enter Controller");
        if (accountService.findAll(pageNum, pageSize).get("msg") != "1") {

            return Result.error(CodeMsg.ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_USER_NOT_FOUND);
        }


        return Result.success(accountService.findAll(pageNum, pageSize).get("AccountInfo"));
    }

    @ApiOperation(value = "查询单个用户信息", notes = "查询单个用户信息")
    @PostMapping("/findTarget")
    @ResponseBody
    @BiccAuth(auth = {AuthConstants.Account})
    public Result findTargetByName(@RequestBody String name) {
        log.debug("Controller:" + name);

        //   System.out.println(accountService.findTarget(name).get("msg").toString());
        if (accountService.findTarget(name).get("msg").toString() != "1") {

            return Result.error(CodeMsg.ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_USER_NOT_FOUND);
        }

        return Result.success(accountService.findTarget(name).get("Accountlist"));
        // return accountService.findTarget(name);
    }


    @ApiOperation(value = "添加用户信息", notes = "添加用户信息")
    @PostMapping("/add")
    @ResponseBody
    @BiccAuth(auth = {AuthConstants.Account})
    public Result AddAccount(@RequestBody Account account) {
        log.debug("Controller:" + account.toString());
        if (accountService.addAccount(account).get("msg") != "1") {

            return Result.success(accountService.addAccount(account).get("Accountlist"));
            // return Result.error(CodeMsg.ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_USERNAME_ALREADY_USE);
        }
        return Result.success(null);
        //return accountService.addAccount(account);
    }

    @ApiOperation(value = "修改用户信息", notes = "修改用户信息")
    @PostMapping("/save")
    @ResponseBody
    @BiccAuth(auth = {AuthConstants.Account})
    public Result SaveAccount(@RequestBody Account account) {
        log.debug("Controller:" + account.toString());
        if (accountService.updataAccount(account).get("msg") != "1") {

            return Result.error(CodeMsg.ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_USERNAME_ALREADY_USE);
        }
        return Result.success(null);

    }

    @ApiOperation(value = "冻结用户", notes = "冻结用户")
    @PostMapping("/ban")
    @ResponseBody
    @BiccAuth(auth = {AuthConstants.Account})
    public Result BanAccount(@RequestBody int id) {
        log.debug("Controller:" + id);
        accountService.banAccount(id);
        return Result.success(null);

    }

    @ApiOperation(value = "启用用户", notes = "启用用户")
    @PostMapping("/start")
    @ResponseBody
    @BiccAuth(auth = {AuthConstants.Account})
    public Result StartAccount(@RequestBody int id) {
        log.debug("Controller:" + id);
        accountService.startAccount(id);
        return Result.success(null);
    }

    @ApiOperation(value = "删除用户信息", notes = "删除用户信息")
    @PostMapping("/delete")
    @ResponseBody
    @BiccAuth(auth = {AuthConstants.Account})
    public Result DeleteAccount(@RequestBody int id) {
        log.debug("Controller:" + id);
        if (accountService.deleteAccount(id).get("msg") != "1") {

            return Result.error(CodeMsg.ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_TO_DELETE_A_SINGLE_USER);
        }
        return Result.success(null);

    }

    @ApiOperation(value = "批量删除用户信息", notes = "批量删除用户信息")
    @PostMapping("/batchdelete")
    @ResponseBody
    @BiccAuth(auth = {AuthConstants.Account})
    public Result BatchdeleteAccount(@RequestBody List<Integer> list) {
        log.debug("Controller:" + list.toString());
        if (accountService.batchdeleteAccount(list).get("msg") != "1") {

            return Result.error(CodeMsg.ACCOUNT_MANAGEMENT_MODULEPROGRAMMASS_DELETION_OF_USERS_FAILED);
        }
        return Result.success(null);

    }

    //测试中！！！
    @ApiOperation(value = "获取用户权限", notes = "获取用户权限")
    @PostMapping("/getpower")
    @ResponseBody
    @BiccAuth(auth = {AuthConstants.Account})
    public Result getPower(@RequestBody int id) {
        //   log.debug("Controller:"+list.toString());
        if (accountService.getPower(id).get("msg") != "1") {

            return Result.error(CodeMsg.ACCOUNT_MANAGEMENT_MODULETHE_SERVERFAILED_TO_OBTAIN_USER_RIGHTS);
        }
        return Result.success(accountService.getPower(id).get("power"));


    }

    //测试中！！！
    @ApiOperation(value = "修改用户权限", notes = "修改用户权限")
    @PostMapping("/changepower")
    @ResponseBody
    @BiccAuth(auth = {AuthConstants.Account})
    public Result updateAccountPower(@RequestParam int id, @RequestParam int power) {
        //      log.debug("Controller:"+account.toString());
        if (accountService.updateAccountPower(id, power).get("msg") != "1") {

            return Result.error(CodeMsg.ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_TO_MODIFY_USER_PERMISSIONS);
        }
        return Result.success(null);

    }


    /**
     * 上传 - 修改头像
     */
    @ApiOperation(value = "上传用户头像", notes = "上传用户头像")
    @PostMapping("/uploadPortrait")
    @ResponseBody
    public Result uploadPortrait(@RequestParam(value = "file", required = true) MultipartFile file
            , @RequestParam(value = "userName", required = true) String userName
            , @RequestParam(value = "userId", required = true) String userId) {
        log.debug("Controller:" + file.toString() + ";userName:" + userName + ";userId:" + userId);
        return accountService.uploadPortrait(file, userName, userId);
    }


    @ApiOperation(value = "查询联系人", notes = "查询联系人")
    @PostMapping("/findPersonByName")
    @ResponseBody
    public Result findTargetPersonName(@RequestBody String name) {
        log.debug("Controller:" + name);
        if (accountService.findPerson(name).get("msg") != "1") {

            return Result.error(CodeMsg.ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_USER_NOT_FOUND);
        }

        return Result.success(accountService.findPerson(name).get("Accountlist"));
    }

    @ApiOperation(value = "查询联系人", notes = "查询联系人")
    @PostMapping("/findPersonByEmail")
    @ResponseBody
    public Result findTargetPersonEmail(@RequestBody String mail) {
        log.debug("Controller:" + mail);
        if (accountService.findPersonEmail(mail).get("msg") != "1") {

            return Result.error(CodeMsg.ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_USER_NOT_FOUND);
        }

        return Result.success(accountService.findPersonEmail(mail).get("Accountlist"));
    }

    @ApiOperation(value = "查询联系人", notes = "查询联系人")
    @PostMapping("/findPersonByNickName")
    @ResponseBody
    public Result findTargetPersonNickName(@RequestBody String name) {
        log.debug("Controller:" + name);
        if (accountService.findPersonNickName(name).get("msg") != "1") {

            return Result.error(CodeMsg.ACCOUNT_MANAGEMENT_MODULEPROGRAMFAILED_USER_NOT_FOUND);
        }

        return Result.success(accountService.findPersonNickName(name).get("Accountlist"));
    }
}

