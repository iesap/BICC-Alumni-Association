package com.bgdCA.controller;

import com.bgdCA.annotation.BiccAuth;
import com.bgdCA.domain.ArticleDomain;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.ArticleExamineService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Slf4j
@RequestMapping("/examine")
@Controller
@Api(value = "文章审核", tags = "文章审核API")
public class ArticleExamineController {

    @Autowired
    private ArticleExamineService auditService;

//    @ApiOperation(value = "指定文章的所有信息", notes = "通过文章id查询查询指定文章的所有信息")
//    @RequestMapping(value = "/detail",method = RequestMethod.POST)
//    @ResponseBody
//    @BiccAuth(auth = {"11","12"})
//    public Result findTarget(@RequestBody String key){
//        return Result.success(auditService.findTarget(key));
//    }

    @ApiOperation(value = "查询当前管理员未审核文章", notes = "通过userId查询所有当前管理员未审核文章")
    @RequestMapping(value = "/findNoPass", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = {"705"})
    public Result findNoPass(
            @RequestParam(defaultValue = "1") int pageNum,
            @RequestParam(defaultValue = "10") int pageSize,
            @RequestParam int userId,
            @ApiParam(value = "查询数(默认空)", example = "", required = false)
            @RequestParam String key) {
        log.debug("Controller: Examine module Select all no pass article by userId");
        return auditService.findNoPass(userId, key, pageNum, pageSize);
    }

    //通过
    @ApiOperation(value = "文章通过审核", notes = "通过artcileId通过文章审核")
    @RequestMapping(value = "/pass", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = {"703", "704"})
    public Result pass(@RequestBody int articleId) {
        log.debug("Controller: Examine module Update article pass examine by artcileId");
        return auditService.pass(articleId);
    }

    //不通过
    @ApiOperation(value = "文章不通过审核", notes = "通过artcileId不通过文章审核")
    @RequestMapping(value = "/noPass", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = {"703", "704"})
    public Result noPass(@RequestBody int articleId) {
        log.debug("Controller: Examine module Update article no pass examine by artcileId");
        return auditService.noPass(articleId);
    }

    //批量审核通过
    @ApiOperation(value = "文章批量通过审核", notes = "批量通过artcileId通过文章审核")
    @RequestMapping(value = "/passAllSelect", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = {"703", "704"})
    public Result passAllSelect(@RequestBody Map<String, Object> list) {
        log.debug("Controller: Examine module Update article no pass examine by artcileId");
        List<Integer> artlist = (List<Integer>) list.get("list");
        return auditService.passAllSelect(artlist);
    }

    //批量审核不通过
    @ApiOperation(value = "文章批量不通过审核", notes = "批量通过artcileId不通过文章审核")
    @RequestMapping(value = "/noPassAllSelect", method = RequestMethod.POST)
    @ResponseBody
    @BiccAuth(auth = {"703", "704"})
    public Result noPassAllSelect(@RequestBody Map<String, Object> list) {
        log.debug("Controller: Examine module Update article no pass examine by artcileId");
        List<Integer> artlist = (List<Integer>) list.get("list");
        return auditService.noPassAllSelect(artlist);
    }



}
