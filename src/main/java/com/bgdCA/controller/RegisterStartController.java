package com.bgdCA.controller;


import com.bgdCA.domain.Account;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
public class RegisterStartController {
    @Autowired
    RegisterService registerService;
    @Autowired
    private RedisTemplate redisTemplate;

    @RequestMapping("/start")
    @ResponseBody
    public Result Start(@RequestBody String name) {

        // 获取hash下的所有key和value
        Map<String, Object> resultMap = redisTemplate.opsForHash().entries(name);
//        for (String hashKey : resultMap.keySet()) {
//            System.out.println(hashKey + ": " + resultMap.get(hashKey));
//        }


        System.out.println(name);

        Account account = new Account();

        account.setUsername(name);
        account.setPassword(redisTemplate.opsForHash().get(name, "password").toString());
        account.setMail(redisTemplate.opsForHash().get(name, "mail").toString());
        //String username=name;
        // String password=redisTemplate.opsForHash().get(username,"password").toString();
        //  String mail=(redisTemplate.opsForHash().get(name,"mail").toString());

        //  System.out.println(name+password+mail);

        registerService.insertUser(account);

        redisTemplate.delete(name);

        return Result.success(null);


//        RegisterUserInfo registerUserInfo = new RegisterUserInfo();
//
//        registerUserInfo.setUsername(resultMap.get("username"));
        // System.out.println(redisTemplate.getExpire(username));

    }
}
