package com.bgdCA.controller;


import com.bgdCA.domain.ResetUser;
import com.bgdCA.response.CodeMsg;
import com.bgdCA.response.Result;
import com.bgdCA.service.ResetPsdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ResetPsd2Controller {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    ResetPsdService resetPsdService;

    @RequestMapping("/reset2")
    @ResponseBody
    public Result resetPart2(@RequestBody ResetUser resetUser) {

        String username = resetUser.getUsername();
        String captcha = resetUser.getCaptcha();
        String newpsd = resetUser.getNewpsd();

        String captcha1 = redisTemplate.opsForValue().get(username).toString();

        if (!captcha.trim().toUpperCase().equals(captcha1)) {
            return Result.error(CodeMsg.RESET_CAPTCHA_NOT_RIGHT);
        }

        resetPsdService.resetPsd(resetUser);

        redisTemplate.delete(username);

        return Result.success(null);
    }

}
