package com.bgdCA.controller;


import com.bgdCA.annotation.BiccAuth;
import com.bgdCA.domain.FileInfo;
import com.bgdCA.domain.Team;
import com.bgdCA.response.Result;
import com.bgdCA.service.ProgramService;
import com.bgdCA.service.TeamService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Api(value = "项目组管理", tags = "项目组API")
@Slf4j
@RequestMapping("/team")
@Controller
public class TeamController {

    @Autowired
    private TeamService teamService;

    @Autowired
    private ProgramService programService;
    @ApiOperation(value = "新增项目组", notes = "新增项目组")
    @PostMapping("/addTeam")
    @BiccAuth(auth = "9")
    @ResponseBody
    public Result AddTeam(@RequestBody Team team) {

        System.out.println(team.toString());
        return teamService.addTeam(team);
    }

    @ApiOperation(value = "删除项目组", notes = "删除项目组")
    @PostMapping("/deleteTeam")
    @BiccAuth(auth = "9")
    @ResponseBody
    public Result DeleteTeam(@RequestBody int teamid) {

        return teamService.deleteTeam(teamid);
    }

    @ApiOperation(value = "查找项目组内容", notes = "查找项目组内容")
    @RequestMapping(value = "/program/foundProgram",method = {RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = "9")
    public Result foundProgram(){
        return programService.foundProgram();
    }

    @ApiOperation(value = "查找单一项目组", notes = "查找单一项目组")
    @RequestMapping(value = "selectTargetTeam",method = {RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = "9")
    public Result selectTargetTeam(@RequestParam("teamId") int teamId){
        return teamService.selectTargetTeam(teamId);
    }
}
