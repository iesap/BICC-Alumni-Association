package com.bgdCA.controller;

import com.bgdCA.annotation.BiccAuth;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Api(value = "请求上传视频", tags = "请求上传视频API")
@CrossOrigin
@Slf4j
@Controller
public class FileUploadController {

    //访问路径为：http://localhost:8080/file
    @ApiOperation(value = "请求上传视频", notes = "请求上传视频")
    @RequestMapping(value = "/file", method = {RequestMethod.GET, RequestMethod.POST})
    @BiccAuth(auth = "06")
    public String file() {
        System.out.print("================请求路径===跳转file页面=====" + "\n");
        return "/file";

    }

//    @RequestMapping("/shangchuan")
//
//    public String shangchuan(){
//        System.out.print("================请求路径===跳转index页面====="+"\n");
//        return "/index";
//
//    }
}
