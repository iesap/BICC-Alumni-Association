package com.bgdCA.controller;

import com.bgdCA.annotation.BiccAuth;
import com.bgdCA.response.Result;
import com.bgdCA.service.ArticleRebackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/26 16:05
 */
@Slf4j
@Controller
@Api(value = "文章历史记录管理", tags = "文章历史记录API")
public class ArticleRebackController {
    @Autowired
    ArticleRebackService artcileRebackService;

    @ApiOperation(value = "查询所有文章历史记录信息", notes = "查询所有文章历史记录信息")
    @RequestMapping(value = "/articleReback/findAll", method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result findAll(@RequestParam(defaultValue = "1") int pageNum,
                          @RequestParam(defaultValue = "10") int pageSize) {
        log.debug("controller：Article table history table query all");
        return artcileRebackService.findAll(pageNum, pageSize);
    }

    @ApiOperation(value = "查询所有文章历史记录信息(模糊查询)", notes = "查询所有文章历史记录信息(模糊查询)")
    @RequestMapping(value = "/articleReback/findAllPro", method = {RequestMethod.POST})
    @ResponseBody
    @BiccAuth(auth = "6")
    public Result findAllPro(@RequestBody String keyWord) {
        log.debug("controller：Article table history table query all");
        return artcileRebackService.findAllPro(keyWord);
    }
}
