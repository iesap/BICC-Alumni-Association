package com.bgdCA.controller;

import com.bgdCA.annotation.BiccAuth;
import com.bgdCA.domain.Groups;
import com.bgdCA.response.Result;
import com.bgdCA.service.GroupsService;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (Groups)表控制层
 *
 * @author sms
 * @since 2020-08-12 18:09:08
 */
@Api(value = "分组【站外信】", tags = "分组【站外信】")
@RestController
@RequestMapping("groups")
@CrossOrigin
public class GroupsController {
    /**
     * 服务对象
     */
    @Resource
    private GroupsService groupsService;

    @ApiOperation("删除分组")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "groupId", value = "分组id", dataType = "int", paramType = "query", required = true, example = "3")
    })
    @PostMapping("deleteGroups")
    @BiccAuth(auth = "7")
    Result deleteGroupById(@ApiParam(example = "2") @RequestParam Integer groupId) {
        return this.groupsService.deleteGroupById(groupId);
    }


    @ApiOperation("重命名分组")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "groupId", value = "分组id", dataType = "int", paramType = "query", required = true, example = "3"),
            @ApiImplicitParam(name = "groupContent", value = "分组内容", dataType = "int", paramType = "query", required = true, example = "3")
    })
    @PostMapping("updateGroups")
    @BiccAuth(auth = "7")
    Result updateGroup(@RequestParam String groupContent,
                       @RequestParam int groupId) {
        return this.groupsService.updateGroup(groupContent, groupId);
    }

    @ApiOperation("新增分组")
    @PostMapping("insertGroups")
    @BiccAuth(auth = "7")
    Result insertGroup(@RequestBody Groups groups) {
        return this.groupsService.insertGroup(groups);
    }



}