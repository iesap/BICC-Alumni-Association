package com.bgdCA.controller;

import com.bgdCA.annotation.BiccAuth;
import com.bgdCA.domain.Signature;
import com.bgdCA.response.Result;
import com.bgdCA.service.SignatureService;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Signature)表控制层
 *
 * @author sms
 * @since 2020-08-12 20:11:21
 */
@Api(value = "签名【站外信】", tags = "签名【站外信】")
@RestController
@CrossOrigin
@RequestMapping("signature")
public class SignatureController {
    @Resource
    private SignatureService signatureService;

    @ApiOperation("查询签名（根据用户名）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "用户名", dataType = "String", paramType = "query", required = true, example = "system2"),
    })
    @GetMapping("querySignatureById")
    @BiccAuth(auth = "7")
    public Result querySignatureById(@RequestParam String userName) {
        return this.signatureService.querySignatureById(userName);
    }

    @ApiOperation("新建签名")
    @PostMapping("insertSignature")
    @BiccAuth(auth = "7")
    Result insertSignature(@RequestBody Signature signature) {
        return signatureService.insertSignature(signature);
    }

    @ApiOperation("删除签名（根据主键）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "signatureId", value = "签名ID", dataType = "Integer", paramType = "query", required = true, example = "1"),
    })
    @PostMapping("deleteSignatureById")
    @BiccAuth(auth = "7")
    Result deleteSignatureById(@RequestParam Integer signatureId) {
        return signatureService.deleteSignatureById(signatureId);
    }
}