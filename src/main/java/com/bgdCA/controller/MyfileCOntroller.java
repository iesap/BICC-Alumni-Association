package com.bgdCA.controller;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/22 11:18
 */

import com.bgdCA.annotation.BiccAuth;
import com.bgdCA.domain.Shipin;
import com.bgdCA.service.impl.ShiPinService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


@Controller
@Api(value = "上传视频", tags = "上传视频API")
@CrossOrigin
@Slf4j
//@RequestMapping("/file")
public class MyfileCOntroller {

    @Autowired
    private ShiPinService shiPinService;

    private String url;

    @PostMapping(value = "/uploadFile", produces = "application/json;charset=UTF-8")
    @ApiOperation(value = "上传视频", notes = "上传视频")
    @BiccAuth(auth = "06")
    @ResponseBody
    public HashMap<String, Object> uploadFile(MultipartFile file) {
        HashMap<String, Object> hashMap = new HashMap<String, Object>();
        System.out.print("上传文件===" + "\n");
        //判断文件是否为空
        if (file.isEmpty()) {
            hashMap.put("status", "028");
            hashMap.put("msg", "文件为空");
            return hashMap;
        }


        // 获取文件名
        String fileName = file.getOriginalFilename();
        System.out.println(file.getOriginalFilename());
//        System.out.print("上传的文件名为: "+fileName+"\n");

        fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "_" + fileName;
        System.out.print("（加个时间戳，尽量避免文件名称重复）保存的文件名为: " + fileName + "\n");


        //加个时间戳，尽量避免文件名称重复


        //String path = "C:/fileUpload/" +fileName;
        String path = "C:/Program Files/apache-tomcat-8.5.57-windows-x64/apache-tomcat-8.5.57/webapps/fileUpload/" + fileName;
        //String path = "/Users/fushengyuanwuhui/Downloads/" +fileName;


        //String path = "E:/fileUpload/" + new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "_" + fileName;
        //文件绝对路径
        System.out.print("保存文件绝对路径" + path + "\n");

        //创建文件路径
        File dest = new File(path);

        //判断文件是否已经存在
        if (dest.exists()) {
            hashMap.put("status", "028");
            hashMap.put("msg", "文件已经存在");
            return hashMap;
        }

        //判断文件父目录是否存在
        if (!dest.getParentFile().exists()) {
            dest.getParentFile().mkdir();
        }

        try {
            //上传文件
            file.transferTo(dest); //保存文件
            System.out.print("保存文件路径" + path + "\n");
            //url="http://你自己的域名/项目名/bicc/"+fileName;//正式项目
            url = "http://111.205.28.204:8085/fileUpload/" + fileName;//本地运行项目
            int jieguo = shiPinService.insertUrl(fileName, path, url);
            System.out.print("插入结果" + jieguo + "\n");
            System.out.print("保存的完整url====" + url + "\n");

        } catch (IOException e) {
            hashMap.put("status", "028");
            hashMap.put("msg", "上传失败");
            return hashMap;
        }
        hashMap.put("status", "027");
        hashMap.put("msg", "上传成功");
        hashMap.put("url", url);
        return hashMap;
    }

    //查询
    @ApiOperation(value = "查询视频", notes = "查询视频")
    @BiccAuth(auth = "06")
    @RequestMapping(value = "/chaxun", method = {RequestMethod.GET, RequestMethod.POST})
    public String huizhiDuanxin(Model model) {
        System.out.print("查询视频" + "\n");
        List<Shipin> shipins = shiPinService.selectShipin();
        System.out.print("查询到的视频数量==" + shipins.size() + "\n");
        model.addAttribute("Shipins", shipins);

        return "/filelist";
    }

}