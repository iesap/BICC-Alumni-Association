package com.bgdCA.test;

import com.bgdCA.MySpringBootApplication;
import com.bgdCA.domain.ContactAndGroup;
import com.bgdCA.domain.DutyGroup;
import com.bgdCA.mapper.ContactMapper;
import com.bgdCA.service.IMailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * @author Sun Mingshan
 * @Description TODO
 * @date 2020/8/8 11:51
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MySpringBootApplication.class)
public class TestModule {
    @Autowired
    IMailService iMailService;

    @Autowired
    ContactMapper contactMapper;

    @Test
    public void test() throws Exception {
        // String content = "<html>\n" + "<body>\n" + "<h3>hello world!这是一封html邮件！</h3>\n" + "</body>\n" + "</html>";
        // String[] to = {"223337742@qq.com"};
        // String[] path = {"src/1.txt", "src/2.txt"};
        // SendInfo s = new SendInfo();
        // s.setTo(to);
        // s.setSubject("这是一封复杂的邮件");
        // s.setHtml(true);
        // s.setContent(content);
        // s.setUseAttachment(true);
        // s.setAttachment(path);
        // iMailService.sendHtmlMail(s);
        // List<ContactAndGroup> list =  contactMapper.selectByPrimaryKey("system2");
        // System.out.println(list.toString());

        // List<DutyGroup> list = contactMapper.selectEmailGroupByDuty();
        // System.out.println(list);
        // for (DutyGroup dutyGroup:list) {
        //     System.out.println(dutyGroup.toString());
        // }


    }
}
