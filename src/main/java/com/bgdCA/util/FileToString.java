package com.bgdCA.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author ShiJiaWei
 * @version 1.0
 * @date 2020/7/4 15:50
 */
public class FileToString {
    public static String fileToString(String filePath) throws IOException {
        StringBuffer buffer = new StringBuffer();
        BufferedReader bf = new BufferedReader(new FileReader(filePath));
        String s = null;
        while ((s = bf.readLine()) != null) {//使用readLine方法，一次读一行
            buffer.append(s.trim());
        }

        String xml = buffer.toString();
        return xml;
    }
}
