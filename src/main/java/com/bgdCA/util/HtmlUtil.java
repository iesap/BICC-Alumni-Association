package com.bgdCA.util;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * @ClassName HtmlUtil
 * @Description TODO
 * @Author AlvinZheng
 * @Date 2020/7/27 13:56
 * @Version 1.0
 **/
@Slf4j
public class HtmlUtil {

    public static String readFile(String filename) {

        try {
            char[] array = new char[1000];
            File file = new File(filename);
            FileInputStream fip = new FileInputStream(file);
            InputStreamReader reader = new InputStreamReader(fip, "UTF-8");
            StringBuffer sb = new StringBuffer();
            int n;
            int charCount = 0;
            while ((n = reader.read(array)) != -1) {
                charCount += n;
                sb.append(array);
            }
            log.info(filename + " read finish, total " + charCount + " chars");
            return sb.toString();
        } catch (Exception e) {
            log.error("file read failure", e);
            return null;
        }
    }
}
